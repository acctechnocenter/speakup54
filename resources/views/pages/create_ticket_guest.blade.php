@extends('layouts.app')

@section('content')
<!--================Banner Area =================-->
<section class="banner_area">
    <div class="container">
        <div class="banner_text_inner">
            <h4>Form Pelaporan</h4>
            <h6 style="color:#ffffff">
                Anda tidak perlu khawatir terungkapnya identitas diri anda karena ACC Whistle akan
                MERAHASIAKAN IDENTITAS DIRI ANDA sebagai whistleblower. ACC Whistle menghargai informasi yang Anda
                laporkan.
                Fokus kami hanya kepada materi informasi yang Anda Laporkan.
                <br><br>laporkan segera di form pelaporan atau login terlebih dahulu untuk membuat pelaporan.
            </h6>
        </div>
    </div>
</section>
<!--================End Banner Area =================-->

<section class="main_slider_area">
    <div class="copy_right_area_top">

    </div>
</section>

<!--================Static Area =================-->
<section class="static_area">
    <div class="container">
        <div class="static_inner">
            <div class="row">
                <div class="col-lg-12">
                    @if (session('work'))
                    <div class="alert alert-success text-center">
                        {{ session('work') }}
                    </div>
                    @endif
                </div>
                <div class="col-lg-5">
                <h5 style = "margin : 0 0 5px 0">Full Disclosure</h5>
                <p style = "margin : 0 0 10px 0">Pelapor memilih untuk memberikan seluruh identitasnya untuk memudahkan komunikasi dengan PIC yang melakukan tindak lanjut terhadap pelaporan. Dengan memilih pelaporan Full Disclosure, anda dapat menggunakan fitur lacak tiket untuk memantau pelaporan anda.</p>
                    <div class="panel panel-default">
                        <div class="panel-heading">Form Login</div>
                        <div class="panel-body">
                            @if (session('status'))
                            <div class="alert alert-success text-center">
                                {{ session('status') }}
                            </div>
                            @endif
                            <form method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}

                                <div class="form-group row">
                                    <label for="email" class="col-sm-3 col-form-label text-md-right">Email</label>

                                    <div class="col-md-8">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                            name="email" value="{{ old('email') }}" required autofocus>

                                        @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-md-3 col-form-label text-md-right">Password</label>

                                    <div class="col-md-8">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                            name="password" required>
                                        <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                        @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-3">
                                        <button type="submit" value="submit" class="btn submit_btn2">Login</button>
                                        <br>

                                        <a class="btn btn-link" href="{{ route('register') }}">
                                            Daftar
                                        </a>
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            Lupa password ?
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- CONTENT -->
                <div class="col-lg-7">
                <h5 style = "margin : 0 0 5px 0">Partial Anonymity</h5>
                <p style = "margin : 0 0 10px 0">Pelapor memilih untuk hanya memberikan alamat email (dapat menggunakan email samaran) dan nomor telepon (jika bersedia) untuk memudahkan komunikasi dengan PIC yang melakukan tindak lanjut terhadap pelaporan. Dengan memilih pelaporan Partial Anonymity, anda tidak dapat menggunakan fitur lacak tiket untuk memantau pelaporan anda.</p>
                    <div class="panel panel-default">
                        <div class="panel-heading">Form Pengaduan</div>
                        <div class="panel-body">
                            <div class="static_main_content">
                                <div class="contact_form">
                                    <!-- <div class="main_title text-center">
                                                        <h3>Create your Compliance</h3>
                                                    </div> -->
                                    {!! Form::open(['url' => '/users/create-complaint/save-guest', 'role' => 'form',
                                    'enctype' => 'multipart/form-data', 'novalidate','files' => true]) !!}
                                    <div class="form-group col-lg-12 {{ $errors->has('email') ? ' has-error' : '' }}">
                                        {!! Form::label('email_guest', 'Email', ['for' => 'email_guest', 'style' =>
                                        'font-weight: bold !important;']) !!} *
                                        <p style="font-size: 12px;">Silahkan masukan email anda.</p>
                                        {!! Form::text('email_guest', '', ['class' => 'form-control', 'required']) !!}
                                    </div>

                                    <div class="form-group col-lg-12 {{ $errors->has('email') ? ' has-error' : '' }}">
                                        {!! Form::label('no_handphone_for_guest', 'No handphone (Jika Bersedia)', ['for' => 'no_handphone_for_guest', 'style' => 'font-weight: bold !important;']) !!} 
                                        <!-- <p style="font-size: 12px;">Silahkan masukan no handphone anda.</p> -->
                                        {!! Form::text('no_handphone_for_guest', '', ['class' => 'form-control']) !!}
                                    </div>

                                    <div class="form-group col-lg-12 {{ $errors->has('ticket_attachment') ? ' has-error' : '' }}">
                                        {!! Form::label('id_category', 'Kategori', ['for' => 'kategori', 'style' =>
                                        'font-weight: bold !important;']) !!} *
                                        <p style="font-size: 12px;">Silahkan pilih kategori pelaporan.</p>
                                        <select id="inputSelectCategory" class="form-control editor" name="id_category">
                                            <option value="">--Silahkan Pilih--</option>
                                            @foreach($parentcategories as $category)
                                            <option value={{$category->id}}>{{ $category->category_name }}</option>
                                            @endforeach
                                        </select>
                                        <p id="category_description"></p>
                                        <span class="help-block">
                                            <strong>{{ $errors->first('id_category') }}</strong>
                                        </span>
                                    </div>

                                    <div class="form-group col-lg-12 {{ $errors->has('ticket_attachment') ? ' has-error' : '' }}">
                                        {!! Form::label('id_sub_category', 'Detail Kategori', ['class' => 'subkategori'
                                        , 'style' => 'font-weight: bold !important;']) !!} *
                                        <p style="font-size: 12px;">Silahkan pilih detail kategori pelaporan.</p>
                                        <select id="inputSelectSubCategory" class="form-control editor" name="id_sub_category">
                                        </select>
                                        <p ></p>
                                        <span class="help-block">
                                            <strong>{{ $errors->first('id_sub_category') }}</strong>
                                        </span>
                                    </div>

                                    <div class="form-group col-lg-12 {{ $errors->has('ticket_description') ? ' has-error' : '' }}">
                                        <p id="sub_category_description" style="font-size: 12px;">
                                        Uraikan kasus dugaan pelanggaran secara rinci. Apa dugaan pelanggaran yang anda ketahui, Dimana perbuatan pelanggaran tersebut dilakukan, Kapan perbuatan pelanggaran tersebut dilakukan. Siapa saja yang terlibat dalam perbuatan pelanggaran, bagaimana perbuatan pelanggaran tersebut dilakukan. 
Mohon untuk dibold seperti contoh terlampir diatas.
                                        </p>
                                        {!! Form::label('ticket_description', 'Kronologi', ['for' =>
                                        'exampleFormControlFile1' , 'style' => 'font-weight: bold !important;']) !!} *
                                        {!! Form::textarea('ticket_description', null, ['class' => 'form-control
                                        editor', 'required', 'autofocus']) !!}
                                        <span class="help-block">
                                            <strong>{{ $errors->first('ticket_description') }}</strong>
                                        </span>
                                    </div>

                                    <div class="form-group col-lg-12 {{ $errors->has('ticket_attachment') ? ' has-error' : '' }}">
                                        {!! Form::label('ticket_attachment', 'Anda dapat melampirkan file data/dokumen pendukung. Maksimum ukuran file 10 MB per input file upload.', ['for' =>
                                        'exampleFormControlFile1', 'style' => 'font-weight: bold !important;']) !!}
                                         <p>type file = jpg,gif,doc,pdf,docx,xls,xlsx,ppt,pptx,jpeg,png,mpga,mp3,mpeg,mp4,3gp </p>
                                        <div class="file-upload"> 
                                            <div class="file-select">
                                                <div class="file-select-button" >Unggah File</div>
                                                <div class="file-select-name noFile" >Belum ada file..</div> 
                                                {!! Form::file('ticket_attachment[]',  ['class' => 'exampleFormControlFile']) !!}
                                            </div>
                                        </div>
                                        <div class="hide clone">
                                            <div class="file-upload">
                                                <div class="file-select">
                                                    <div class="file-select-button" >Unggah File</div>
                                                    <div class="file-select-name noFile" >Belum ada file..</div> 
                                                    {!! Form::file('ticket_attachment[]',  ['class' => 'exampleFormControlFile']) !!}
                                                </div>
                                            </div>
                                        </div>
                                       
                                            <span class="help-block">
                                                <strong>{{ $errors->first('ticket_attachment[]') }}</strong>
                                            </span>
                                    </div>

                                    <div class="col-lg-12 text-center">
                                        <button type="submit" value="submit" class="btn submit_btn2" id = "create" style="margin-left:20px;">Simpan</button>
                                        <a href="{{ URL::previous() }}" class="btn submit_btn2">Batal</a>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT -->
            </div>
        </div>
    </div>
</section>
<!--================End Static Area =================-->
@endsection
@push('scripts')
<script src="{{asset('vendors/tinymce/jquery.tinymce.min.js')}}"></script>
<script src="{{asset('vendors/tinymce/tinymce.min.js')}}"></script>
<script>
    $(function () {
        var base_url = '{{ url("/") }}';
        // $('.selectpicker').selectpicker({
        //     style: 'btn-info',
        //     size: false
        // });
        $('#inputSelectCategory').on('change', function () {
            var value = $(this).find("option:selected").val();
            if (value != '' || value != '000') {
                $.ajax({
                    type: "GET",
                    url: base_url + '/users/getSubCategory/' + value,
                    beforeSend: function () {
                        $("#inputSelectSubCategory").prepend($('<option></option>').html(
                            'Loading ...'));
                    },
                    success: function (data) {
                        buildDropdown(jQuery.parseJSON(data), $('#inputSelectSubCategory'),
                            '--Silahkan Pilih--');
                    }
                });
            }
        });

        $('#inputSelectCategory').on('change', function () {
            var text_uraian = `Jelaskan kasus dugaan Tempat Kejadian Pelanggaran ("TKP") secara rinci.
                                            Mohon untuk mengisi alinea pertama dengan judul / nama kasus TKP yang
                                            dilaporkan.
                                            Selanjutnya silahkan diuraikan mengenai apa dugaan TKP, siapa yang diduga
                                            terlibat, di instansi mana terjadi TKP, dan kapan terjadinya TKP tersebut.`;
            var value = $(this).find("option:selected").val();
            if (value != '' || value != '000') {
                $.ajax({
                    type: "GET",
                    url: base_url + '/users/getParentCategory/' + value,
                    success: function (data) {
                        var desc = jQuery.parseJSON(data);
                        $('#category_description').html('');
                        $('#sub_category_description').html(text_uraian);
                        if(desc != null)
                        {
                            $('#category_description').append('<strong>Deskripsi: </strong> ' + desc);
                        }
                    }
                });
            }
        });
        $('#inputSelectSubCategory').on('change', function () {
            var parent_id = $('#inputSelectCategory').find("option:selected").val();
            var sub_id = $(this).find("option:selected").val();
            if (parent_id != '' || parent_id != '000' || sub_id != '' || sub_id != '000') {
                $.ajax({
                    type: "GET",
                    url: base_url + '/users/getSubCategoryDesc/' + parent_id +'/' +sub_id,
                    success: function (data) {
                        var desc = jQuery.parseJSON(data);
                        $('#sub_category_description').html('');
                        if(desc != null)
                        {
                            $('#sub_category_description').append(desc);
                        }
                    }
                });
            }
        });

        function buildDropdown(result, dropdown, emptyMessage) {
            // Remove current options
            dropdown.html('');
            //dropdown.attr('disabled', false);
            // Add the empty option with the empty message
            dropdown.append('<option value="">' + emptyMessage + '</option>');
            // Check result isnt empty
            if (result != '') {
                // Loop through each of the results and append the option to the dropdown
                $.each(result, function (k, v) {
                    dropdown.append('<option value="' + k + '">' + v + '</option>');
                });
            }
            // dropdown.selectpicker('destroy');
            // dropdown.selectpicker('refresh');
        }
        //$('.selectpicker').selectpicker('destroy');
        //$('.selectpicker').selectpicker('refresh');
    });
</script>
<script>
    $(document).on('change','.exampleFormControlFile', function () {
        var filename = $(this).val();        
        var allowedExtensions = /(\.jpg|\.gif|\.doc|\.pdf|\.docx|\.xls|\.xlsx|\.ppt|\.pptx|\.jpeg|\.png|\.mpga|\.mp3|\.mpeg|\.mp4|\.3gp)$/i;
        if (/^\s*$/.test(filename)) {
            $(this).parent().parent().removeClass('active');
        } else {
            if(!allowedExtensions.exec(filename)){
                swal({
                    title   : "Gagal!",  
                    text    : "Format dokumen anda salah ",
                    type    : "error",
                    button  : "OK!",
                });
            }
            else
            {
                $(this).parent().parent().addClass('active');
                $(this).parent().find(".noFile").text(filename.replace("C:\\fakepath\\", ""));
                var html = $('.clone').html();
                $(".hide").before(html);
            }
        }
    });
</script>
<script>
    var base_url = '{{ url("/") }}';
    var editor_config = {
        path_absolute: "/",
        selector: "textarea.editor",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            //   "searchreplace wordcount visualblocks visualchars code fullscreen",
            //   "insertdatetime media nonbreaking save table contextmenu directionality",
            //   "emoticons template paste textcolor colorpicker textpattern codesample",
            //   "fullpage toc tinymcespellchecker imagetools help"
        ],
        menu: {},
        toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent removeformat formatselect',
        // toolbar: "insertfile undo redo | styleselect | bold italic strikethrough | alignleft aligncenter alignright alignjustify | ltr rtl | bullist numlist outdent indent removeformat formatselect| link image media | emoticons charmap | code codesample | forecolor backcolor",
        //external_plugins: { "nanospell": "http://YOUR_DOMAIN.COM/js/tinymce/plugins/nanospell/plugin.js" },
        nanospell_server: "php",
        browser_spellcheck: true,
        relative_urls: false,
        remove_script_host: false,
        file_browser_callback: function (field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName(
                'body')[0].clientWidth;
            var y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName(
                'body')[0].clientHeight;

            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }
            tinymce.activeEditor.windowManager.open({
                file: '<?= route('elfinder.tinymce4') ?>', // use an absolute path!
                title: 'File manager',
                width: 900,
                height: 450,
                resizable: 'yes'
            }, {
                setUrl: function (url) {
                    win.document.getElementById(field_name).value = url;
                }
            });
        }
    };

    tinymce.init(editor_config);
</script>
<script>
    {
        !!\File::get(base_path('vendor/barryvdh/laravel-elfinder/resources/assets/js/standalonepopup.min.js')) !!
    }
</script>
<script src="{{asset('/vendors/bootstrap-sweetalerts/dist/sweetalert.min.js')}}"></script>
<script>
    $.urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if(results)
          return results[1] || 0;
    }
    $('#create').on('click', function(e){
        e.preventDefault();
        var form = e.target.form;
        var email = $("#email_guest").val();
        var parentcategory = $("#inputSelectCategory").val();
        var category = $("#inputSelectSubCategory").val();
        // var description = document.getElementById("ticket_description").val();
        var description = tinymce.activeEditor.getContent();
        if(email == '' || parentcategory == '' || category == '' || description == '')
        {
            swal({
                title   : "Gagal!",  
                text    : "Silakan lengkapi data pelaporan",
                type    : "error",
                button   : "OK!",
            });
        }
        else
        {
            form.submit();
           
        }
    })
    if ($.urlParam('type') == 'success') {
        swal({
                title: "Pelaporan berhasil dibuat!",
                text: "Terima Kasih sudah melapor",
                type: "success",
                customClass: 'sweetalert-lg',
                button: "OK!",
            },
            function (isConfirm) {
                window.location = '{{ url("/home") }}'
            });
    }
    else if($.urlParam('type') == 'fail')
    {
        swal({
        title   : "Pelaporan gagal dibuat!",  
        text    : "File yang anda upload terlalu besar. Silakan coba lagi",
        type    : "error",
        customClass: 'sweetalert-lg',
        button   : "OK!",
        },
        function(isConfirm){
            window.location = '{{ url("/create-complaint") }}'
        });
    }
</script>
@endpush
@push('style')
<style>
    .field-icon {
        float: right;
        margin-left: -25px;
        margin-top: -25px;
        position: relative;
        z-index: 2;
        margin-right: 10px;
    }

    .panel-default>.panel-heading {
        color: #ffffff;
        background-color: #0089d1;
        border-color: #0089d1;
    }
    .hide{
        visibility: hidden;
    }
    .sweet-alert { 
        margin: auto; 
        transform: translateX(-50%); 
    }
    .sweet-alert.sweetalert-lg { 
        width: 800px; 
    }

    .file-upload{display:block;text-align:center;font-family: Helvetica, Arial, sans-serif;font-size: 12px;}
    .file-upload .file-select{display:block;border: 2px solid #dce4ec;color: #34495e;cursor:pointer;height:40px;line-height:40px;text-align:left;background:#FFFFFF;overflow:hidden;position:relative;}
    .file-upload .file-select .file-select-button{background:#dce4ec;padding:0 10px;display:inline-block;height:40px;line-height:40px;}
    .file-upload .file-select .file-select-name{line-height:40px;display:inline-block;padding:0 10px;}
    .file-upload .file-select:hover{border-color:#34495e;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
    .file-upload .file-select:hover .file-select-button{background:#34495e;color:#FFFFFF;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
    .file-upload.active .file-select{border-color:#006ad6;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
    .file-upload.active .file-select .file-select-button{background:#006ad6;color:#FFFFFF;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
    .file-upload .file-select input[type=file]{z-index:100;cursor:pointer;position:absolute;height:100%;width:100%;top:0;left:0;opacity:0;filter:alpha(opacity=0);}
    .file-upload .file-select.file-select-disabled{opacity:0.65;}
    .file-upload .file-select.file-select-disabled:hover{cursor:default;display:block;border: 2px solid #dce4ec;color: #34495e;cursor:pointer;height:40px;line-height:40px;margin-top:5px;text-align:left;background:#FFFFFF;overflow:hidden;position:relative;}
    .file-upload .file-select.file-select-disabled:hover .file-select-button{background:#dce4ec;color:#666666;padding:0 10px;display:inline-block;height:40px;line-height:40px;}
    .file-upload .file-select.file-select-disabled:hover .file-select-name{line-height:40px;display:inline-block;padding:0 10px;}
</style>
@endpush
@push('scripts')
<script>
    $(".toggle-password").click(function () {

        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>
@endpush