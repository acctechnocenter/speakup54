@extends('layouts.app')

@section('content')
    {{-- <div class="static_area">
        <div class="container">
            <div class="row">
                asdasdas
            </div>
        </div>
    </div> --}}
    <div class="static_area">
        <div class="container" style="padding-top: 2em; height:65vh;">
            <h3 style="border-bottom-color: #0072BB; border-bottom-style: solid; padding-bottom: .5em; margin-bottom: 2em">Media Pelaporan</h3>
            <p class="normal-paragraph">
            Untuk anda yang ingin melaporkan indikasi pelanggaran yang dilakukan oleh karyawan ACC, anda dapat menyampaikan melalui sarana : 
            <br><br>
- Website            : www.accwhistle.acc.co.id
<br>
- Email                 : ronggur.saragih@acc.co.id
<br>
- Pos atau surat yang ditujukan kepada  : 
<br><br>
Bpk. Ronggur Saragih <br>
Astra Credit Companies <br>
Jl TB Simatupang No.90 <br>
Jakarta Selatan <br><br>
            </p>
        </div>
    </div>
@endsection