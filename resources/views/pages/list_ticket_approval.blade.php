@extends('layouts.app')

@section('content')
    <!--================Banner Area =================-->
    <section class="banner_area">
            <div class="container">
                <div class="banner_text_inner">
                    <h4>Daftar Pelaporan Approval</h4>
					<h6 style="color:#ffffff">Pantau selalu tiket pelaporan mu</h6>
                </div>
            </div>
        </section>
        <!--================End Banner Area =================-->
		
		<section class="main_slider_area">
			<div class="copy_right_area_top">
  
            </div>
		</section>
		
        <!--================Static Area =================-->
        <section class="static_area">
            <div class="container">
                <div class="static_inner">
                    <div class="row">
						<div class="col-lg-3">
								<ul class="ca-menu">
									<li>
										<a href="{{url('/administrator/report')}}">
											<i class="fa fa fa-key ca-icon" aria-hidden="true"></i>
											<div class="ca-content">
												<h5 class="ca-main">Administrator</h5>
												<h6 class="ca-sub">Menu Administrator</h6>
											</div>
										</a>
									</li>
									<li>
										<a href="{{url('/users/list-complaint-approval')}}">
											<i class="fa fa fa-key ca-icon" aria-hidden="true"></i>
											<div class="ca-content">
												<h5 class="ca-main">List Tiket Approval</h5>
												<h6 class="ca-sub">List tiket yang perlu Approval</h6>
											</div>
										</a>
									</li>
								</ul>
                        </div>
						
                        <!-- CONTENT -->
						
						
						<div class="col-lg-9">
                            <div class="static_main_content">
								<div class="main_title text-center">
								@if (session('status'))
								<div class="alert alert-danger">	
									{{ session('status') }}
								</div>
								@elseif(session('work'))
								<div class="alert alert-success">	
									{{ session('work') }}
								</div>
								@endif
									<!-- <h3>List Compliance</h3>
								</div>-->
								<br><br>
								<div class="table-responsive">
									<table id="list-complaint-table-for-approval" class="table table-bordered">
										<thead>
											<tr>
                                                                                              <th>No</th>
												<th>ID Tiket</th>
												<th>Tanggal Pelaporan</th>
												<th>Kategori</th>
												<th>Sub Kategori</th>
												<th>SLA</th>
												<th>Status</th>
												<th>Aksi</th>
											</tr>
										</thead>
									</table>
								</div>
                            </div>
                        </div>
                        <!-- END CONTENT -->
                    </div>
                </div>
            </div>
        </section>
        <!--================End Static Area =================-->
@endsection
@push('scripts')
@include('pages.detail_template')
    <script>
        //ambil data untuk induk
        $(function() {
			var template = Handlebars.compile($("#details-template").html());
            var table = $('#list-complaint-table-for-approval').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ url('/users/complaint/data/approval') }}",
                order: [[ 0, "desc" ]],
                columns: [
                 { data: 'id', name: 'id', width: "2%" },
                    { data: 'id_ticket', name: 'id_ticket', width: "15%" },
					{ data: 'date_ticket', name: 'date_ticket', width: "15%" },
                    { data: 'category_name', name: 'category_name' , width: "20%"},
					{ data: 'sub_category_name', name: 'sub_category_name', width: "20%" },
					{ data: 'sla_terbaru', name: 'sla_terbaru', width: "10%" },
					{ data: 'ticket_status', name: 'ticket_status', width: "10%" },
					{ data: 'action' , name : 'action', width: "20%", orderable: false }
                ],
                       columnDefs: [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
        ]
            });
		});
	</script>
@endpush