@extends('layouts.app')

@section('content')
    <!--================Banner Area =================-->
    <section class="banner_area">
        <div class="container">
            <div class="banner_text_inner">
                <h4>Frequently asked questions </h4>
                <h6 style="color: #FFFFFF;"> Berikut adalah beberapa pertanyaan yang sering di tanyakan oleh pengguna Speak Up</h6>
            </div>
        </div>
    </section>
        <!--================End Banner Area =================-->
		
		<section class="main_slider_area">
			<div class="copy_right_area_top">
  
            </div>
		</section>

       <!--================Static Area =================-->
       <section class="latest_news_area p_100">
        <div class="container ">
          
            <div class="row">
                <div class="col-sm-12">
                <br>
                     <div class="panel-group" >
                        @forelse ($faq as $key => $faq_content)
                            <div class="panel panel-default ">
                                <div class="panel-heading ">
                                    <h4 class="panel-title">
                                        <a href="#">{!! $faq_content->faqs_question !!}</a>
                                    </h4>
                                </div>
                                <div id="question{{$key}}" class="panel">
                                    <div class="panel-body">
                                        {!! $faq_content->answers_question !!}
                                    </div>
                                </div>
                            </div>
                        @empty
                            <p>EMPTY BLOG</p>
                        @endforelse
                    </div>
                </div>
                <div class="col-sm-2"></div>
            </div>
      
                <!--/panel-group-->
            </div>
        </section>
       
        <!--================End Static Area =================-->
@endsection
@push('style')
        <style>
        .field-icon {
            float: right;
            margin-left: -25px;
            margin-top: -25px;
            position: relative;
            z-index: 2;
            margin-right:10px;
        }
        .panel-default > .panel-heading {
            color: #ffffff;
            background-color: #0089d1;
            border-color: #0089d1;
        }
        </style>
@endpush