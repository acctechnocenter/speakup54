@extends('layouts.app')

@section('content')
    <!--================Banner Area =================-->
    <section class="banner_area">
            <div class="container">
                <div class="banner_text_inner">
                    <h4>Form Pelaporan</h4>
                    <h6 style="color:#ffffff">
                    Anda tidak perlu khawatir terungkapnya identitas diri anda karena ACC WHISTLE akan <b>MERAHASIAKAN IDENTITAS DIRI ANDA</b> sebagai whistleblower. ACC Whistle menghargai informasi yang Anda laporkan. Fokus kami hanya kepada materi informasi yang Anda Laporkan.
                    </h6>

                </div>
            </div>
        </section>
        <!--================End Banner Area =================-->
		
		<section class="main_slider_area">
			<div class="copy_right_area_top">
  
            </div>
		</section>

        <!--================Static Area =================-->
        <section class="static_area">
            <div class="container">
                <div class="static_inner">
                    <div class="row">
						<div class="col-lg-3">
                            <ul class="ca-menu">
                                <li>
                                    <a href="{{url('/users/create-complaint')}}">
                                        <i class="fa fa-pencil-square-o ca-icon" aria-hidden="true"></i>
                                        <div class="ca-content">
                                            <h5 class="ca-main">Buat Pelaporan</h5>
                                            <h6 class="ca-sub">Menu Pelaporan</h6>
                                        </div>
                                    </a>
                                </li>
                                @if(Auth::user()->group != 'administrator')
                                <li>
                                    <a href="{{url('/users/list-complaint')}}">
                                        <i class="fa fa fa-table ca-icon" aria-hidden="true"></i>
                                        <div class="ca-content">
                                            <h5 class="ca-main">Daftar Pelaporan</h5>
											<h6 class="ca-sub">Menu laporan mu</h6>
                                        </div>
                                    </a>
                                </li>
                                @endif
                                @if(Auth::user()->group == 'pic')
                                <li>
                                    <a href="{{url('/users/list-complaint-pic')}}">
                                        <i class="fa fa fa-key ca-icon" aria-hidden="true"></i>
                                        <div class="ca-content">
                                            <h5 class="ca-main">Daftar Pelaporan  PIC</h5>
											<h6 class="ca-sub">Menu Laporan PIC</h6>
                                        </div>
                                    </a>
                                </li>
                                @endif
                                @if(Auth::user()->group == 'administrator')
                                <li>
                                    <a href="{{url('/administrator/report')}}">
                                        <i class="fa fa fa-key ca-icon" aria-hidden="true"></i>
                                        <div class="ca-content">
                                            <h5 class="ca-main">Administrator</h5>
                                            <h6 class="ca-sub">Menu Administrator</h6>
                                        </div>
                                    </a>
                                </li>
                                @endif
                            </ul>
                        </div>
                        <!-- CONTENT -->
                        <div class="col-lg-9">
                            <div class="static_main_content">
								<div class="contact_form">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Form Pelaporan</div>
                                            <div class="panel-body">
                                                <!-- <div class="main_title text-center">
                                                    <h3>Create your Compliance</h3>
                                                </div> -->
                                                {!! Form::open(['url' => '/users/create-complaint/save', 'role' => 'form', 'enctype' => 'multipart/form-data', 'novalidate']) !!}
                                                    <div class="form-group col-lg-12 {{ $errors->has('email') ? ' has-error' : '' }}">
                                                        {!! Form::label('email', 'Email', ['for' => 'email', 'style' => 'font-weight: bold !important;']) !!} *
                                                        {!! Form::text('email', Auth::user()->email, ['class' => 'form-control', 'required', 'readonly']) !!}
                                                    </div>

                                                    <div class="form-group col-lg-12 {{ $errors->has('ticket_attachment') ? ' has-error' : '' }}">
                                                        {!! Form::label('id_category', 'Kategori', ['for' => 'kategori', 'style' => 'font-weight: bold !important;']) !!} *
                                                        <p style="font-size: 12px;">Silahkan pilih kategori pelaporan.</p>
                                                        <select id="inputSelectCategory" class="form-control editor" name="id_category">
                                                        <option value="">--Silahkan Pilih--</option>
                                                            @foreach($parentcategories as $category)
                                                                <option value={{$category->id}}>{{ $category->category_name }}</option>
                                                            @endforeach
                                                        </select>
                                                        <p id="category_description"></p>
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('id_category') }}</strong>
                                                        </span>
                                                    </div>

                                                    <div class="form-group col-lg-12 {{ $errors->has('ticket_attachment') ? ' has-error' : '' }}">
                                                        {!! Form::label('id_sub_category', 'Detail Kategori', ['class' => 'subkategori' , 'style' => 'font-weight: bold !important;']) !!}  *
                                                        <p style="font-size: 12px;">Silahkan pilih detail kategori pelaporan.</p>
                                                        <select id="inputSelectSubCategory" class="form-control editor" name="id_sub_category">
                                                        </select>
                                                        <p ></p>
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('id_sub_category') }}</strong>
                                                        </span>
                                                    </div>

                                                    <div class="form-group col-lg-12 {{ $errors->has('ticket_description') ? ' has-error' : '' }}">
                                                        <p id="sub_category_description" style="font-size: 12px;">
                                                        Uraikan kasus dugaan pelanggaran secara rinci. Apa dugaan pelanggaran yang anda ketahui, Dimana perbuatan pelanggaran tersebut dilakukan, Kapan perbuatan pelanggaran tersebut dilakukan. Siapa saja yang terlibat dalam perbuatan pelanggaran, bagaimana perbuatan pelanggaran tersebut dilakukan. 
Mohon untuk dibold seperti contoh terlampir diatas.
                                                        </p>
                                                        {!! Form::label('ticket_description', 'Kronologi', ['for' => 'exampleFormControlFile1' , 'style' => 'font-weight: bold !important;']) !!} *
                                                        {!! Form::textarea('ticket_description', null, ['class' => 'form-control editor', 'required', 'autofocus']) !!}
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('ticket_description') }}</strong>
                                                        </span>
                                                    </div>

                                                    <div class="form-group col-lg-12 {{ $errors->has('ticket_attachment') ? ' has-error' : '' }}">
                                                        <!-- {!! Form::label('ticket_attachment', 'File Lampiran', ['for' => 'exampleFormControlFile1', 'style' => 'font-weight: bold !important;']) !!}
                                                        {!! Form::file('ticket_attachment',  ['class' => 'form-control-file', 'id' => 'exampleFormControlFile1']) !!}
                                                        <p style="font-size: 12px;">Anda dapat melampirkan file data/dokumen pendukung. Maksimum ukuran file 10 MB.</p>
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('ticket_attachment') }}</strong>
                                                        </span> -->
                                                        {!! Form::label('ticket_attachment', 'Anda dapat melampirkan file data/dokumen pendukung. Maksimum ukuran file 10 MB per input file upload.', ['for' => 'exampleFormControlFile1', 'style' => 'font-weight: bold !important;']) !!}
                                                        <div class="file-upload"> 
                                                            <div class="file-select">
                                                                <div class="file-select-button" >Unggah File</div>
                                                                <div class="file-select-name noFile" >Belum ada file..</div> 
                                                                {!! Form::file('ticket_attachment[]',  ['class' => 'exampleFormControlFile']) !!}
                                                            </div>
                                                        </div>
                                                        <div class="hide clone">
                                                            <div class="file-upload">
                                                                <div class="file-select">
                                                                    <div class="file-select-button" >Unggah File</div>
                                                                    <div class="file-select-name noFile" >Belum ada file..</div> 
                                                                    {!! Form::file('ticket_attachment[]',  ['class' => 'exampleFormControlFile']) !!}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('ticket_attachment[]') }}</strong>
                                                        </span>
                                                    </div>

										<div class="col-lg-12 text-center">
											<button type="submit" value="submit" class="btn submit_btn2" id = "create" style="margin-left:20px;">Laporkan</button>
                                            <a href="{{ URL::previous() }}" class="btn submit_btn2">Batal</a> 
										</div>
									{!! Form::close() !!}
								</div>
                            </div>
                        </div>
                        <!-- END CONTENT -->
                    </div>
                </div>
            </div>
        </section>
        <!--================End Static Area =================-->
@endsection
@push('scripts')
<script src="{{asset('vendors/tinymce/jquery.tinymce.min.js')}}"></script>
<script src="{{asset('vendors/tinymce/tinymce.min.js')}}"></script>
<script>
$(function() {
   var base_url = '{{ url("/") }}';
   // $('.selectpicker').selectpicker({
   //     style: 'btn-info',
   //     size: false
   // });
   $('#inputSelectCategory').on('change', function(){
       var value = $(this).find("option:selected").val();
       if(value != '' || value != '000'){
           $.ajax({
               type: "GET",
               url: base_url + '/users/getSubCategory/'+value,
               beforeSend: function() {
                    $("#inputSelectSubCategory").prepend($('<option></option>').html('Loading ...'));
               },
               success: function(data){
                       buildDropdown(jQuery.parseJSON(data),$('#inputSelectSubCategory'),'--Silahkan Pilih--');
               }
           });
       }
       console.log(value);  
   });
   $('#inputSelectCategory').on('change', function () {
            var value = $(this).find("option:selected").val();
            var text_uraian = `Jelaskan kasus dugaan Tempat Kejadian Pelanggaran ("TKP") secara rinci. Mohon untuk mengisi alinea pertama dengan judul / nama kasus TKP yang dilaporkan.
                                                        Selanjutnya silahkan diuraikan mengenai apa dugaan TKP, siapa yang diduga terlibat, di instansi mana terjadi TKP, dan kapan terjadinya TKP tersebut.`;
            console.log(value);
            if (value != '' || value != '000') {
                $.ajax({
                    type: "GET",
                    url: base_url + '/users/getParentCategory/' + value,
                    success: function (data) {
                        var desc = jQuery.parseJSON(data);
                        $('#category_description').html('');
                        $('#sub_category_description').html(text_uraian);
                        if(desc != null)
                        {
                            $('#category_description').append('<strong>Deskripsi: </strong> ' + desc);
                        }
                    }
                });
            }
        });

        $('#inputSelectSubCategory').on('change', function () {
            var parent_id = $('#inputSelectCategory').find("option:selected").val();
            var sub_id = $(this).find("option:selected").val();
            if (parent_id != '' || parent_id != '000' || sub_id != '' || sub_id != '000') {
                $.ajax({
                    type: "GET",
                    url: base_url + '/users/getSubCategoryDesc/' + parent_id +'/' +sub_id,
                    success: function (data) {
                        var desc = jQuery.parseJSON(data);
                        $('#sub_category_description').html('');
                        if(desc != null)
                        {
                            $('#sub_category_description').append(desc);
                        }
                    }
                });
            }
        });

   function buildDropdown(result, dropdown, emptyMessage) {
       // Remove current options
       dropdown.html('');
       //dropdown.attr('disabled', false);
       // Add the empty option with the empty message
       dropdown.append('<option value="">' + emptyMessage + '</option>');
       // Check result isnt empty
       if(result != '')
       {
           // Loop through each of the results and append the option to the dropdown
           $.each(result, function(k, v) {
               dropdown.append('<option value="' + k + '">' + v + '</option>');
           });
       }
       // dropdown.selectpicker('destroy');
       // dropdown.selectpicker('refresh');
   }
   //$('.selectpicker').selectpicker('destroy');
   //$('.selectpicker').selectpicker('refresh');
});
</script>
<script>
    $(document).on('change','.exampleFormControlFile', function () {
        var filename = $(this).val();
        var allowedExtensions = /(\.jpg|\.gif|\.doc|\.pdf|\.docx|\.xls|\.xlsx|\.ppt|\.pptx|\.jpeg|\.png|\.mpga|\.mp3|\.mpeg|\.mp4|\.3gp)$/i;
        if (/^\s*$/.test(filename)) {
            $(this).parent().parent().removeClass('active');
        } else {
            if(!allowedExtensions.exec(filename)){
                swal({
                    title   : "Gagal!",  
                    text    : "Format dokumen anda salah ",
                    type    : "error",
                    button  : "OK!",
                });
            }
            else
            {
                $(this).parent().parent().addClass('active');
                $(this).parent().find(".noFile").text(filename.replace("C:\\fakepath\\", ""));
                var html = $('.clone').html();
                $(".hide").before(html);
            }
        }
    });
</script>
<script>
    var base_url = '{{ url("/") }}';
    var editor_config = {
        path_absolute : "/",
        selector: "textarea.editor",
        plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        //   "searchreplace wordcount visualblocks visualchars code fullscreen",
        //   "insertdatetime media nonbreaking save table contextmenu directionality",
        //   "emoticons template paste textcolor colorpicker textpattern codesample",
        //   "fullpage toc tinymcespellchecker imagetools help"
        ],
        menu: {},
        toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent removeformat formatselect',
        // toolbar: "insertfile undo redo | styleselect | bold italic strikethrough | alignleft aligncenter alignright alignjustify | ltr rtl | bullist numlist outdent indent removeformat formatselect| link image media | emoticons charmap | code codesample | forecolor backcolor",
        //external_plugins: { "nanospell": "http://YOUR_DOMAIN.COM/js/tinymce/plugins/nanospell/plugin.js" },
        nanospell_server:"php",
        browser_spellcheck: true,
        relative_urls: false,
        remove_script_host: false,
        file_browser_callback : function(field_name, url, type, win) {
        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
        if (type == 'image') {
            cmsURL = cmsURL + "&type=Images";
        } else {
            cmsURL = cmsURL + "&type=Files";
        }
        tinymce.activeEditor.windowManager.open({
            file: '<?= route('elfinder.tinymce4') ?>',// use an absolute path!
            title: 'File manager',
            width: 900,
            height: 450,
            resizable: 'yes'
        }, {
            setUrl: function (url) {
            win.document.getElementById(field_name).value = url;
            }
        });
        }
    };

    tinymce.init(editor_config);
</script>
<script>
  {!! \File::get(base_path('vendor/barryvdh/laravel-elfinder/resources/assets/js/standalonepopup.min.js')) !!}
</script>
<script src="{{asset('/vendors/bootstrap-sweetalerts/dist/sweetalert.min.js')}}"></script>
<script>
    $.urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if(results)
        return results[1] || 0;
    }
    $('#create').on('click', function(e){
        e.preventDefault();
        var form = e.target.form;
        var parentcategory = $("#inputSelectCategory").val();
        var category = $("#inputSelectSubCategory").val();
        // var description = document.getElementById("ticket_description").val();
        var description = tinymce.activeEditor.getContent();

        if(parentcategory == '' || category == '' || description == '')
        {
            swal({
                title   : "Gagal!",  
                text    : "Silakan lengkapi data pelaporan",
                type    : "error",
                button   : "OK!",
            });
        }
        else
        {
            form.submit();
        }
    })
    if($.urlParam('type') == 'success')
    {
        swal({
        title   : "Pelaporan berhasil dibuat!",  
        text    : "Terima Kasih sudah melapor",
        type    : "success",
        customClass: 'sweetalert-lg',
        button   : "OK!",
        },
        function(isConfirm){
            window.location = '{{ url("/users/list-complaint") }}'
        });
    }
    else if($.urlParam('type') == 'fail')
    {
        swal({
        title   : "Pelaporan gagal dibuat!",  
        text    : "File yang anda upload terlalu besar. Silakan coba lagi",
        type    : "error",
        customClass: 'sweetalert-lg',
        button   : "OK!",
        },
        function(isConfirm){
            window.location = '{{ url("/users/create-complaint") }}'
        });
    }
</script>
@endpush
@push('style')
        <style>
        .field-icon {
            float: right;
            margin-left: -25px;
            margin-top: -25px;
            position: relative;
            z-index: 2;
            margin-right:10px;
        }
        .panel-default > .panel-heading {
            color: #ffffff;
            background-color: #0089d1;
            border-color: #0089d1;
        }
        .hide{
            visibility: hidden;
        }
        .sweet-alert { 
            margin: auto; 
            transform: translateX(-50%); 
        }
        .sweet-alert.sweetalert-lg { 
            width: 800px; 
        }

        .file-upload{display:block;text-align:center;font-family: Helvetica, Arial, sans-serif;font-size: 12px;}
        .file-upload .file-select{display:block;border: 2px solid #dce4ec;color: #34495e;cursor:pointer;height:40px;line-height:40px;text-align:left;background:#FFFFFF;overflow:hidden;position:relative;}
        .file-upload .file-select .file-select-button{background:#dce4ec;padding:0 10px;display:inline-block;height:40px;line-height:40px;}
        .file-upload .file-select .file-select-name{line-height:40px;display:inline-block;padding:0 10px;}
        .file-upload .file-select:hover{border-color:#34495e;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
        .file-upload .file-select:hover .file-select-button{background:#34495e;color:#FFFFFF;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
        .file-upload.active .file-select{border-color:#006ad6;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
        .file-upload.active .file-select .file-select-button{background:#006ad6;color:#FFFFFF;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
        .file-upload .file-select input[type=file]{z-index:100;cursor:pointer;position:absolute;height:100%;width:100%;top:0;left:0;opacity:0;filter:alpha(opacity=0);}
        .file-upload .file-select.file-select-disabled{opacity:0.65;}
        .file-upload .file-select.file-select-disabled:hover{cursor:default;display:block;border: 2px solid #dce4ec;color: #34495e;cursor:pointer;height:40px;line-height:40px;margin-top:5px;text-align:left;background:#FFFFFF;overflow:hidden;position:relative;}
        .file-upload .file-select.file-select-disabled:hover .file-select-button{background:#dce4ec;color:#666666;padding:0 10px;display:inline-block;height:40px;line-height:40px;}
        .file-upload .file-select.file-select-disabled:hover .file-select-name{line-height:40px;display:inline-block;padding:0 10px;}

        @media (max-width: 768px) {
        .sweet-alert.sweetalert-lg {
            width: 352px;
        }

        .sweet-alert {
            width: auto;
            margin-left: 0;
            margin-right: 0;
            left: 199px;
            right: 15px;
        }
    
    }
        </style>
@endpush