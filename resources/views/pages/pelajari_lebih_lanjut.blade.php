@extends('layouts.app')

@section('content')
    <div class="static_area" id="tentang_acc">
        <div class="container text-justify" style="padding-top: 1.2em">
            <h3 style="border-bottom-color: #0072BB; border-bottom-style: solid; padding-bottom: .5em; margin-bottom: 2em">Tentang ACC Whistle</h3>
            <p>
                Perusahaan senantiasa menerapkan prinsip-prinsip tata kelola Perusahaan yang baik (Good
                Corporate Governance) secara konsisten dan berkelanjutan. Dalam menjalankan kegiatan
                Perusahaan, setiap Insan ACC dituntut untuk melaksanakan kegiatan usahanya dengan penuh
                tanggung jawab, transparan dan akuntabel, serta dengan menghindari aktifitas/kegiatan
                yang mengarah kepada tindakan yang tidak beretika atau melanggar pedoman perilaku, dan
                benturan kepentingan.
            </p>
            <p>
                Sebagai wujud komitmen Perusahaan terhadap implementasi tata kelola Perusahaan yang
                baik (Good Corporate Governance), dan dalam rangka mencegah dan melakukan deteksi dini
                atas pelanggaran yang mungkin terjadi di lingkungan Perusahaan, maka Perusahaan
                memandang penting adanya suatu sarana pelaporan pelanggaran.
            </p>
            <br>
            <p>
                <strong>ACC Whistle</strong> adalah suatu sarana bagi karyawan, mitra bisnis / pemangku
                kepentingan dan pelanggan yang mudah diakses dan berfokus pada pembentukan budaya
                pelaporan yang didasari rasa aman dan rahasia serta memastikan seluruh pelaporan yang
                disampaikan, ditangani dengan baik.
            </p>
            <p>
                Terdiri dari :
                <ul>
                    <li>Corporate Compliance</li>
                    <li>Fraud Risk Management</li>
                    <li>Human Capital (Industrial Relation)</li>
                    <li>Corporate Communication</li>
                </ul>
            </p>
        </div>
    </div>
    <div class="static_area">
        <div class="container text-justify" style="padding-top: 1.2em">
            <h3 style="border-bottom-color: #0072BB; border-bottom-style: solid; padding-bottom: .5em; margin-bottom: 2em">Tujuan ACC Whistle</h3>
            <ol class="tujuan_acc">
                <li>
                  <span>
                      Memberdayakan karyawan, mitra bisnis dan pelanggan untuk berperan serta dalam upaya penegakan Kode Etik Perilaku Karyawan, Peraturan Perusahaan, maupun hukum yang berlaku di Indonesia.
                  </span>
                </li>
                <li>
                  <span>
                      Menyediakan berbagai sarana untuk pertanyaan, pelaporan dan masukan serta saluran umpan balik yang dapat dipercaya oleh semua pihak.
                  </span>
                </li>
                <li>
                    <span>
                        Menyediakan sarana bagi karyawan jika pelaporan berjenjang kepada atasan langsung dirasa tidak memungkinkan bagi karyawan.
                    </span>
                </li>
                <li>
                    <span>Mendapatkan informasi dari karyawan, mitra bisnis dan pelanggan mengenai adanya potensi, atau rencana, atau kejadian kasus pelanggaran Fraud, Etika Bisnis, Pedoman Perilaku, Peraturan Perusahaan dan Peraturan Perundangan-undangan yang memerlukan pencegahan maupun penanganan sesegera mungkin.</span>
                </li>
            </ol>
        </div>
    </div>
    <div class="static_area" id="ruang_lingkup">
        <div class="container text-justify" style="padding-top: 1.2em">
            <h3 style="border-bottom-color: #0072BB; border-bottom-style: solid; padding-bottom: .5em; margin-bottom: 2em">Ruang Lingkup Pelapor dan Pengaduan</h3>
         <p>
               Sebagai bentuk perlindungan bagi individu yang melakukan pelaporan, maka berikut ini adalah ketentuan yang berlaku bagi  individu yang menyampaikan                 pertanyaan / pelaporan :</p>
<br>
              <ol class="tujuan_acc">
<li>Tidak diperkenankan menyampaikan isi pelaporan kepada pihak lain selain petugas yang ditunjuk untuk melakukan investigasi lanjutan atas pelaporan yang disampaikan. </li>
<li>Tidak akan disebutkan identitasnya sebagai pelapor dalam proses pembahasan tindak lanjut laporan. </li>
<li>Bisa dipanggil sebagai saksi/narasumber untuk keperluan investigasi atau tindak lanjut atas keputusan yang diambil atas hasil investigasi. </li>
<li>Berhak mendapatkan perlindungan dari Perusahaan bila akibat dari pelaporannya mengakibatkan sesuatu hal yang mengancam keberadaan Pelapor di dalam maupun di luar perusahaan. </li>
<li>Pelapor akan mendapatkan perlindungan dari perusahaan terhadap perlakuan yang merugikan seperti : </li>
	  <ol class="tujuan_acc">
                 <li>Pemecatan yang tidak adil. </li>
	<li>Penurunan jabatan. </li>
	<li>Pelecehan atau diskriminasi dalam segala bentuknya. </li></ol>
<li>Dibebaskan dari segala sanksi bila ternyata laporannya tidak dapat dibuktikan kebenarannya.  <br>

            </li></ol>
  Dengan poin-poin  tersebut di atas, pihak pelapor diharapkan bersedia untuk memberikan informasi yang lebih terperinci kepada manajemen terkait tindak lanjut atas laporan mereka. <br><br>
              
            <div class="row">
              <div class="col-md-6">
                  <ul class="list-group">
                      <li class="list-group-item active">Pihak Pelapor</li>
                      <li class="list-group-item">Karyawan</li>
                      <li class="list-group-item">Pelanggan / dealer</li>
                      <li class="list-group-item">Pemasok</li>
                      <li class="list-group-item">Penyalur</li>
                      <li class="list-group-item">Media Massa</li>
                      <li class="list-group-item">Perusahaan Afiliasi</li>
                      <li class="list-group-item">Pemegang Saham</li>
                    </ul>
              </div>
              <div class="col-md-6">
                  <ul class="list-group">
                    <li class="list-group-item list-group-item-secondary">Fraud</li>
                    <li class="list-group-item">Penggelapan</li>
                    <li class="list-group-item">Pencurian</li>
                    <li class="list-group-item">Penipuan</li>
                    <li class="list-group-item">Penyuapan</li>
                  </ul>
                  <br>
                  <ul class="list-group">
                    <li class="list-group-item list-group-item-secondary">Etika Kerja & Etika Bisnis</li>
                    <li class="list-group-item">Diskriminasi</li>
                    <li class="list-group-item">Komitmen dengan pemangku kepentingan</li>
                    <li class="list-group-item">Konflik Kepentingan</li>
                    <li class="list-group-item">Pelecehan</li>
                    <li class="list-group-item">Pemberian atau penerimaan imbalan bayaran maupun bingkisan yang tidak wajar</li>
                  </ul>
                  <br>
                  <ul class="list-group">
                    <li class="list-group-item list-group-item-secondary">Peraturan Perusahan</li>
                    <li class="list-group-item">Citra Perusahaan</li>
                    <li class="list-group-item">Hubungan antar karyawan</li>
                    <li class="list-group-item">Kesejahteraan</li>
                    <li class="list-group-item">Komunikasi</li>
                    <li class="list-group-item">Lingkungan kerja</li>
                    <li class="list-group-item">Manajemen</li>
                    <li class="list-group-item">Pekerjaan</li>
                    <li class="list-group-item">Pengembangan karyawan</li>
                    <li class="list-group-item">Penghasilan</li>
                  </ul>
                  <br>
                  <ul class="list-group">
                  <li class="list-group-item list-group-item-secondary">Pelanggaran penggunaan media sosial</li>
                    <li class="list-group-item">Provokasi pandangan politik / kampanye politik)</li>
                    <li class="list-group-item">Publikasi pornografi</li>
                    <li class="list-group-item">Publikasi rahasia perusahaan</li>
                    <li class="list-group-item">Publikasi SARA</li>
                  </ul>
              </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script>
$(document).ready(function (){
  var element = '{{$Element}}';
  if(element == 'tentang-acc-whistle'){
    $('html, body').animate({
      scrollTop: $("#tentang_acc").offset().top
    }, 2000);
  }
  else if(element == 'ruang_lingkup')
  {
    $('html, body').animate({
      scrollTop: $("#ruang_lingkup").offset().top
    }, 2000);
  }
});
</script>
@endpush
@push('style')
<style>
  .tujuan_acc li span{
  position: relative; left: 20px;
}
</style>
@endpush