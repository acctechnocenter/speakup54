@extends('layouts.app')

@section('content')
    <!--================Banner Area =================-->
    <section class="banner_area">
            <div class="container">
                <div class="banner_text_inner">
                    <h4>Daftar Pelaporan</h4>
					<h6 style="color:#ffffff">Pantau selalu tiket pelaporan mu</h6>
                </div>
            </div>
        </section>
        <!--================End Banner Area =================-->
		
		<section class="main_slider_area">
			<div class="copy_right_area_top">
  
            </div>
		</section>
		
        <!--================Static Area =================-->
        <section class="static_area">
            <div class="container">
                <div class="static_inner">
                    <div class="row">
						<div class="col-lg-3">
								<ul class="ca-menu">
									@if(Auth::user()->group != 'authorizer')
									<li>
										<a href="{{url('/users/create-complaint')}}">
											<i class="fa fa-pencil-square-o ca-icon" aria-hidden="true"></i>
											<div class="ca-content">
												<h5 class="ca-main">Buat Pelaporan</h5>
												<h6 class="ca-sub">Buat laporan mu</h6>
											</div>
										</a>
									</li>
									@endif
									@if(Auth::user()->group != 'administrator')
									<li>
										<a href="{{url('/users/list-complaint')}}">
											<i class="fa fa fa-table ca-icon" aria-hidden="true"></i>
											<div class="ca-content">
												<h5 class="ca-main">Daftar Pelaporan</h5>
												<h6 class="ca-sub">Daftar laporan mu</h6>
											</div>
										</a>
									</li>
                                    @endif
									@if(Auth::user()->group == 'pic')
									<li>
										<a href="{{url('/users/list-complaint-pic')}}">
											<i class="fa fa fa-key ca-icon" aria-hidden="true"></i>
											<div class="ca-content">
												<h5 class="ca-main">Daftar Pelaporan PIC</h5>
												<h6 class="ca-sub">Daftar Laporan PIC</h6>
											</div>
										</a>
									</li>
									@endif
									@if(Auth::user()->group == 'administrator')
									<li>
										<a href="{{url('/administrator/report')}}">
											<i class="fa fa fa-key ca-icon" aria-hidden="true"></i>
											<div class="ca-content">
												<h5 class="ca-main">Administrator</h5>
												<h6 class="ca-sub">Menu Administrator</h6>
											</div>
										</a>
									</li>
									@endif
								</ul>
                        </div>
						
                        <!-- CONTENT -->
						
						
						<div class="col-lg-9">
                            <div class="static_main_content">
								<div class="main_title text-center">
								@if (session('status'))
								<div class="alert alert-danger">	
									{{ session('status') }}
								</div>
								@elseif(session('work'))
								<div class="alert alert-success">	
									{{ session('work') }}
								</div>
								@endif
									<!-- <h3>List Compliance</h3>
								</div>-->
								<br><br>
								<div class="table-responsive">
									<table id="list-complaint-table" class="table table-bordered responsive">
										<thead>
											<tr>
												<th>ID Tiket</th>
												<th>Tanggal Pelaporan</th>
												<th>Kategori</th>
												<th>Sub Kategori</th>
												<th>SLA</th>
												<th>Status</th>
												<th>Aksi</th>
											</tr>
										</thead>
									</table>
								</div>
                            </div>
                        </div>
                        <!-- END CONTENT -->
                    </div>
                </div>
            </div>
        </section>
        <!--================End Static Area =================-->
@endsection
@push('scripts')
@include('pages.detail_template')
{{-- <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script> --}}
    <script>
        //ambil data untuk induk
        $(document).ready(function() {
			var template = Handlebars.compile($("#details-template").html());
            var table = $('#list-complaint-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ url('/users/complaint/data') }}",
                columns: [
                    { data: 'id_ticket', name: 'id_ticket', width: "15%" },
					{ data: 'date_ticket', name: 'date_ticket', width: "15%" },
                    { data: 'category_name', name: 'category_name' , width: "20%"},
					{ data: 'sub_category_name', name: 'sub_category_name', width: "20%" },
					{ data: 'sla_terbaru', name: 'sla_terbaru', width: "10%" },
					{ data: 'status', name: 'status', width: "10%" },
					{ data: 'action' , name : 'action', width: "20%", orderable: false }
                ]
            });
		});
	</script>
@endpush