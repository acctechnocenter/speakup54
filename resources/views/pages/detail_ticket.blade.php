@extends('layouts.app')

@section('content')
    <!--================Banner Area WRAP=================-->
    <section class="banner_area">
            <div class="container">
                <div class="banner_text_inner">
                    <h4>Detail Tiket {{$ticket->id_ticket}}</h4>
                    <h6 style="color:#ffffff">
                    @if(!empty($parentcaegoriesname) OR !empty($subcategoriesname))
                        {{$parentcategoriesname->category_name}} - {{$subcategoriesname->sub_category_name}}
                    @endif
                    </h6>
                </div>
            </div>
    </section>
        <!--================End Banner Area =================-->
		
		<section class="main_slider_area">
			<div class="copy_right_area_top">
  
            </div>
		</section>

        <!--================Static Area =================-->
        <section class="static_area">
            <div class="container">
                <div class="static_inner">
                    <div class="row">
						<div class="col-lg-3">
								<ul class="ca-menu">
									<li>
										<a href="{{url('/users/create-complaint')}}">
											<i class="fa fa-pencil-square-o ca-icon" aria-hidden="true"></i>
											<div class="ca-content">
                                                <h5 class="ca-main">Buat Pelaporan</h5>
                                                <h6 class="ca-sub">Menu Pelaporan</h6>
											</div>
										</a>
									</li>
                                    @if(Auth::user()->group != 'administrator')
									<li>
										<a href="{{url('/users/list-complaint')}}">
											<i class="fa fa fa-table ca-icon" aria-hidden="true"></i>
											<div class="ca-content">
                                                <h5 class="ca-main">Daftar Pelaporan</h5>
												<h6 class="ca-sub">Menu laporan mu</h6>
											</div>
										</a>
									</li>
                                    @endif
                                    @if(Auth::user()->group == 'pic')
									<li>
										<a href="{{url('/users/list-complaint-pic')}}">
											<i class="fa fa fa-key ca-icon" aria-hidden="true"></i>
											<div class="ca-content">
                                                <h5 class="ca-main">Daftar Pelaporan  PIC</h5>
												<h6 class="ca-sub">Menu Laporan PIC</h6>
											</div>
										</a>
									</li>
									@endif
									@if(Auth::user()->group == 'administrator')
									<li>
										<a href="{{url('/administrator/report')}}">
											<i class="fa fa fa-key ca-icon" aria-hidden="true"></i>
											<div class="ca-content">
												<h5 class="ca-main">Administrator</h5>
												<h6 class="ca-sub">Menu Administrator</h6>
											</div>
										</a>
									</li>
									@endif
								</ul>
                        </div>
                        <!-- CONTENT -->
						<div class="col-lg-9">
                            <div class="panel-body">
                                {!! Form::model($ticket, ['url' => '/users/followup-complaint/save', 'role' => 'form', 'novalidate']) !!}
                                    {!! Form::hidden('id_ticket', $ticket->id_ticket, ['class' => 'form-control']) !!}
                                    <div class="wrap-table100">
                                        <div class="table">
                                            <div class="row-table header">
                                                <div class="cell">
                                                    <b>Detail Tiket</b>  
                                                </div>
                                                <div class="cell">
                                                    {{$ticket->id_ticket}}
                                                </div>
                                            </div>

                                            <div class="row-table">
                                                <div class="cell" data-title="">
                                                <b>Tanggal Pelaporan</b> 
                                                </div>
                                                <div class="cell" data-title="">
                                                    {{\Carbon\Carbon::parse($ticket->created_at)->format(' d M Y H:i')}}
                                                </div>
                                            </div>

                                            <div class="row-table">
                                                <div class="cell" data-title="">
                                                <b>Email Pelapor</b> 
                                                </div>
                                                <div class="cell" data-title="">
                                                    {{!empty($user->email) ? $user->email : $ticket->email_for_guest}}
                                                </div>
                                            </div>

                                            <div class="row-table">
                                                <div class="cell" data-title="">
                                                <b> Kategori</b> 
                                                </div>
                                                <div class="cell" data-title="">
                                                @if(!empty($parentcategoriesname) OR !empty($subcategoriesname))
                                                    @if(Auth::user()->group == 'administrator' AND $ticket->ticket_status == 'REGS')
                                                        <select id="inputSelectCategory" class="form-control editor" name="id_category">
                                                        <option value="">--Silahkan Pilih--</option>
                                                            @foreach($parentcategories as $category)
                                                                @if($category->id == $ticket->id_category)
                                                                    <option value={{$category->id}} selected>{{ $category->category_name }}</option>
                                                                @else
                                                                    <option value={{$category->id}}>{{ $category->category_name }}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('category') }}</strong>
                                                        </span>
                                                    @else
                                                        {{$parentcategoriesname->category_name}}
                                                    @endif
                                                @else
                                                    
                                                @endif   
                                                </div>
                                            </div>

                                            <div class="row-table">
                                                <div class="cell" data-title="">
                                                <b> Sub Kategori</b> 
                                                </div>
                                                <div class="cell" data-title="">
                                                @if(!empty($parentcategoriesname) OR !empty($subcategoriesname))
                                                    @if(Auth::user()->group == 'administrator' AND $ticket->ticket_status == 'REGS')
                                                            <select id="inputSelectSubCategory" class="form-control editor" name="id_sub_category">
                                                        </select>
                                                    @else
                                                        {{$subcategoriesname->sub_category_name}}
                                                    @endif
                                                @else
                                                
                                                @endif
                                                </div>
                                            </div>
                                            @if(Auth::user()->group == 'administrator')
                                            @else
                                                <div class="row-table">
                                                    <div class="cell" data-title="">
                                                    <b> Deskripsi</b> 
                                                    </div>
                                                    <div class="cell" data-title="">
                                                        {!!$ticket->ticket_description!!}
                                                    </div>
                                                </div>
                                            @endif

                                            <div class="row-table">
                                                <div class="cell" data-title="">
                                                <b> File Pendukung</b> 
                                                </div>
                                                
                                                <div class="cell" data-title="">
                                                @if(!empty($ticket->ticket_attachment))
                                                  @foreach(json_decode($ticket->ticket_attachment, true) as $value)
                                                          <a href="{{url('/download',$value)}}">{{$value}}</a> ||
                                                  @endforeach
                                                @else
                                                    -
                                                @endif
                                                </div>
                                            </div> 

                                            <div class="row-table">
                                                <div class="cell" data-title="">
                                                <b>    Status Tiket</b> 
                                                </div>
                                                <div class="cell" data-title="">
                                                    <!-- @if(Auth::user()->group == 'administrator' AND $ticket->ticket_status == 'REGS')
                                                        <select id="inputStatus" class="form-control editor" name="ticket_status">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="REGS" {{(!empty($ticket->ticket_status) AND $ticket->ticket_status == 'REGS') ?  'selected' : ''}}>REGISTER</option>
                                                            <option value="OPEN" {{(!empty($ticket->ticket_status) AND $ticket->ticket_status == 'OPEN') ?  'selected' : ''}}>OPEN</option>
                                                        </select>
                                                    @elseif(Auth::user()->group == 'pic' AND $ticket->ticket_status == 'PEND')
                                                        <select id="inputStatus" class="form-control editor" name="ticket_status">                                                            
                                                            <option value="PEND" {{(!empty($ticket->ticket_status) AND $ticket->ticket_status == 'PEND') ?  'selected' : ''}}>OPEN</option>
                                                            <option value="OPEN" {{(!empty($ticket->ticket_status) AND $ticket->ticket_status == 'OPEN') ?  'selected' : ''}}>CLOSING</option>
                                                        </select>
                                                    @elseif(Auth::user()->group == 'administrator' AND $ticket->ticket_status == 'OPEN')
                                                        <select id="inputStatus" class="form-control editor" name="ticket_status">                                                            
                                                            <option value="OPEN" {{(!empty($ticket->ticket_status) AND $ticket->ticket_status == 'OPEN') ?  'selected' : ''}}>OPEN</option>
                                                            <option value="CLOS" {{(!empty($ticket->ticket_status) AND $ticket->ticket_status == 'CLOS') ?  'selected' : ''}}>CLOSING</option>
                                                        </select>
                                                    @else
                                                    @endif -->
                                                    @if(Auth::user()->group == 'administrator')
                                                        <select id="inputStatus" class="form-control editor" name="ticket_status">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            @if($ticket->ticket_status !== 'OPEN')
                                                            <option value="REGS" {{(!empty($ticket->ticket_status) AND $ticket->ticket_status == 'REGS') ?  'selected' : ''}}>REGS</option>
                                                            <option value="PEND" {{(!empty($ticket->ticket_status) AND $ticket->ticket_status == 'PEND') ?  'selected' : ''}}>PENDING</option>
                                                            @endif
                                                            <option value="CLOS" {{(!empty($ticket->ticket_status) AND $ticket->ticket_status == 'CLOS') ?  'selected' : ''}}>CLOSE</option>                            
                                                            @if($ticket->ticket_status == 'OPEN')
                                                                <option value="OPEN" {{(!empty($ticket->ticket_status) AND $ticket->ticket_status == 'OPEN') ?  'selected' : ''}}>APPROVE</option>
                                                            @elseif($ticket->ticket_status == 'REJ')
                                                                <option value="REJ" {{(!empty($ticket->ticket_status) AND $ticket->ticket_status == 'REJ') ?  'selected' : ''}}>REJECT</option>
                                                            @endif
                                                        </select>
                                                    @elseif(Auth::user()->group == 'authorizer')
                                                        <select id="inputStatus" class="form-control editor" name="ticket_status">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="PEND" {{(!empty($ticket->ticket_status) AND $ticket->ticket_status == 'PEND') ?  'selected' : ''}}>PENDING</option>
                                                            <option value="OPEN" {{(!empty($ticket->ticket_status) AND $ticket->ticket_status == 'OPEN') ?  'selected' : ''}}>APPROVE</option>
                                                            <option value="REJ" {{(!empty($ticket->ticket_status) AND $ticket->ticket_status == 'REJ') ?  'selected' : ''}}>REJECT</option>
                                                        </select>
                                                    @else
                                                        {{$ticket->ticket_status}}
                                                    @endif 
                                                </div>
                                            </div>

                                            @if(Auth::user()->group == 'administrator')
                                                <div class="row-table">
                                                    <div class="cell" data-title="">
                                                    <b>  PIC</b> 
                                                    </div>
                                                    <div class="cell" data-title="">
                                                        {!! Form::hidden('id_pic',  $ticket->id_pic, ['id' => 'idPic', 'class' => 'form-control']) !!}
                                                        @if(!empty($pic))
                                                            <p id="render_pic">{{$pic->name_pic}}</p>
                                                        @else
                                                            PIC dari kategori tersebut baru saja dihapus
                                                        @endif
                                                    </div>
                                                </div>
                                            @endif 

                                            <div class="row-table header">
                                                <div class="cell">
                                                <b> Umpan Balik</b>   
                                                </div>
                                                <div class="cell">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        
                                    <div class="wrap-table100">
                                        <div class="table">    

                                                @forelse($detailTicket as $dt)
                                                <div class="row-table">
                                                    <div class="cell" data-title="">
                                                
                                                        {{date("d-m-Y H:i", strtotime($dt->created_at))}}
                                                    </div>
                                                    <div class="cell" data-title="">
                                                        {!!$dt->response!!} 
                                                    </div>
                                                </div>  
                                                @endforeach

                                                @if(Auth::user()->group == 'administrator')
                                                    <div class="row-table">
                                                        <div class="cell" data-title="">
                                                            <b> Input Feedback </b> 
                                                        </div>
                                                        <div class="cell" data-title="">
                                                        <select id="feedback" class="form-control editor" name="feedback">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="Pelaporan sudah diterima">Pelaporan sudah diterima</option>
                                                            <option value="Pelaporan sedang dianalisa">Pelaporan sedang dianalisa</option>
                                                            <option value="Pelaporan sedang ditindaklanjuti">Pelaporan sedang ditindaklanjuti</option>
                                                            <option value="Hasil">Hasil</option>
                                                            <!--<option value="Others">Others</option>-->
                                                        </select>
                                                        {!! Form::hidden('user', Auth::user()->fullname, ['class' => 'form-control']) !!}
                                                        {!! Form::textarea('response', null, ['class' => 'form-control editor', 'required', 'autofocus'])!!}
                                                        <br>
                                                        <p style="font-size: 14px;">
                                                            Isi feedback ini untuk menindak lanjuti tiket ini
                                                        </p>
                                                        </div>
                                                    </div>
                                                @elseif(Auth::user()->group == 'authorizer' AND $ticket->ticket_status == 'PEND')
                                                    <div class="row-table">
                                                        <div class="cell" data-title="Full Name">
                                                            Input Feedback
                                                        </div>
                                                        <div class="cell" data-title="Age">
                                                        <select id="feedback" class="form-control editor" name="feedback">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="Pelaporan sudah diterima">Pelaporan sudah diterima</option>
                                                            <option value="Pelaporan sedang dianalisa">Pelaporan sedang dianalisa</option>
                                                            <option value="Pelaporan sedang ditindaklanjuti">Pelaporan sedang ditindaklanjuti</option>
                                                            <option value="Hasil">Hasil</option>
                                                            <!--<option value="Others">Others</option>-->
                                                        </select>
                                                        {!! Form::hidden('user', Auth::user()->fullname, ['class' => 'form-control']) !!}
                                                        {!! Form::textarea('response', null, ['class' => 'form-control editor', 'required', 'autofocus']) !!}
                                                        <br>
                                                        <p style="font-size: 14px;">
                                                            Isi feedback ini untuk menindak lanjuti tiket ini
                                                        </p>
                                                        </div>
                                                    </div> 
                                                @elseif(Auth::user()->group == 'pic' AND $ticket->ticket_status == 'OPEN')
                                                    <div class="row-table">
                                                        <div class="cell" data-title="">
                                                        <b> Input Feedback</b> 
                                                        </div>
                                                        <div class="cell" data-title="">
                                                        <select id="feedback" class="form-control editor" name="feedback">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="Pelaporan sudah diterima">Pelaporan sudah diterima</option>
                                                            <option value="Pelaporan sedang dianalisa">Pelaporan sedang dianalisa</option>
                                                            <option value="Pelaporan sedang ditindaklanjuti">Pelaporan sedang ditindaklanjuti</option>
                                                            <option value="Hasil">Hasil</option>
                                                            <!--<option value="Others">Others</option>-->
                                                        </select>   
                                                        {!! Form::hidden('user', Auth::user()->fullname, ['class' => 'form-control']) !!}
                                                        {!! Form::textarea('response', null, ['class' => 'form-control editor', 'required', 'autofocus']) !!}
                                                        <br>
                                                        <p style="font-size: 14px;">
                                                            Isi feedback ini untuk menindak lanjuti tiket ini
                                                        </p>
                                                        </div>
                                                    </div>             
                                                @elseif(Auth::user()->group == 'member' AND $ticket->ticket_status == 'REGS')
                                                    <div class="row-table">
                                                        <div class="cell" data-title="">
                                                        <b>    Input Feedback </b> 
                                                        </div>
                                                        <div class="cell" data-title="">
                                                        <select id="feedback" class="form-control editor" name="feedback">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="Pelaporan sudah diterima">Pelaporan sudah diterima</option>
                                                            <option value="Pelaporan sedang dianalisa">Pelaporan sedang dianalisa</option>
                                                            <option value="Pelaporan sedang ditindaklanjuti">Pelaporan sedang ditindaklanjuti</option>
                                                            <option value="Hasil">Hasil</option>
                                                            <!--<option value="Others">Others</option>-->
                                                        </select>
                                                        {!! Form::hidden('user', Auth::user()->fullname, ['class' => 'form-control']) !!}
                                                        {!! Form::textarea('response', null, ['class' => 'form-control editor', 'required', 'autofocus']) !!}
                                                        <br>
                                                        <p style="font-size: 14px;">
                                                            Isi feedback ini untuk menindak lanjuti tiket ini
                                                        </p>
                                                        </div>
                                                    </div>             
                                                @endif
                                        </div>
                                    </div>
                                    <div class="text-xs-center">
                                                    {{ $detailTicket->render("pagination::bootstrap-4") }}
                                    </div>         
                                <br>
                                <!-- tempcount untuk menghitung jumlah dari id_ticket yang sama. untuk mengatur status -->
                                <!-- contoh : semisal status REGS maka tempcount = 1 -->
                                <div class="col-lg-12 text-center">
                                     {!! Form::hidden('url_segment', Request::segment(6), ['class' => 'form-control']) !!}
                                    <button type="submit" class="btn submit_btn2" name="save" value="next">
                                        Simpan 
                                    </button>
                                    <a href = "{{ URL::previous() }}" class="btn submit_btn2">
                                        Batal
                                    </a>
                                </div>
                                        
                            {!! Form::close() !!}
                            </div>
						</div>
                        <!-- END CONTENT -->
                    </div>
                </div>
            </div>
        </section>
        <!--================End Static Area =================-->
@endsection
@push('scripts')
<script src="{{asset('vendors/tinymce/jquery.tinymce.min.js')}}"></script>
<script src="{{asset('vendors/tinymce/tinymce.min.js')}}"></script>
<script src="{{asset('/vendors/bootstrap-sweetalerts/dist/sweetalert.min.js')}}"></script>
<script>
$(function() {
   var base_url = '{{ url("/") }}';

   // $('.selectpicker').selectpicker({
   //     style: 'btn-info',
   //     size: false
   // });

   $('#inputSelectCategory').on('change', function(){
       var value = $(this).find("option:selected").val();
       if(value != '' || value != '000'){
           $.ajax({
               type: "GET",
               url: base_url + '/users/getSubCategory/'+value,
               beforeSend: function() {
                    $("#inputSelectSubCategory").prepend($('<option></option>').html('Loading ...'));
               },
               success: function(data){
                       buildDropdown(jQuery.parseJSON(data),$('#inputSelectSubCategory'),'--Silahkan Pilih--');
               }
           });
       } 
   });

    $('#inputSelectSubCategory').on('change', function(){
       var value = $(this).find("option:selected").val();
       if(value != ''){
           $.ajax({
               type: "GET",
               url: base_url + '/users/getPicSubCategory/'+value,
               success: function(data){
                     var data = jQuery.parseJSON(data);
                     $.each(data, function(k, v) {
                        $('#render_pic').html(v);
                        $('#idPic').val(k);
                    });
                     
               }
           });
       }  
    });

   function buildDropdown(result, dropdown, emptyMessage) {
       // Remove current options
       dropdown.html('');
       //dropdown.attr('disabled', false);
       // Add the empty option with the empty message
       dropdown.append('<option value="">' + emptyMessage + '</option>');
       // Check result isnt empty
       if(result != '')
       {
           // Loop through each of the results and append the option to the dropdown
           $.each(result, function(k, v) {
               dropdown.append('<option value="' + k + '">' + v + '</option>');
           });
       }
       // dropdown.selectpicker('destroy');
       // dropdown.selectpicker('refresh');
   }
   //$('.selectpicker').selectpicker('destroy');
   //$('.selectpicker').selectpicker('refresh');
});
window.onload = function(){
    document.getElementById('mceu_15').style.display = 'none';
};
$(function() {
    $('#feedback').on('change', function(){
        var value = $(this).find("option:selected").val();
        if(value != '' || value != '000')
        {
            if(value == 'Hasil')
            {
                document.getElementById('mceu_15').style.display = 'inline';
                // document.getElementById('response').style.display = "show";
            }
            else
            {
                document.getElementById('mceu_15').style.display = 'none';
                // document.getElementById('response').value = value;
            }
        }
        else
        {
            document.getElementById('mceu_15').style.display = 'none';
            // document.getElementById('response').value = "";
        }
    });
});

if( '{{session("type")}}' == 'success')
    {
        swal({
        title   : "Pelaporan berhasil diFollow-Up!",
        text    : "Terima Kasih",
        type    : "success",
        customClass: 'sweetalert-lg',
        button   : "OK!",
        },
        function(isConfirm){
            var thisurl =  window.location.href;
            var arr = thisurl.split('/');
            var last = arr[arr.length-1];

            if(last == 'list-complaint-pic')
            {
                // window.location = '{{ url("/users/list-complaint-pic") }}'
                window.location.replace('{{ url("/users/list-complaint-pic") }}')
            }
            else if (last == 'list-complaint-approval')
            {
                // window.location = '{{ url("/users/list-complaint-approval") }}'
                window.location.replace('{{ url("/users/list-complaint-approval") }}')
            }
            else
            {
                // window.location = '{{ url("/users/list-complaint") }}'
                window.location.replace('{{ url("/users/list-complaint") }}')
            }
        });
    }
</script>
@endpush
@push('style')
<style>
.noshow {
    display : block;
}
.show {
    display : inline;
}
.limiter {
  width: 100%;
  margin: 0 auto;
}
.container-table100 {
  width: 100%;
  min-height: 100vh;
  background: #c4d3f6;
  display: -webkit-box;
  display: -webkit-flex;
  display: -moz-box;
  display: -ms-flexbox;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  padding: 33px 30px;
}
.table {
  width: 100%;
  display: table;
  margin: 0;
}
.pagination {
    display: -ms-flexbox;
    display: flex;
    padding-left: 0;
    list-style: none;
    border-radius: .25rem;
    justify-content: center;
}
.scrollbar {
    overflow-x : hidden;
    max-height: 400px;
    overflow: scroll;
}
@media screen and (max-width: 768px) {
  .table {
    display: block;
  }
}
.row-table {
  display: table-row;
  background: #fff;
}
.row-table.header {
  color: #ffffff;
  background: #0089d1;
}
@media screen and (max-width: 768px) {
  .row-table {
    display: block;
  }
  .row-table.header {
    padding: 0;
    height: 0px;
  }
  .row-table.header .cell {
    display: none;
  }
  .row-table .cell:before {
    font-family: Poppins-Bold;
    font-size: 12px;
    color: #808080;
    line-height: 1.2;
    text-transform: uppercase;
    font-weight: unset !important;
    margin-bottom: 13px;
    content: attr(data-title);
    min-width: 98px;
    display: block;
  }
}
.cell {
  display: table-cell;
}
@media screen and (max-width: 768px) {
  .cell {
    display: block;
  }
}
.row-table .cell {
    font-family: Poppins-Regular;
    font-size: 15px;
    color: #666666;
    line-height: 1.2;
    font-weight: unset !important;
    padding-top: 10px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f2f2f2;
}
.row-table.header .cell {
  font-family: Poppins-Regular;
  font-size: 18px;
  color: #fff;
  line-height: 1.2;
  font-weight: unset !important;

  padding-top: 10px;
  padding-bottom: 10px;
}
.row-table .cell:nth-child(1) {
  width: 60px;
  padding-left: 40px;
}
.row-table .cell:nth-child(2) {
  width: 360px;
}
.table, .row-table {
  width: 100% !important;
}
.row-table:hover {
  background-color: #ececff;
  cursor: pointer;
}
.row-table.header:hover {
    background-color: #0072bb;
    cursor: pointer;
}
@media (max-width: 768px) {
    body {
        overflow-x: hidden;
    }
  .row-table {
    border-bottom: 1px solid #f2f2f2;
    /* padding-bottom: 18px;
    padding-top: 30px; */
    /* padding-right: 15px; */
    margin: 0;
  } 
  .row-table .cell {
    border: none;
    padding-left: 30px;
    /* padding-top: 16px;
    padding-bottom: 16px; */
  }
  .row-table .cell:nth-child(1) {
    padding-left: 30px;
    background-color: #b4c0ff73;
  }
  .row-table .cell {
    font-family: Poppins-Regular;
    font-size: 18px;
    color: #555555;
    line-height: 1.2;
    font-weight: unset !important;
  }
  .table, .row-table, .cell {
    width: 100% !important;
  }
  .submit_btn2 {
    width: 113px;
  }
  .ca-menu {
    padding: 0px 33px 0px 33px;
}
}
</style>
@endpush
@push('scripts')
<script src="{{asset('vendors/tinymce/jquery.tinymce.min.js')}}"></script>
<script src="{{asset('vendors/tinymce/tinymce.min.js')}}"></script>
<script>
$(function() {

    var base_url = '{{ url("/") }}';
    var value = '{{$ticket->id_category}}';
    var valueSubCategory = '{{$ticket->id_sub_category}}';
    callSubCategory(value, valueSubCategory);
    function callSubCategory(value, valueSubCategory) {
        $.ajax({
            type: "GET",
            url: base_url + '/users/getSubCategory/'+value,
            success: function(data){
                    buildDropdown(jQuery.parseJSON(data), $('#inputSelectSubCategory'),'--Silahkan Pilih--', valueSubCategory);
            }
        }); 
    }

   function buildDropdown(result, dropdown, emptyMessage, valueSubCategory) {
       dropdown.html('');
       dropdown.append('<option value="">' + emptyMessage + '</option>');
       if(result != '')
       {
           $.each(result, function(k, v) {
               if(k == valueSubCategory){
                    dropdown.append('<option value="' + k + '" selected>' + v + '</option>');
               }else{
                    dropdown.append('<option value="' + k + '">' + v + '</option>');
               }
           });
       }
   }

});
</script>
<script>
    var base_url = '{{ url("/") }}';
    var editor_config = {
        path_absolute : "/",
        selector: "textarea.editor",
        plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        //   "searchreplace wordcount visualblocks visualchars code fullscreen",
        //   "insertdatetime media nonbreaking save table contextmenu directionality",
        //   "emoticons template paste textcolor colorpicker textpattern codesample",
        //   "fullpage toc tinymcespellchecker imagetools help"
        ],
        menu: {},
        toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent removeformat formatselect',
        // toolbar: "insertfile undo redo | styleselect | bold italic strikethrough | alignleft aligncenter alignright alignjustify | ltr rtl | bullist numlist outdent indent removeformat formatselect| link image media | emoticons charmap | code codesample | forecolor backcolor",
        //external_plugins: { "nanospell": "http://YOUR_DOMAIN.COM/js/tinymce/plugins/nanospell/plugin.js" },
        nanospell_server:"php",
        browser_spellcheck: true,
        relative_urls: false,
        remove_script_host: false,
        file_browser_callback : function(field_name, url, type, win) {
        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
        if (type == 'image') {
            cmsURL = cmsURL + "&type=Images";
        } else {
            cmsURL = cmsURL + "&type=Files";
        }

        tinymce.activeEditor.windowManager.open({
            file: '<?= route('elfinder.tinymce4') ?>',// use an absolute path!
            title: 'File manager',
            width: 900,
            height: 450,
            resizable: 'yes'
        }, {
            setUrl: function (url) {
            win.document.getElementById(field_name).value = url;
            }
        });
        }
    };

    tinymce.init(editor_config);
</script>
<script>
  {!! \File::get(base_path('vendor/barryvdh/laravel-elfinder/resources/assets/js/standalonepopup.min.js')) !!}
</script>
@endpush