@extends('layouts.app')

@section('content')
    <!--================Banner Area =================-->
    <section class="banner_area">
        <div class="container">
            <div class="banner_text_inner">
                @if(!empty($content->title))
                    <h4>{{ $content->title }}</h4>
                    <h6 style="color: #FFFFFF;">Posted on {{ $content->created_at_blog }}</h6>
                @else
                    <h4>Speak Up Blog</h4>
                    <h6 style="color: #FFFFFF;">Speak Up Blog - Informasi Terbaru department Compliance</h6>
                @endif
            </div>
        </div>
    </section>
        <!--================End Banner Area =================-->
		
		<section class="main_slider_area">
			<div class="copy_right_area_top">
  
            </div>
		</section>

       <!--================Static Area =================-->

        @if (!empty($view) AND $view == 'blog-list')
            @include('pages.blog.content.blog_list')
        @endif
        @if (!empty($view) AND $view == 'blog-content')
            @include('pages.blog.content.blog_content')
        @endif
       
        <!--================End Static Area =================-->
@endsection
@push('style')
    @if(!empty($content->image_url))
        <style>
            .banner_area {
                background: url({{URL::asset('/images/' . $content->image_url . '')}}) no-repeat scroll center center;
                background-size: cover;
                position: relative;
                min-height: 200px;
            }
        </style>
    @endif
@endpush

