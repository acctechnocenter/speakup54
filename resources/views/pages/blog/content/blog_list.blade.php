<section class="latest_news_area p_100">
    <div class="container">
        <div class="b_center_title">
            <br>
        </div>
        <div class="l_news_inner">
            <div class="row">

                @forelse ($blog as $content)
                    <div class="col-lg-4 col-md-6">
                        <div class="l_news_item" style="padding-top: 10px;">
                            <div class="l_news_img" style="max-width: 370px;max-height: 220px;"><a href="#"><img class="img-fluid" style="min-height: 221px;" src="{{URL::asset('/images/' . $content->image_url . '')}}" alt=""></a></div>
                            <div class="l_news_content">
                                <a href="{{url('/blog/'. $content->slug_blog .'')}}"><h4>{{ $content->title }}</h4></a>
                                <p>{{ str_limit(strip_tags($content->post_body), 100) }}</p>
                                <a class="more_btn" href="{{url('/blog/'. $content->slug_blog .'')}}">Learn More</a>
                            </div>
                        </div>
                    </div>
                @empty
                    <p>EMPTY BLOG</p>
                @endforelse
                
            </div>
            <br>
                {{-- <a class="tp_btn" href="{{url('/blog')}}" style="color:#000000;">&nbsp;&nbsp;Older Posts&nbsp;&nbsp;</a> --}}
                <div class="text-xs-center">
                    {{ $blog->render("pagination::bootstrap-4") }}
                    
                </div>
        </div>
    </div>
</section>
@push('style')
<style>
.pagination {
    display: -ms-flexbox;
    display: flex;
    padding-left: 0;
    list-style: none;
    border-radius: .25rem;
    justify-content: center;
}
</style>
@endpush