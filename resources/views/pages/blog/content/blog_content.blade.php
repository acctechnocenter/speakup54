<section class="static_area">
            <div class="container">
                <div class="static_inner">
                    <div class="row">
						         <div class="col-lg-3">
                            <div class="right_sidebar_area">
                                <aside class="right_widget r_news_widget">
                                    <div class="r_w_title">
                                        <h3>Recent Posts</h3>
                                    </div>
                                    <div class="news_inner">
                                        @forelse ($listBlog as $listContent)
                                        <div class="news_item">
                                            <a href="{{url('/blog/'. $listContent->slug_blog .'')}}"><h4>{{ $listContent->title }}</h4></a>
                                            <a href="{{url('/blog/'. $listContent->slug_blog .'')}}"><h6>Posted on {{ $listContent->created_at_blog }}</h6></a>
                                        </div>
                                        @empty
                                        <div class="news_item">
                                            <a href="#"><h4></h4></a>
                                            <a href="#"><h6></h6></a>
                                        </div>
                                        @endforelse
                                    </div>
                                </aside>
                          
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="static_main_content">
                                <div class="static_social">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                        <li><a href="#"><i class="fa fa-thumb-tack" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                                <div class="static_img">
                                    <img class="img-fluid" style="width: 860px;" src="{{URL::asset('/images/' . $content->image_url . '')}}" alt="">
                                </div>
                                <div class="static_text">
                                {!! $content->post_body !!}
                                </div>
                         
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>