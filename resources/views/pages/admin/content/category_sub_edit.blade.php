<div class="container">
    <div class="row">
    
        <div class="col-md-12">
            <div class="panel panel-default">
                <ol class="breadcrumb">
                    {{-- <li class="breadcrumb-item">
                        <a href="#">Faq's</a>
                    </li> --}}
                    <li class="breadcrumb-item active"><a href="{{url('/administrator/category') }}" class="btn btn-info btn-wkwk" role="button">List Category</a></li>
                    <li></li>
                    <li></li>
                </ol>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                 @endif
                <div class="panel-body">
                    {!! Form::model($subcategory, ['url' => '/administrator/category/savesub', 'role' => 'form', 'novalidate']) !!}
                    {!! Form::hidden('id', null, ['class' => 'form-control']) !!}
                    
                    {{-- select parent category --}}
                    <div class="form-group row">
                        {!! Form::label('category_name','Category*: ',['class' => 'col-md-3 control-label']) !!}
                        <div class="col-md-9">   
                            <select readonly id="inputSelectCategory" class="form-control" name="id_category">
                            <option value="0">-- PILIH KATEGORI --</option>
                                @foreach($parentcategory as $pc)
                                    @if($pc->id == $subcategory->id_category)
                                        <option value="{{ $pc->id }}" selected>{{ $pc->category_name }}</option>
                                    @else
                                        <option value="{{ $pc->id }}">{{ $pc->category_name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    {{-- sub category field --}}
                    <div class="form-group row {{ $errors->has('category_name') ? 'has-error' : ''}}">
                        {!! Form::label('sub_category_name','Sub Category*: ',['class' => 'col-md-3 control-label']) !!}
                        <div class="col-md-9">
                            {!! Form::text('sub_category_name',null,['class'=>'form-control editor', 'required']) !!}
                            <span class="help-block">
                                <strong>{{ $errors->first('sub_category_name') }}</strong>
                            </span>
                        </div>
                    </div>
                    {{-- sub category desc --}}
                    <div class="form-group row">
                        {!! Form::label('desc_sub_category','Sub Category Description*: ',['class' => 'col-md-3 control-label']) !!}
                        <div class="col-md-9">
                            {!! Form::textarea('desc_sub_category', null, ['class' => 'form-control editor']) !!}
                            <span class="help-block">
                                <strong>{{ $errors->first('desc_sub_category') }}</strong>
                            </span>
                        </div>
                    </div>
                    {{-- static PIC --}}
                    <div class="form-group row">
                        {!! Form::label('pic','PIC*: ',['class' => 'col-md-3 control-label']) !!}
                        <div class="col-sm-9">
                            <select id="pic" class="form-control" class="form-control editor" name="pic">
                                <option value="">-- PILIH PIC --</option>
                                @foreach($pics as $pic)
                                    @if($pic->id == $subcategory->id_pic)
                                        <option value="{{ $pic->id }}" selected>{{ $pic->name_pic  }}</option>
                                    @else
                                        <option value="{{ $pic->id }}">{{ $pic->name_pic  }}</option>
                                    @endif
                                @endforeach
                            </select>
                            <span class="help-block">
                                <strong>{{ $errors->first('pic') }}</strong>
                            </span>
                        </div>
                    </div>
                    {{-- SLA --}}
                    <div class="form-group row">
                        {!! Form::label('sla','SLA (Days)*: ',['class' => 'col-md-3 control-label']) !!}
                        <div class="col-sm-9">
                            {!! Form::text('sla', null, ['class' => 'form-control editor','placeholder' => 'Days']) !!}
                            <span class="help-block">
                                <strong>{{ $errors->first('sla') }}</strong>
                            </span>
                         </div>    
                    </div>
                    @if(Auth::user()->group == 'authorizer')
                        <div class="form-group row">
                            {!! Form::label('flag_active','Activation',['class' => 'col-md-3 control-label']) !!}
                            <div class="col-md-9">   
                                <label class="radio-inline"><input type="radio" name="flag_active" value="yes" {{$subcategory->flag_active == "yes" ? 'checked' : ''}}>Ya</label>
                                <label class="radio-inline"><input type="radio" name="flag_active" value="no" {{$subcategory->flag_active == "no" ? 'checked' : ''}}>Tidak</label>
                            </div>
                        </div>
                    @endif
                    <div class="form-group">
                        <div class="col-md-12 d-flex flex-row-reverse">
                        <a href="{{ URL::previous() }}" class="btn btn-primary">Batal</a> 
                            <button type="submit" class="btn btn-primary mr-1">
                                Perbarui
                            </button> 
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
          
            </div>
        </div>

    </div>
</div>
@push('scripts')
<script src="{{asset('/vendors/bootstrap-sweetalerts/dist/sweetalert.min.js')}}"></script>
    <script>
    $('#update').on('click', function(e){
        e.preventDefault();
        var form = e.target.form;
        swal({
            title   : "Are you sure?",
            text    : "Update this data will affect the Ticket data!",
            type    : "warning",
            showCancelButton    : true,
            confirmButtonClass  : "btn-danger",
            confirmButtonText   : "Yes",
            cancelButtonText    : "No, cancel",
            closeOnConfirm      : false,
            closeOnCancel       : false
        },
        function(isConfirm) {
            if(isConfirm){
                swal({
                    title   : "Yeah",
                    text    : "Wait a sec! Your request has been sent!",
                    type    : "success"
                    },
                    function(isConfirm){
                        if(isConfirm)
                        form.submit();
                    })
            }else
            {
                swal("Cancelled", "No data update", "error");
            }
        });
    })
    </script>
@endpush
