<div class="container">
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <ol class="breadcrumb">
                    {{-- <li class="breadcrumb-item">
                        <a href="#">Faq's</a>
                    </li> --}}
                    <li class="breadcrumb-item active"><a href="{{url('/administrator/user') }}" class="btn btn-info btn-wkwk" role="button">List User</a></li>
                    <li></li>
                    <li></li>
                </ol>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                 @endif
                <div class="panel-body">
                    {!! Form::model($user, ['url' => '/administrator/user/save', 'role' => 'form', 'novalidate']) !!}
                    {!! Form::hidden('id', null, ['class' => 'form-control']) !!}

                    {{-- FULLNAME --}}
                    <div class="form-group row {{ $errors->has('fullname') ? 'has-error' : ''}}">
                        {!! Form::label('fullname','Fullname*: ',['class' => 'col-md-3 control-label']) !!}
                        <div class="col-md-9">
                            {!! Form::text('fullname',null,['class'=>'form-control editor', 'required']) !!}
                            <span class="help-block">
                                <strong>{{ $errors->first('fullname') }}</strong>
                            </span>
                        </div>
                    </div>
                    {{-- EMAIL --}}
                    <div class="form-group row {{ $errors->has('email') ? 'has-error' : ''}}">
                        {!! Form::label('email','Email*: ',['class' => 'col-md-3 control-label']) !!}
                        <div class="col-md-9">
                            {!! Form::text('email',null,['class'=>'form-control editor', 'required']) !!}
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        </div>
                    </div>
                    {{-- HANDPHONE --}}
                    <div class="form-group row {{ $errors->has('no_handphone') ? 'has-error' : ''}}">
                        {!! Form::label('no_handphone','No Telephone*: ',['class' => 'col-md-3 control-label']) !!}
                        <div class="col-md-9">
                            {!! Form::text('no_handphone',null,['class'=>'form-control editor', 'required']) !!}
                            <span class="help-block">
                                <strong>{{ $errors->first('no_handphone') }}</strong>
                            </span>
                        </div>
                    </div>
                    {{-- GROUP --}}
                    <div class="form-group row">
                        {!! Form::label('category_name','Category*: ',['class' => 'col-md-3 control-label']) !!}
                        <div class="col-md-9">   
                            <select id="inputSelectCategory" class="form-control editor" name="group">
                                <option value="administrator" {{($user->group == "administrator") ? 'selected' : '' }}>Admin</option>
                                <option value="member" {{($user->group == "member") ? 'selected' : '' }}>Member</option>
                                <option value="pic" {{($user->group == "pic") ? 'selected' : '' }}>PIC</option>
                                <option value="pic" {{($user->group == "authorizer") ? 'selected' : '' }}>Authorizer</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        {!! Form::label('flag_active','Activation*: ',['class' => 'col-md-3 control-label']) !!}
                        <div class="col-md-9">   
                        <label class="radio-inline"><input type="radio" name="flag_active" value="1" {{$user->flag_active == "1" ? 'checked' : ''}}>Ya</label>
                            <label class="radio-inline"><input type="radio" name="flag_active" value="0" {{$user->flag_active == "0" ? 'checked' : ''}}>Tidak</label>
                        </div>
                    </div>
                    

                    <div class="form-group">
                        <div class="col-md-12 d-flex flex-row-reverse">
                        <a href="{{ URL::previous() }}" class="btn btn-primary">Batal</a> 
                            <button type="submit" class="btn btn-primary mr-1">
                                Perbarui
                            </button> 
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
          
            </div>
        </div>

    </div>
</div>
