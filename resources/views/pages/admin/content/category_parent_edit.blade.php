<div class="container">
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <ol class="breadcrumb">
                    {{-- <li class="breadcrumb-item">
                        <a href="#">Faq's</a>
                    </li> --}}
                    <li class="breadcrumb-item active"><a href="{{url('/administrator/category') }}" class="btn btn-info btn-wkwk" role="button">List Category</a></li>
                    <li></li>
                    <li></li>
                </ol>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                 @endif
                <div class="panel-body">
                    {!! Form::model($category, ['url' => '/administrator/category/saveparent', 'role' => 'form', 'novalidate']) !!}
                    {!! Form::hidden('id', null, ['class' => 'form-control']) !!}

                    <div class="form-group row {{ $errors->has('category_name') ? 'has-error' : ''}}">
                        {!! Form::label('category_name','Category*: ',['class' => 'col-md-3 control-label']) !!}
                        <div class="col-md-9">
                            {!! Form::text('category_name',null,['class'=>'form-control editor', 'required']) !!}
                            <span class="help-block">
                                <strong>{{ $errors->first('category_name') }}</strong>
                            </span>
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('desc_category','Category Description: ',['class' => 'col-md-3 control-label']) !!}
                        <div class="col-md-9">
                            {!! Form::textarea('desc_category', null, ['class' => 'form-control editor']) !!}
                            <span class="help-block">
                                <strong>{{ $errors->first('desc_category') }}</strong>
                            </span>
                        </div>
                       
                    </div>

                    <div class="form-group">
                        <div class="col-md-12 d-flex flex-row-reverse">
                        <a href="{{ URL::previous() }}" class="btn btn-primary">Batal</a> 
                            <button type="submit" class="btn btn-primary mr-1">
                                Perbarui
                            </button> 
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
          
            </div>
        </div>

    </div>
</div>

@push('scripts')
<script src="{{asset('/vendors/bootstrap-sweetalerts/dist/sweetalert.min.js')}}"></script>
    <script>
    $('#update').on('click', function(e){
        e.preventDefault();
        var form = e.target.form;
        swal({
            title   : "Are you sure?",
            text    : "Update this data will affect the Ticket data!",
            type    : "warning",
            showCancelButton    : true,
            confirmButtonClass  : "btn-danger",
            confirmButtonText   : "Yes",
            cancelButtonText    : "No, cancel",
            closeOnConfirm      : false,
            closeOnCancel       : false
        },
        function(isConfirm) {
            if(isConfirm){
                swal({
                    title   : "Yeah",
                    text    : "Wait a sec! Your request has been sent!",
                    type    : "success"
                    },
                    function(isConfirm){
                        if(isConfirm)
                        form.submit();
                    })
            }else
            {
                swal("Cancelled", "No data update", "error");
            }
        });
    })
    </script>
@endpush

