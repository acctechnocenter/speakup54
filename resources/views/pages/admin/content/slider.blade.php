

<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="#">Slider</a>
    </li>
    <li class="breadcrumb-item active"><a href="{{url('/administrator/slider/create') }}" class="btn btn-info btn-wkwk" role="button">Create Slider</a></li>
    <li></li>
    <li></li>
</ol>

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

<div class="table-responsive">
    <table class="table table-bordered" id="slider-table" width="100%" style="font-size:11px;">
        <thead>
            <tr>
                <th>Slider Title</th>
                <th>Publish</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>

@push('scripts')
<script src="{{asset('/vendors/bootstrap-sweetalerts/dist/sweetalert.min.js')}}"></script>
<script>
$(function() {
    $('#slider-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ url('/administrator/slider/data') }}",
        columns: [
            { data: 'title', name: 'title', width: "60%" },
            { data: 'flag_publish', name: 'flag_publish', width: "10%" },
            { data: 'action', name: 'action' , width: "12%", orderable:false}
        ]
    });
});
</script>
<script>
     $(document).ready(function(){
        $(document).on('click','.delete', function(e){
            e.preventDefault();
            var data_id = $(this).attr("data-id");
            var href = "{{ url('/administrator/slider') }}/" + data_id + "/delete"
            swal({
                title   : "Are you sure?",
                text    : "Update this data will affect the Ticket data!",
                type    : "warning",
                showCancelButton    : true,
                confirmButtonClass  : "btn-danger",
                confirmButtonText   : "Yes",
                cancelButtonText    : "No, cancel",
                closeOnConfirm      : false,
                closeOnCancel       : false
            },
            function(isConfirm) {
                if(isConfirm){
                    swal({
                        title   : "Yeah",
                        text    : "Wait a sec! Your request has been sent!",
                        type    : "success"
                        },
                        function(isConfirm){
                            if(isConfirm){
                            window.location.href = href;
                            }
                        })
                }else
                {
                    swal("Cancelled", "No data update", "error");
                }
            });
        })
    })
</script>
@endpush