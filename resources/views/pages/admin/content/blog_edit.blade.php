<div class="container">
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">Blogs</a>
                    </li>
                    <li class="breadcrumb-item active"><a href="{{url('/administrator/blog') }}" class="btn btn-info btn-wkwk" role="button">List Blogs</a></li>
                    <li></li>
                    <li></li>
                </ol>

                <div class="panel-body">
                    {!! Form::model($blogs, ['url' => '/administrator/blog/save', 'class' => 'form-horizontal', 'role' => 'form', 'enctype' => 'multipart/form-data', 'novalidate']) !!}
                        
                        {!! Form::hidden('id', null, ['class' => 'form-control']) !!}
                        <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                            {!! Form::label('image_url', 'Image Upload*', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::file('image_url', null, ['class' => 'form-control', 'required']) !!}
                                @if(!empty($blogs))
                                <br>
                                <img src="{{URL::asset('/images/' . $blogs->image_url . '')}}" alt="Image upload" width="400" height="200">
                                @endif
                                <span class="help-block">
                                    <strong>{{ $errors->first('image_url') }}</strong>
                                </span>
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            {!! Form::label('title', 'Title*', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::text('title', null, ['class' => 'form-control', 'required', 'autofocus']) !!}
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                            {!! Form::label('post_body', 'Body*', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::textarea('post_body', null, ['class' => 'form-control editor', 'required']) !!}
                                <span class="help-block">
                                    <strong>{{ $errors->first('post_body') }}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('flag_publish') ? ' has-error' : '' }}">
                            {!! Form::label('flag_publish', 'Published*', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {{ Form::radio('flag_publish', 'yes') }} Yes
                                {{ Form::radio('flag_publish', 'no') }}  No
                                <span class="help-block">
                                    <strong>{{ $errors->first('flag_publish') }}</strong>
                                </span>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">
                                    Perbarui
                                </button>
                                <a href="{{ URL::previous() }}" class="btn btn-primary">Batal</a> 
                            </div>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>
</div>

@push('scripts')
<script src="{{asset('vendors/tinymce/jquery.tinymce.min.js')}}"></script>
<script src="{{asset('vendors/tinymce/tinymce.min.js')}}"></script>
<script>
    var base_url = '{{ url("/") }}';
    var editor_config = {
        path_absolute : "/",
        selector: "textarea.editor",
        plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        //   "searchreplace wordcount visualblocks visualchars code fullscreen",
        //   "insertdatetime media nonbreaking save table contextmenu directionality",
        //   "emoticons template paste textcolor colorpicker textpattern codesample",
        //   "fullpage toc tinymcespellchecker imagetools help"
        ],
        menu: {},
        toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent removeformat formatselect',
        // toolbar: "insertfile undo redo | styleselect | bold italic strikethrough | alignleft aligncenter alignright alignjustify | ltr rtl | bullist numlist outdent indent removeformat formatselect| link image media | emoticons charmap | code codesample | forecolor backcolor",
        //external_plugins: { "nanospell": "http://YOUR_DOMAIN.COM/js/tinymce/plugins/nanospell/plugin.js" },
        nanospell_server:"php",
        browser_spellcheck: true,
        relative_urls: false,
        remove_script_host: false,
        file_browser_callback : function(field_name, url, type, win) {
        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
        if (type == 'image') {
            cmsURL = cmsURL + "&type=Images";
        } else {
            cmsURL = cmsURL + "&type=Files";
        }

        tinymce.activeEditor.windowManager.open({
            file: '<?= route('elfinder.tinymce4') ?>',// use an absolute path!
            title: 'File manager',
            width: 900,
            height: 450,
            resizable: 'yes'
        }, {
            setUrl: function (url) {
            win.document.getElementById(field_name).value = url;
            }
        });
        }
    };

    tinymce.init(editor_config);
</script>
<script>
  {!! \File::get(base_path('vendor/barryvdh/laravel-elfinder/resources/assets/js/standalonepopup.min.js')) !!}
</script>
@endpush