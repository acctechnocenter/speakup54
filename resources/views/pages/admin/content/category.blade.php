
@if (session('status'))
    <div class="alert alert-danger">
        {{ session('status') }}
        @if (session('getidticket'))
            @foreach(session('getidticket') as $ticket)
            <br>
            - {{ $ticket->id_ticket }}
            @endforeach
            
        @endif
    </div>
@elseif(session('work'))
    <div class="alert alert-success">
        {{ session('work') }}
        <!-- @if(session('oldcategory') AND session('category'))
            @php
            $old = session('oldcategory');
            $new = session('category');
            @endphp
            @if($old->desc_sub_category != $new->desc_sub_category)
            <br>
                - Deskripsi Sub Kategori : {{$old->desc_sub_category}} ==> {{$new->desc_sub_category}}
            @endif
            @if($old->sub_category_name != $new->sub_category_name)
            <br>
                - Nama Sub Kategori : {{$old->sub_category_name}} ==>  {{$new->sub_category_name}}
            @endif
            @if($old->id_pic != $new->pic)
                @if($new->pic != '')
                <br>
                - Id PIC : {{$old->id_pic}} ==>  {{$new->pic}}
                @endif
            @endif
            @if($old->sla != $new->sla)
            <br>
                - SLA : {{$old->sla}} ==>  {{$new->sla}}
            @endif
        @endif  -->
    </div>
@endif



<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="#">Category</a>
    </li>
    <li class="breadcrumb-item active"><a href="{{url('/administrator/category/create') }}" class="btn btn-info btn-wkwk" role="button">Create Category</a></li>
    <li></li>
    <li></li>
</ol>

<div class="table-responsive">
    <table class="table table-bordered" id="category_table" width="100%" style="font-size:11px;">
        <thead>
            <tr>
            <th></th>
                <!-- <th>Id</th> -->
                <th>Category Name</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>
@push('scripts')
@include('pages.admin.content.mustache_template') 
<script src="{{asset('/vendors/bootstrap-sweetalerts/dist/sweetalert.min.js')}}"></script>
<script>
$(function() {
    var template = Handlebars.compile($("#details-template").html());
    var table = $('#category_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ url('/administrator/category/data') }}",
        columns: [
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "width": "5%",
                "defaultContent": ''
            },
            //{ data: 'id', name: 'id', width: "5%" },
            { data: 'category_name', name: 'category_name', width: "60%" },
            { data: 'action', name: 'action' , width: "12%", orderable: false }
        ],
        order: [[1, 'asc']]
    });
    
    // Add event listener for opening and closing details
    $('#category_table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var tableId = 'posts-' + row.data().id;

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child(template(row.data())).show();
            initTable(tableId, row.data());
            tr.addClass('shown');
            tr.next().find('td').addClass('no-padding bg-gray');
        }
    });

    function initTable(tableId, data) {
        $('#' + tableId).DataTable({
            processing: true,
            serverSide: true,
            ajax: data.sub_category,
            searching: false, paging: false, info: false,
            columns: [
                //{ data: 'id', name: 'id',  width: "5%" },
                { data: 'sub_category_name', name: 'sub_category_name', width: "60%" },
                { data: 'status', name: 'status', width: "12%"},
                { data: 'action', name: 'action' , width: "12%", orderable: false }
            ]
        })
    }
});
</script>
 <script>
     $(document).ready(function(){
        $(document).on('click','.delete', function(e){
            e.preventDefault();
            var data_id = $(this).attr("data-id");
            var href = "{{ url('/administrator/category') }}/" + data_id + "/delete"
            swal({
                title   : "Are you sure?",
                text    : "Update this data will affect the Ticket data!",
                type    : "warning",
                showCancelButton    : true,
                confirmButtonClass  : "btn-danger",
                confirmButtonText   : "Yes",
                cancelButtonText    : "No, cancel",
                closeOnConfirm      : false,
                closeOnCancel       : false
            },
            function(isConfirm) {
                if(isConfirm){
                    swal({
                        title   : "Yeah",
                        text    : "Wait a sec! Your request has been sent!",
                        type    : "success"
                        },
                        function(isConfirm){
                            if(isConfirm){
                            window.location.href = href;
                            }
                        })
                }else
                {
                    swal("Cancelled", "No data update", "error");
                }
            });
        });
        $(document).on('click','.delete-sub', function(e){
            e.preventDefault();
            var data_id = $(this).attr("data-id");
            var href = "{{ url('/administrator/sub-category') }}/" + data_id + "/delete"
            swal({
                title   : "Are you sure?",
                text    : "Update this data will affect the Ticket data!",
                type    : "warning",
                showCancelButton    : true,
                confirmButtonClass  : "btn-danger",
                confirmButtonText   : "Yes",
                cancelButtonText    : "No, cancel",
                closeOnConfirm      : false,
                closeOnCancel       : false
            },
            function(isConfirm) {
                if(isConfirm){
                    swal({
                        title   : "Yeah",
                        text    : "Wait a sec! Your request has been sent!",
                        type    : "success"
                        },
                        function(isConfirm){
                            if(isConfirm){
                            window.location.href = href;
                            }
                        })
                }else
                {
                    swal("Cancelled", "No data update", "error");
                }
            });
        });
     })
</script>
@endpush