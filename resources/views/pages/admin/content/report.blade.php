

<ol class="breadcrumb">
    <li class="breadcrumb-item">
    <a href="{{url('/administrator/report')}}">REPORT</a>
    </li>
    <li></li>
</ol>

<div class="form-group col-lg-12">
    {!! Form::open(['url' => '/administrator/report/export','mehod' => 'POST','role' => 'form', 'novalidate']) !!}
    {!! Form::label('select_period', 'Periode', ['class' => 'select_period']) !!} 
    <select name="select_period"class="form-control editor" id="select_period">
    @foreach($years as $year)
        @if($year->format('Y') == date('Y'))
            <option selected value="{{date('Y')}}">{{date('Y')}}</option>
        @else
            <option selected value="{{$year->format('Y')}}">{{$year->format('Y')}}</option>
        @endif
    @endforeach
    </select>
    <button type="submit">Export</button>
    {!! Form::close() !!}
</div>
<div id="container-report" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

@push('scripts')
<script src="{{ asset('vendors/highchart/code/highcharts.js') }}"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="{{asset('/vendors/bootstrap-sweetalerts/dist/sweetalert.min.js')}}"></script>
<script>
    var url = "{{ url('/administrator/report/data/') }}";
    var url_table = "{{ url('/administrator/report/datatable/') }}";
    var tickets = [];
    var temp = [];
    var colorsChart = ['#123123','#bbd744','#abab78','#666555','#cc569a','#10abc9'];

    function onlyUnique(value, index, self) { 
        return self.indexOf(value) === index;
    };

    $.ajax({
        url: url,
        dataType: 'json',
        cache: false,
        success: function(data){
            makeSeries(data);
            Highcharts.chart({
                chart: {
                    type: 'column',
                    renderTo: 'container-report',
                },
                title: {
                    text: `ACC Report {{date('Y')}}`
                },
                subtitle: {
                    text: 'acc whistle tickets'
                },
                xAxis: {
                    type: "category"
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total'
                    },
                    allowDecimals: false
                },
                legend: {
                    enabled: true
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function(){
                                    var value = $('#select_period').find('option:selected').val();
                                    reportTable.ajax.url(url_table +'/'+ value + '/' + this.options.month).load();
                                }
                            }
                        }
                    },
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0,
                    },
                },
                series: tickets,
                colors: ['#123123','#bbd744','#abab78','#666555','#cc569a','#10abc9']
            });
        }
    });

    $('#select_period').on('change',function(){
        var value = $(this).find('option:selected').val();
        if(value != '' || value != '000'){
            reportTable.ajax.url(url_table +'/'+ value).load();
            $.ajax({
                url: url + '/' + value,
                dataType: 'json',
                cache: false,
                success: function(data){
                    makeSeries(data);
                    Highcharts.chart({
                        chart: {
                            type: 'column',
                            renderTo: 'container-report',
                        },
                        title: {
                            text: `ACC Report ${value}`
                        },
                        subtitle: {
                            text: 'ACC Whistle Compliance Tickets'
                        },
                        xAxis: {
                            type: "category"
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Total'
                            },
                            allowDecimals: false
                        },
                        legend: {
                            enabled: true
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y}</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            series: {
                                cursor: 'pointer',
                                point: {
                                    events: {
                                        click: function(){
                                            var value = $('#select_period').find('option:selected').val();
                                            reportTable.ajax.url(url_table +'/'+ value + '/' + this.options.month).load();
                                        }
                                    }
                                }
                            },
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0,
                            },
                        },
                        series: tickets,
                        colors: ['#123123','#bbd744','#abab78','#666555','#cc569a','#10abc9']
                    });
                }
            })
        }
    })
    
    function makeSeries(data){
        tickets = [];
        for (i=0; i<data.length; i++){
            temp.push(data[i].category_name);
        }
        category = temp.filter(onlyUnique);
        temp = [];
        for (let i = 0; i < category.length; i++) {
            var series = {};
            series.name = category[i];
            series.type = 'column';
            series.keys = ['name','y','drilldown','month','colors'];
            series.data = [];
            for (let j = 0; j < data.length; j++) {
                if (data[j].category_name === series.name) {
                    for (let month = 0; month < 12; month++) {
                        array = [];
                        monthname = "";
                        switch(month){
                            case 0: monthname = "Jan"; break;
                            case 1: monthname = "Feb"; break;
                            case 2: monthname = "Mar"; break;
                            case 3: monthname = "Apr"; break;
                            case 4: monthname = "May"; break;
                            case 5: monthname = "Jun"; break;
                            case 6: monthname = "Jul"; break;
                            case 7: monthname = "Aug"; break;
                            case 8: monthname = "Sep"; break;
                            case 9: monthname = "Oct"; break;
                            case 10: monthname = "Nov"; break;
                            case 11: monthname = "Dec"; break;
                        }
                        array.push(monthname); //keys=>name
                        data[j].bulan == month+1 ? array.push(data[j].total) : array.push(0); //keys=>y                   
                        array.push(monthname);//keys=>drilldown
                        array.push(month+1);//keys=>month
                        array.push(colorsChart[i]);//keys=>colors

                        if(series.data[month] && array[1] !== 0){
                            series.data.splice(month,1,array)  
                        } 
                        else if(series.data[month])
                        {
                            continue;
                        }
                        else
                        {
                            series.data.push(array)
                        }
                    }
                }
            }
            tickets.push(series);
        }
    }
    var reportTable = $('#report_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: url_table,
        order: [[ 0, "desc" ]],
        columns: [
            { data: 'id', name: 'id', width: "2%" },
            { data: 'id_ticket', name: 'id_ticket', width: "10%" },
            { data: 'date_ticket', name: 'date_ticket', width: "12%" },
            { data: 'last_date_response', name: 'last_response', width: "12%" },
            { data: 'category_name', name: 'category_name' , width: "15%"},
            { data: 'sub_category_name', name: 'sub_category_name', width: "18%" },
            { data: 'name_pic', name: 'name_pic', width: "15%" },
            { data: 'sla_terbaru', name: 'sla_terbaru', width: "5%" },
            { data: 'ticket_status', name: 'ticket_status', width: "5%" },
            { data: 'action' , name : 'action', width: "10%", orderable: false}
        ],
       columnDefs: [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
        ]
    });
    
     $(document).ready(function(){
            $(document).on('click','.delete', function(e){
                e.preventDefault();
                var data_id = $(this).attr("data-id");
                var href = "{{ url('/users/complaint') }}/" + data_id + "/delete"
                swal({
                    title   : "Are you sure?",
                    text    : "Update this data will affect the Ticket data!",
                    type    : "warning",
                    showCancelButton    : true,
                    confirmButtonClass  : "btn-danger",
                    confirmButtonText   : "Yes",
                    cancelButtonText    : "No, cancel",
                    closeOnConfirm      : false,
                    closeOnCancel       : false
                },
                function(isConfirm) {
                    if(isConfirm){
                        swal({
                            title   : "Yeah",
                            text    : "Wait a sec! Your request has been sent!",
                            type    : "success"
                            },
                            function(isConfirm){
                                if(isConfirm){
                                window.location.href = href;
                                }
                            })
                    }else
                    {
                        swal("Cancelled", "No data update", "error");
                    }
                });
            })
            
           $(document).on('click','.non-delete', function(e){
                e.preventDefault();
           
                swal({
                    title   : "Info !!",
                    text    : "Tiket sudah di proses, tidak bisa di delete!",
                    type    : "info",
                    showCancelButton    : false,
                    confirmButtonClass  : "btn-danger",
                    confirmButtonText   : "Ok",
                    closeOnConfirm      : false,
                    closeOnCancel       : false
                });
            })
        })

</script>
@endpush