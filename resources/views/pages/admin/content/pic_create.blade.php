<div class="container">
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">PIC</a>
                    </li>
                    <li class="breadcrumb-item active"><a href="{{url('/administrator/pic') }}" class="btn btn-info btn-wkwk" role="button">List PIC</a></li>
                    <li></li>
                    <li></li>
                </ol>

                @if (session('status'))
                    <div class="alert alert-danger">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="panel-body">
                    {!! Form::open(['url' => '/administrator/pic/save', 'role' => 'form', 'novalidate']) !!}
                        
                        <div class="form-group{{ $errors->has('name_pic') ? ' has-error' : '' }}">
                            {!! Form::label('name_pic', 'Name*', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::text('name_pic', null, ['class' => 'form-control', 'required', 'autofocus']) !!}
                                <span class="help-block">
                                    <strong>{{ $errors->first('name_pic') }}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email_pic') ? ' has-error' : '' }}">
                            {!! Form::label('email_pic', 'Email*', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::text('email_pic', null, ['class' => 'form-control', 'required', 'autofocus']) !!}
                                <span class="help-block">
                                    <strong>{{ $errors->first('email_pic') }}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('no_handphone') ? ' has-error' : '' }}">
                            {!! Form::label('no_handphone', 'Phone*', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::text('no_handphone', null, ['class' => 'form-control', 'maxlength' => '15', 'required', 'autofocus']) !!}
                                <span class="help-block">
                                    <strong>{{ $errors->first('no_handphone') }}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('department_pic') ? ' has-error' : '' }}">
                            {!! Form::label('department_pic', 'Department*', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::text('department_pic', null, ['class' => 'form-control', 'required', 'autofocus']) !!}
                                <span class="help-block">
                                    <strong>{{ $errors->first('department_pic') }}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">
                                    Tambah
                                </button>
                                <a href="{{ URL::previous() }}" class="btn btn-primary">Batal</a> 
                            </div>

                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>
</div>

@push('scripts')
<script src="{{asset('vendors/tinymce/jquery.tinymce.min.js')}}"></script>
<script src="{{asset('vendors/tinymce/tinymce.min.js')}}"></script>
<script>
    var base_url = '{{ url("/") }}';
    var editor_config = {
        path_absolute : "/",
        selector: "textarea.editor",
        plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        //   "searchreplace wordcount visualblocks visualchars code fullscreen",
        //   "insertdatetime media nonbreaking save table contextmenu directionality",
        //   "emoticons template paste textcolor colorpicker textpattern codesample",
        //   "fullpage toc tinymcespellchecker imagetools help"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic strikethrough | alignleft aligncenter alignright alignjustify | ltr rtl | bullist numlist outdent indent removeformat formatselect| link image media | emoticons charmap | code codesample | forecolor backcolor",
        //external_plugins: { "nanospell": "http://YOUR_DOMAIN.COM/js/tinymce/plugins/nanospell/plugin.js" },
        nanospell_server:"php",
        browser_spellcheck: true,
        relative_urls: false,
        remove_script_host: false,
        file_browser_callback : function(field_name, url, type, win) {
        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
        if (type == 'image') {
            cmsURL = cmsURL + "&type=Images";
        } else {
            cmsURL = cmsURL + "&type=Files";
        }

        tinymce.activeEditor.windowManager.open({
            file: '<?= route('elfinder.tinymce4') ?>',// use an absolute path!
            title: 'File manager',
            width: 900,
            height: 450,
            resizable: 'yes'
        }, {
            setUrl: function (url) {
            win.document.getElementById(field_name).value = url;
            }
        });
        }
    };

    tinymce.init(editor_config);
</script>
<script>
  {!! \File::get(base_path('vendor/barryvdh/laravel-elfinder/resources/assets/js/standalonepopup.min.js')) !!}
</script>
@endpush