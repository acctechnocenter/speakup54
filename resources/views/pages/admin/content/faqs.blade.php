

<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="#">Faq's</a>
    </li>
    <li class="breadcrumb-item active"><a href="{{url('/administrator/faqs/create') }}" class="btn btn-info btn-wkwk" role="button">Create Faq's</a></li>
    <li></li>
    <li></li>
</ol>

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

<div class="table-responsive">
    <table class="table table-bordered" id="blog-table" width="100%" style="font-size:11px;">
        <thead>
            <tr>
                <th>Question</th>
                <th>Publish</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>

@push('scripts')
<script>
$(function() {
    $('#blog-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ url('/administrator/faqs/data') }}",
        columns: [
            { 
                data: 'faqs_question',
                name: 'faqs_question',
                width: "60%",
                render: function ( data, type, row, meta ) {
                    return $("<div/>").html(data).text();
                }
            },
            { data: 'flag_publish', name: 'flag_publish', width: "10%" },
            { data: 'action', name: 'action' , width: "12%", orderable: false }
        ]
    });
});
</script>
<script src="{{asset('/vendors/bootstrap-sweetalerts/dist/sweetalert.min.js')}}"></script>
<script>
$(document).ready(function(){
    $(document).on('click','.delete', function(e){
        e.preventDefault();
        var data_id = $(this).attr("data-id");
        var href = "{{ url('/administrator/faqs') }}/" + data_id + "/delete"
        swal({
            title   : "Are you sure?",
            text    : "Update this data will affect the Ticket data!",
            type    : "warning",
            showCancelButton    : true,
            confirmButtonClass  : "btn-danger",
            confirmButtonText   : "Yes",
            cancelButtonText    : "No, cancel",
            closeOnConfirm      : false,
            closeOnCancel       : false
        },
        function(isConfirm) {
            if(isConfirm){
                swal({
                    title   : "Yeah",
                    text    : "Wait a sec! Your request has been sent!",
                    type    : "success"
                    },
                    function(isConfirm){
                        if(isConfirm){
                        window.location.href = href;
                        }
                    })
            }else
            {
                swal("Cancelled", "No data update", "error");
            }
        });
    })
})
</script>
@endpush