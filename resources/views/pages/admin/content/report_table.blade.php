<div class="table-responsive">
    <table class="table table-bordered" id="report_table" width="100%" style="font-size:11px;">
        <thead>
            <tr>
                <th>No</th>
                <th>Ticket ID</th>
                <th>Request Date</th>
                <th>Last Response</th>
                <th>Category</th>
                <th>Sub Category</th>
                <th>PIC</th>
                <th>SLA</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>


@push('scripts')
<script>
</script>
@endpush