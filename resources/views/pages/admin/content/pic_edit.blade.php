<div class="container">
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">PIC</a>
                    </li>
                    <li class="breadcrumb-item active"><a href="{{url('/administrator/pic') }}" class="btn btn-info btn-wkwk" role="button">List PIC</a></li>
                    <li></li>
                    <li></li>
                </ol>

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="panel-body">
                    {!! Form::model($pic, ['url' => '/administrator/pic/save', 'role' => 'form', 'novalidate']) !!}
                        {!! Form::hidden('id', null, ['class' => 'form-control']) !!}
                        <div class="form-group{{ $errors->has('name_pic') ? ' has-error' : '' }}">
                            {!! Form::label('name_pic', 'Name*', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::text('name_pic', null, ['class' => 'form-control', 'required', 'autofocus']) !!}
                                <span class="help-block">
                                    <strong>{{ $errors->first('name_pic') }}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email_pic') ? ' has-error' : '' }}">
                            {!! Form::label('email_pic', 'Email*', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::text('email_pic', null, ['class' => 'form-control', 'required', 'autofocus']) !!}
                                <span class="help-block">
                                    <strong>{{ $errors->first('email_pic') }}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('no_handphone') ? ' has-error' : '' }}">
                            {!! Form::label('no_handphone', 'Phone*', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::text('no_handphone', null, ['class' => 'form-control', 'required', 'autofocus']) !!}
                                <span class="help-block">
                                    <strong>{{ $errors->first('no_handphone') }}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('department_pic') ? ' has-error' : '' }}">
                            {!! Form::label('department_pic', 'Department*', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::text('department_pic', null, ['class' => 'form-control', 'required', 'autofocus']) !!}
                                <span class="help-block">
                                    <strong>{{ $errors->first('department_pic') }}</strong>
                                </span>
                            </div>
                        </div>

                        @if(Auth::user()->group == 'authorizer')
                            <div class="form-group">
                                {!! Form::label('flag_active','Activation',['class' => 'col-md-3 control-label']) !!}
                                <div class="col-md-9">   
                                    <label class="radio-inline"><input type="radio" name="flag_active" value="yes" {{$pic->flag_active == "APPROVE" ? 'checked' : ''}}>Yes</label>
                                    <label class="radio-inline"><input type="radio" name="flag_active" value="no" {{$pic->flag_active == "REJECT" ? 'checked' : ''}}>No</label>
                                </div>
                            </div>
                        @endif

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <button type="submit" class="btn btn-primary" id = "update">
                                    Perbarui
                                </button>
                                <a href="{{ URL::previous() }}" class="btn btn-primary">Batal</a> 
                            </div>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>
</div>
@push('scripts')
<script src="{{asset('/vendors/bootstrap-sweetalerts/dist/sweetalert.min.js')}}"></script>
    <script>
    $('#update').on('click', function(e){
        e.preventDefault();
        var form = e.target.form;
        swal({
            title   : "Are you sure?",
            text    : "Update this data will affect the Ticket data!",
            type    : "warning",
            showCancelButton    : true,
            confirmButtonClass  : "btn-danger",
            confirmButtonText   : "Yes",
            cancelButtonText    : "No, cancel",
            closeOnConfirm      : false,
            closeOnCancel       : false
        },
        function(isConfirm) {
            if(isConfirm){
                swal({
                    title   : "Yeah",
                    text    : "Wait a sec! Your request has been sent!",
                    type    : "success"
                    },
                    function(isConfirm){
                        if(isConfirm)
                        form.submit();
                    })
            }else
            {
                swal("Cancelled", "No data update", "error");
            }
        });
    })
    </script>
@endpush