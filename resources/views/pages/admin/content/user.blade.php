

<ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#">Users</a>
        </li>
        {{-- <li class="breadcrumb-item active"><a href="{{url('/administrator/category/create') }}" class="btn btn-info btn-wkwk" role="button">Create Users</a></li> --}}
        <li></li>
        <li></li>
    </ol>
    
    <div class="table-responsive">
        <table class="table table-bordered" id="user_table" width="100%" style="font-size:11px;">
            <thead>
                <tr>
                    <!-- <th>Id</th> -->
                    <th>Fullname</th>
                    <th>No Handphone</th>
                    <th>Email</th>
                    <th>Group</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>

@push('scripts')
<script src="{{asset('/vendors/bootstrap-sweetalerts/dist/sweetalert.min.js')}}"></script>
<script>
$(function() {
    $('#user_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ url('/administrator/user/data') }}",
        // searching: false, paging: false, info: false,
        columns: [
           // { data: 'id', name: 'id',  width: "5%" },
            { data: 'fullname', name: 'fullname', width: "30%" },
            { data: 'no_handphone', name: 'no_handphone', width: "15%" },
            { data: 'email', name: 'email', width: "25%" },
            { data: 'group', name: 'group', width: "10%" },
            { data: 'action', name: 'action', width: "18%", orderable: false }
        ]
    });
    $(document).ready(function(){
        $(document).on('click','.delete', function(e){
            e.preventDefault();
            // var url = e.target.attributes.getNamedItem('href').value;
            var data_id = $(this).attr("data-id");
            var href = "{{ url('/administrator/user') }}/" + data_id + "/delete"
            swal({
                title   : "Are you sure?",
                text    : "Update this data will affect the Ticket data!",
                type    : "warning",
                showCancelButton    : true,
                confirmButtonClass  : "btn-danger",
                confirmButtonText   : "Yes",
                cancelButtonText    : "No, cancel",
                closeOnConfirm      : false,
                closeOnCancel       : false
            },
            function(isConfirm) {
                if(isConfirm){
                    swal({
                        title   : "Yeah",
                        text    : "Wait a sec! Your request has been sent!",
                        type    : "success"
                        },
                        function(isConfirm){
                            if(isConfirm){
                            window.location.href = href;
                            }
                        })
                }else
                {
                    swal("Cancelled", "No data update", "error");
                }
            });
        })
     })
});
</script>
@endpush