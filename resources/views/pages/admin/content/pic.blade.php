@if (session('status'))
    <div class="alert alert-danger">
        {{ session('status') }}
        @if (session('getidticket'))
            @foreach(session('getidticket') as $ticket)
            <br>
            - {{ $ticket->id_ticket }}
            @endforeach
            
        @endif
    </div>
@elseif(session('work'))
    <div class="alert alert-success">
        {{ session('work') }}
    </div>
@endif

<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="#">PIC</a>
    </li>
    <li class="breadcrumb-item active"><a href="{{url('/administrator/pic/create') }}" class="btn btn-info btn-wkwk" role="button">Create PIC</a></li>
    <li></li>
    <li></li>
</ol>

<div class="table-responsive">
    <table class="table table-bordered" id="pic-table" width="100%" style="font-size:11px;">
        <thead>
            <tr>
                <th>Name PIC</th>
                <th>Phone Number</th>
                <th>Email</th>
                <th>Department</th>
                <th>Status Approval</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>

@push('scripts')
<script src="{{asset('/vendors/bootstrap-sweetalerts/dist/sweetalert.min.js')}}"></script>
    <script>
        $(function() {
            $('#pic-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ url('/administrator/pic/data') }}",
                columns: [
                    // { data: 'id', name: 'id' },
                    { data: 'name_pic', name: 'name_pic' },
                    { data: 'no_handphone', name: 'no_handphone'},
                    { data: 'email_pic', name: 'email_pic' },
                    { data: 'department_pic', name: 'department_pic'},
                    { data: 'status', name : 'status'},
                    { data: 'action', name: 'action' , orderable: false}
                ]
            });
        });
    </script>
    <script>
        $(document).ready(function(){
            $(document).on('click','.delete', function(e){
                e.preventDefault();
                var data_id = $(this).attr("data-id");
                var href = "{{ url('/administrator/pic') }}/" + data_id + "/delete"
                swal({
                    title   : "Are you sure?",
                    text    : "Update this data will affect the Ticket data!",
                    type    : "warning",
                    showCancelButton    : true,
                    confirmButtonClass  : "btn-danger",
                    confirmButtonText   : "Yes",
                    cancelButtonText    : "No, cancel",
                    closeOnConfirm      : false,
                    closeOnCancel       : false
                },
                function(isConfirm) {
                    if(isConfirm){
                        swal({
                            title   : "Yeah",
                            text    : "Wait a sec! Your request has been sent!",
                            type    : "success"
                            },
                            function(isConfirm){
                                if(isConfirm){
                                window.location.href = href;
                                }
                            })
                    }else
                    {
                        swal("Cancelled", "No data update", "error");
                    }
                });
            })
        })
    </script>
    
@endpush