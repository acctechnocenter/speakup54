<div class="container">
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <ol class="breadcrumb">
                    {{-- <li class="breadcrumb-item">
                        <a href="#">Faq's</a>
                    </li> --}}
                    <li class="breadcrumb-item active"><a href="{{url('/administrator/category') }}" class="btn btn-info btn-wkwk"
                            role="button">List Category</a></li>
                    <li></li>
                    <li></li>
                </ol>
                <div class="panel-body">
                    @if(session('errors'))
                    <div class="alert alert-danger">
                        <div><strong>{{ $errors->first('category_name') }}</strong></div>
                        <div><strong>{{ $errors->first('desc_category') }}</strong></div>
                        <div><strong>{{ $errors->first('id_category') }}</strong></div>
                        <div><strong>{{ $errors->first('sub_category_name') }}</strong></div>
                        <div><strong>{{ $errors->first('desc_sub_category') }}</strong></div>
                        <div><strong>{{ $errors->first('pic') }}</strong></div>
                        <div><strong>{{ $errors->first('sla') }}</strong></div>
                    </div>
                    @endif

                    <ul class="nav nav-tabs">
                        <li><a data-toggle="tab" href="#home">Create CategorY</a></li>
                        <li><a data-toggle="tab" href="#menu1">Create Sub Category</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="home" class="tab-pane fade in">
                            {!! Form::open(['url' => '/administrator/category/saveparent','role' => 'form',
                            'novalidate']) !!}
                            <h5> Create Category</h5> <br>
                            <div class="form-group row {{ $errors->has('category_name') ? 'has-error' : ''}}">
                                {!! Form::label('category_name','Category*: ',['class' => 'col-md-3 control-label'])
                                !!}
                                <div class="col-md-9">
                                    {!! Form::text('category_name',null,['class'=>'form-control editor', 'required'])
                                    !!}
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category_name') }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('desc_category','Category Description: ',['class' => 'col-md-3
                                control-label']) !!}
                                <div class="col-md-9">
                                    {!! Form::textarea('desc_category', null, ['class' => 'form-control editor']) !!}
                                    <span class="help-block">
                                        <strong>{{ $errors->first('desc_category') }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 d-flex flex-row-reverse">
                                    <a href="{{ URL::previous() }}" class="btn btn-primary">Batal</a>
                                    <button type="submit" class="btn btn-primary  mr-1">
                                        Tambah
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>

                        <div id="menu1" class="tab-pane fade">
                            {!! Form::open(['url' => '/administrator/category/savesub', 'role' => 'form',
                            'novalidate']) !!}
                            <h5> Create Sub Category</h5> <br>
                            <div class="form-group row">
                                {!! Form::label('category_name','Category*: ',['class' => 'col-md-3 control-label'])
                                !!}
                                <div class="col-md-9">
                                    <select id="inputSelectCategory" class="form-control editor" name="id_category">
                                        <option value="">-- PILIH KATEGORI --</option>
                                        @foreach($categories as $category)
                                        <option value={{$category->id}}>{{ $category->category_name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block">
                                        <strong>{{ $errors->first('id_category') }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row {{ $errors->has('category_name') ? 'has-error' : ''}}">
                                {!! Form::label('sub_category_name','Sub Category*: ',['class' => 'col-md-3
                                control-label']) !!}
                                <div class="col-md-9">
                                    {!! Form::text('sub_category_name',null,['class'=>'form-control editor',
                                    'required']) !!}
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sub_category_name') }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('desc_sub_category','Sub Category Description : ',['class' => 'col-md-3
                                control-label']) !!}
                                <div class="col-md-9">
                                    {!! Form::textarea('desc_sub_category', null, ['class' => 'form-control editor'])
                                    !!}
                                    <span class="help-block">
                                        <strong>{{ $errors->first('desc_sub_category') }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('pic','PIC : ',['class' => 'col-md-3 control-label']) !!}
                                <div class="col-sm-9">
                                    <select id="pic" class="form-control" class="form-control editor" name="pic">
                                        <option value="">-- PILIH PIC --</option>
                                        @foreach($pics as $pic)
                                        @if($pic->flag_active == 'APPROVE')
                                        <option value={{$pic->id}}>{{ $pic->name_pic }}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pic') }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('sla','SLA (Days) : ',['class' => 'col-md-3 control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('sla', null, ['class' => 'form-control editor','placeholder' =>
                                    'Days']) !!}
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sla') }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 d-flex flex-row-reverse">
                                    <a href="{{ URL::previous() }}" class="btn btn-primary">Batal</a>
                                    <button type="submit" class="btn btn-primary mr-1">
                                        Tambah
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>
</div>