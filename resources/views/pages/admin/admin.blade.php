@extends('layouts.app')

@section('content')
    <!--================Banner Area =================-->
    <section class="banner_area">
            <div class="container">
                <div class="banner_text_inner">
                    <h4>Halaman Administrator</h4>
                </div>
            </div>
        </section>
        <!--================End Banner Area =================-->
		
		<section class="main_slider_area">
			<div class="copy_right_area_top">
  
            </div>
		</section>

        <!--================Static Area =================-->
        <section class="static_area">
            <div class="container">
                <div class="static_inner">
                    <div class="row">
						<div class="col-lg-3">
								<ul class="ca-menu">
									@if(Auth::user()->group != 'authorizer')
									<li>
										<a href="{{url('/users/create-complaint')}}">
											<i class="fa fa-pencil-square-o ca-icon" aria-hidden="true"></i>
											<div class="ca-content">
												<h5 class="ca-main">Buat Pelaporan</h5>
												<h6 class="ca-sub">Menu Pelaporan</h6>
											</div>
										</a>
									</li>
									@endif
									<!-- <li>
										<a href="{{url('/users/list-complaint')}}">
											<i class="fa fa fa-table ca-icon" aria-hidden="true"></i>
											<div class="ca-content">
												<h5 class="ca-main">List Compliance</h5>
												<h6 class="ca-sub">Daftar laporan mu</h6>
											</div>
										</a>
									</li> -->
									@if(Auth::user()->group == 'administrator' )
									<li>
										<a href="{{url('/administrator/report')}}">
											<i class="fa fa fa-key ca-icon" aria-hidden="true"></i>
											<div class="ca-content">
												<h5 class="ca-main">Administrator</h5>
												<h6 class="ca-sub">Menu Administrator</h6>
											</div>
										</a>
									</li>
									@endif
									@if(Auth::user()->group == 'authorizer')
									<li>
										<a href="{{url('/administrator/report')}}">
											<i class="fa fa fa-key ca-icon" aria-hidden="true"></i>
											<div class="ca-content">
												<h5 class="ca-main">Administrator</h5>
												<h6 class="ca-sub">Menu Administrator</h6>
											</div>
										</a>
									</li>
									<li>
										<a href="{{url('/users/list-complaint-approval')}}">
											<i class="fa fa fa-key ca-icon" aria-hidden="true"></i>
											<div class="ca-content">
												<h5 class="ca-main">List Tiket Approval</h5>
												<h6 class="ca-sub">List tiket yang perlu Approval</h6>
											</div>
										</a>
									</li>
									@endif
								</ul>
                        </div>
                        <!-- CONTENT -->
                        <div class="col-lg-9">
                            <div class="static_main_content">
								<div class="contact_form">
									<div class="col-md-12">
										<div class="container">
											<div class="tab" role="tabpanel">
												<!-- Nav tabs -->
												<ul class="nav nav-tabs" role="tablist">
													<li role="presentation" class="{{(!empty($view) AND $view == 'report') ?  'active' : ''}}"><a href="{{url('/administrator/report')}}">Report</a></li>
													<li role="presentation" class="{{(!empty($view) AND ($view == 'list-pic' OR $view == 'create-pic' OR $view == 'edit-pic')) ?  'active' : ''}}" ><a href="{{url('/administrator/pic')}}" >PIC</a></li>
													<li role="presentation" class="{{(!empty($view) AND ($view == 'list-category' OR $view == 'create-category' OR $view == 'edit-category')) ?  'active' : ''}}"><a href="{{url('/administrator/category')}}" >Category</a></li>
													<li role="presentation" class="{{(!empty($view) AND ($view == 'list-blog' OR $view == 'create-blog' OR $view == 'edit-blog')) ?  'active' : ''}}"><a href="{{url('/administrator/blog')}}" >Blog</a></li>
													<li role="presentation" class="{{(!empty($view) AND ($view == 'list-faqs' OR $view == 'create-faqs' OR $view == 'edit-faqs')) ?  'active' : ''}}" ><a href="{{url('/administrator/faqs')}}" > Faq's</a></li>
													<li role="presentation" class="{{(!empty($view) AND ($view == 'list-slider' OR $view == 'create-slider' OR $view == 'edit-slider')) ?  'active' : ''}}" ><a href="{{url('/administrator/slider')}}" > Slider</a></li>
													<li role="presentation" class="{{(!empty($view) AND ($view == 'user' OR $view == 'list-user' OR $view == 'create-user' OR $view == 'edit-user')) ?  'active' : ''}}"><a href="{{url('/administrator/user')}}">Users</a></li>
													<li role="presentation" class="{{(!empty($view) AND ($view == 'list-quote' OR $view == 'create-quote' OR $view == 'edit-quote')) ?  'active' : ''}}"><a href="{{url('/administrator/quote')}}">Quote</a></li>
												</ul>
												<!-- Tab panes -->
												<div class="tab-content ">
													<div role="tabpanel">
														@if (!empty($view) AND $view == 'report')
															@include('pages.admin.content.report')
														@endif
														<!-- CATEGORY SUB-->
														@if (!empty($view) AND $view == 'list-category')
															@include('pages.admin.content.category')
														@endif
														@if (!empty($view) AND $view == 'create-category')
															@include('pages.admin.content.category_create')
														@endif
														@if (!empty($view) AND $view == 'edit-parent-category')
															@include('pages.admin.content.category_parent_edit')
														@endif
														@if (!empty($view) AND $view == 'edit-sub-category')
															@include('pages.admin.content.category_sub_edit')
														@endif
														<!-- END CATEGORY -->
														<!-- BLOG -->
														@if (!empty($view) AND $view == 'list-blog')
															@include('pages.admin.content.blog')
														@endif
														@if (!empty($view) AND $view == 'create-blog')
															@include('pages.admin.content.blog_create')
														@endif
														@if (!empty($view) AND $view == 'edit-blog')
															@include('pages.admin.content.blog_edit')
														@endif
														<!-- END BLOG -->
														<!-- FAQ'S -->
														@if (!empty($view) AND $view == 'list-faqs')
															@include('pages.admin.content.faqs')
														@endif
														@if (!empty($view) AND $view == 'create-faqs')
															@include('pages.admin.content.faqs_create')
														@endif
														@if (!empty($view) AND $view == 'edit-faqs')
															@include('pages.admin.content.faqs_edit')
														@endif
														<!-- END FAQ'S -->
														<!-- SLIDER -->
														@if (!empty($view) AND $view == 'list-slider')
															@include('pages.admin.content.slider')
														@endif
														@if (!empty($view) AND $view == 'create-slider')
															@include('pages.admin.content.slider_create')
														@endif
														@if (!empty($view) AND $view == 'edit-slider')
															@include('pages.admin.content.slider_edit')
														@endif
														<!-- END SLIDER -->
														<!-- QUOTE -->
														@if (!empty($view) AND $view == 'list-quote')
															@include('pages.admin.content.quote')
														@endif
														@if (!empty($view) AND $view == 'create-quote')
															@include('pages.admin.content.quote_create')
														@endif
														@if (!empty($view) AND $view == 'edit-quote')
															@include('pages.admin.content.quote_edit')
														@endif
														<!-- END QUOTE -->
														<!-- PIC'S -->
														@if (!empty($view) AND $view == 'list-pic')
															@include('pages.admin.content.pic')
														@endif
														@if (!empty($view) AND $view == 'create-pic')
															@include('pages.admin.content.pic_create')

														@endif
														@if (!empty($view) AND $view == 'edit-pic')
															@include('pages.admin.content.pic_edit')
														@endif
														<!-- END PIC'S -->
														<!-- USER -->
														@if (!empty($view) AND $view == 'list-user')
															@include('pages.admin.content.user')
														@endif
														@if (!empty($view) AND $view == 'edit-user')
															@include('pages.admin.content.user_edit')
														@endif
														<!-- END USER -->
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
                            </div>
                        </div>
                        <!-- END CONTENT -->
					</div>
					@if (!empty($view) AND $view == 'report')
						@include('pages.admin.content.report_table')
					@endif
                </div>
            </div>
        </section>
        <!--================End Static Area =================-->
@endsection
