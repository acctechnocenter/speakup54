@extends('layouts.app')

@section('content')
<!--================Slider Area =================-->
<section class="main_slider_area">
    <div id="main_slider" class="rev_slider" data-version="5.3.1.6">
        <ul>
            @forelse ($slider as $key => $contentSlider)
            <li data-index="rs-{{$key}}" data-transition="zoomout">
                <img src="{{URL::asset('/images/' . $contentSlider->image_url . '')}}" alt="" data-bgposition="center center"
                    data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                <div class="slider_text_box">
                    <div class="tp-caption tp-resizeme secand_text " data-x="['center','center','center','center','center','center']"
                        data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-30','0','0','0','0']"
                        data-fontsize="['40','40','40','20','20','20']" data-lineheight="['60','60','60','36','36','30']"
                        data-width="100%" data-height="none" data-whitespace="normal" data-type="text"
                        data-responsive_offset="on" data-transform_idle="o:1;" data-frames='[{"delay": 500, "speed": 300, "from": "opacity: 0", "to": "opacity: 1"},{"delay": "wait", "speed": 300, "to": "opacity: 0"}]'
                        data-textAlign="['center','center','center','center','center','center']" style="z-index: 8;font-family:'Poppins', sans-serif;font-weight:700;color:#fff;">{!!
                        $contentSlider->title !!}
                    </div>
                </div>
                @if(!empty($contentSlider->additional_text))
                <div class="tp-caption tp-resizeme secand_text mobile_first_text" data-x="['center','center','center','center','center','center']"
                    data-hoffset="['0','80','80','0']" data-y="['middle', 'middle', 'top', 'bottom']" data-width="100%"
                    data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on"
                    data-transform_idle="o:1;" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                    data-textAlign="['center','center','center','center','center','center']" style="z-index: 8;font-family:'Poppins', sans-serif;font-weight:500;color:#fff;">
                    {!! $contentSlider->additional_text !!}
                </div>
                @endif
                @if(!empty($contentSlider->button_text))
                <div class="tp-caption tp-resizeme slider_button" data-x="['center','center','center','center','center','center']"
                    data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['60','80','80','60','60','60']"
                    data-width="['500']" data-height="['auto']" data-whitespace="nowrap" data-type="text"
                    data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                    data-textAlign="['center','center','center','center','center','center']">
                    <a class="tp_btn slider" href="{{url('/'. $contentSlider->button_url .'')}}">&nbsp;&nbsp;{!!
                        $contentSlider->button_text !!}&nbsp;&nbsp;</a>
                </div>
                @endif
            </li>
            @empty
            <!-- NO DATA -->
            @endforelse
        </ul>
    </div>
    <div class="copy_right_area_top">

    </div>
</section>
<!--================End Slider Area =================-->


<!--================3 BOX =================-->
<section class="creative_feature_area" style="background: #ffffff;">
    <div class="container">
        <div class="c_feature_box">
            <div class="row">
                <div class="col-lg-4">
                    <div class="c_box_item">
                        <a href="#myAnchor" rel="" id="anchor1" class="anchorLink">
                            <h4><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Tentang ACC Whistle</h4>
                        </a>
                        <p>Merupakan media bagi karyawan, mitra bisnis / pemangku kepentingan dan pelanggan Astra Credit Companies untuk mengungkapkan permasalahan yang berkaitan dengan perilaku yang tidak baik. </p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="c_box_item">
                        <a href="#myAnchor2" rel="" id="anchor2" class="anchorLink">
                            <h4><i class="fa fa-clock-o" aria-hidden="true"></i> Tujuan ACC Whistle</h4>
                        </a>
                        <p>Memberdayakan karyawan, mitra bisnis / pemangku kepentingan dan pelanggan untuk berperan serta dalam upaya penegakan Kode
                            Etik Perilaku Karyawan </p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="c_box_item">
                        <a href="#myAnchor3" rel="" id="anchor3" class="anchorLink">
                            <h4><i class="fa fa-diamond" aria-hidden="true"></i>Pelapor & Pengaduan</h4>
                        </a>
                        <p>ACC Whistle memberikan perlindungan kepada Pelapor dari segala bentuk ancaman, intimidasi
                            atau tindakan tidak menyenangkan dari pihak manapun. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<br><br>


<section class="industries_area" name="myAnchor" id="myAnchor">
    <div class="container" style="border-left: 3px solid #f7f8fb; border-bottom: 3px solid #f7f8fb;">
        <div class="digital_feature p_100">
            <div class="row">
                <div class="col-lg-6" style="">
                    <div class="d_feature_img">
                        <figure><img src="img/tentang.jpg" alt=""></figure>
                    </div>
                </div>
                <div class="col-lg-6" style="border-left: 0px solid #f7f8fb;">
                    <div class="d_feature_text">
                        <div class="main_title">
                            <h2>Tentang ACC Whistle</h2>
                        </div>
                        <div style="text-align: justify">
                            <p>
                                  ACC Whistle adalah suatu sarana bagi karyawan, mitra bisnis / pemangku kepentingan dan pelanggan yang mudah diakses dan berfokus   pada pembentukan budaya pelaporan yang didasari rasa aman dan rahasia serta memastikan seluruh pelaporan yang disampaikan, ditangani dengan baik.
  
  Departemen yang terkait dalam pelaksanaan tindak lanjut pelaporan terdiri dari :
  
  - Corporate Compliance <br>
  - Fraud Risk Management <br>
  - Human Capital (Industrial Relation) <br>
  - Corporate Communication. 
                            </p>
                            <a class="read_btn" href="{{url('/tentang-acc-whistle?Element=tentang_acc_whistle')}}">Read
                                More</a>
                        </div>
                        {{-- <a class="read_btn" href="#">Read more</a> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="industries_area" name="myAnchor2" id="myAnchor2">
    <div class="container" style="border-right: 3px solid #f7f8fb;border-bottom: 3px solid #f7f8fb;">
        <div class="digital_feature p_100">
            <div class="row">
                <div class="col-lg-6" style="border-right: 0px solid #f7f8fb;">
                    <div class="d_feature_text">
                        <div class="main_title">
                            <h2>Tujuan ACC Whistle</h2>
                        </div>
                        <div style="text-align: justify">
                            <ol style="margin: -10px 29px;padding:0;">
                                <li>
                                    Memberdayakan karyawan, mitra bisnis / pemangku kepentingan dan pelanggan untuk berperan serta dalam upaya
                                    penegakan Kode Etik Perilaku Karyawan, Peraturan Perusahaan, maupun hukum yang
                                    berlaku
                                    di Indonesia.
                                </li>
                                <li>
                                    Menyediakan berbagai sarana untuk pertanyaan, pelaporan dan masukan serta saluran
                                    umpan balik yang dapat dipercaya oleh semua pihak.
                                </li>
                                <li>
                                    Menyediakan sarana bagi karyawan jika pelaporan berjenjang kepada atasan langsung
                                    dirasa tidak memungkinkan bagi karyawan.
                                </li>
                                <li>
                                    Mendapatkan informasi dari karyawan, mitra bisnis dan pelanggan mengenai adanya
                                    potensi, atau rencana, atau kejadian kasus pelanggaran Fraud, Etika Bisnis, Pedoman
                                    Perilaku, Peraturan Perusahaan dan Peraturan Perundangan-undangan yang memerlukan
                                    pencegahan maupun penanganan sesegera mungkin.
                                </li>
                            </ol>
                            <!--<a class="read_btn" href="{{url('/tentang-acc-whistle?Element=tentang_acc_whistle')}}">Read More</a>-->
                        </div>
                        <!-- <a class="read_btn" href="#">Read more</a> -->
                    </div>
                </div>
                <div class="col-lg-6" style="">
                    <div class="d_feature_img">
                        <figure><img src="img/tujuan.jpg" alt=""></figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="industries_area" name="myAnchor3" id="myAnchor3">
    <div class="container" style="border-left: 3px solid #f7f8fb;border-bottom: 1px solid #f7f8fb;">
        <div class="digital_feature p_100">
            <div class="row">
                <div class="col-lg-6" style="">
                    <div class="d_feature_img">
                        <figure><img src="img/etika.jpg" alt=""></figure>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="d_feature_text">
                        <div class="main_title">
                            <h3>Ruang Lingkup Pelapor & Pengaduan</h3>
                        </div>
                        <div style="text-align: justify">
                            Kami menjamin kerahasiaan identitas dan pelaporan anda. Hanya pelapor, user dan ACC Whistle
                            Administrator yang mempunyai wewenang untuk mengetahui isi pelaporan.
                        </div>
                        <a class="read_btn" href="{{url('/tentang-acc-whistle?Element=ruang_lingkup')}}">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- <section class="industries_area" name="myAnchor3" id="myAnchor3">
    <div class="container" style="border-left: 3px solid #f7f8fb;border-bottom: 1px solid #f7f8fb;">
        <div class="digital_feature p_100">
            <div class="row">
                <div class="col-lg-6" style="">
                    <div class="d_feature_img">
                        <figure><img src="img/about_speak_up.jpeg" alt=""></figure>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="d_feature_text">
                        <div class="main_title">
                            <h2>Etika Kerja & Etika Bisnis</h2>
                        </div>
                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                            commodo consequat. Duis aute irure dolor in reprehenderi.</p>
                        <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia dese mollit anim
                            id est laborum. Sed ut perspiciatis unde omnis iste.</p>
                        {{-- <a class="read_btn" href="https://www.acc.co.id/files/page_media/2015/12/ojk/file-etika_bisnis.pdf">Download</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}



<!--================Our Service Area =================-->
<section class="service_area">
    <div class="container">
        <div class="project_inner">
            <div class="center_title">
                <h2>Siap dan berani untuk mengungkapkan kebenaran? </h2>
                <p>Sampaikan dan laporkan kepada kami adanya indikasi Fraud (kecurangan), Pelanggaran etika, keluh kesah dan pelanggaran penggunaan media sosial   yang terjaga kerahasaiannya</p>
            </div>
            <a class="tp_btn" href="{{url('/create-complaint')}}">&nbsp;&nbsp;LAPORKAN KEPADA KAMI&nbsp;&nbsp;</a>
        </div>
    </div>
</section>
<!--================End Our Service Area =================-->

<!--================Testimonials Area =================-->
<section class="testimonials_area p_100" style="background-color:#f7f7f7;">
    <div class="container">
        <div class="testimonials_slider owl-carousel">
            @forelse ($quote as $countQuote)
            <div class="item">
                <div class="media">
                    @if(!empty($countQuote->image_url))
                    <img class="d-flex rounded-circle" src="{{URL::asset('/images/' . $countQuote->image_url . '')}}"
                        alt="Image" style="width:170px;height:161px;">
                    @endif
                    <div class="media-body">
                        <img src="img/dotted-icon.png" alt="">
                        @if(!empty($countQuote->quote))
                        <p>{!!$countQuote->quote!!}</p>
                        @endif
                        @if(!empty($countQuote->person_name) AND !empty($countQuote->job))
                        <h4><a href="#">{!!$countQuote->person_name!!}</a> - {!!$countQuote->job!!}</h4>
                        @endif
                    </div>
                </div>
            </div>
            @empty
            @endforelse

        </div>
    </div>
</section>
<!--================End Testimonials Area =================-->

<!--================Latest News Area =================-->
<section class="latest_news_area p_100">
    <div class="container">
        <div class="b_center_title">
            <h2>ACC Whistle Blog</h2>
            <!--<p> Informasi Terbaru department Compliance </p>-->
        </div>
        <div class="l_news_inner">
            <div class="row">

                @forelse ($blog as $content)
                <div class="col-lg-4 col-md-6">
                    <div class="l_news_item">
                        <div class="l_news_img" style="max-width: 370px;max-height: 220px;"><a href="#"><img class="img-fluid"
                                    src="{{URL::asset('/images/' . $content->image_url . '')}}" alt="" style="min-height: 221px;"></a></div>
                        <div class="l_news_content">
                            <a href="{{url('/blog/'. $content->slug_blog .'')}}">
                                <h4 style="max-height: 90px;">{{ $content->title }}</h4>

                            </a>
                            <p>{{ str_limit(strip_tags($content->post_body), 100) }}</p>
                            <a class="more_btn" href="{{url('/blog/'. $content->slug_blog .'')}}">Read More</a>
                        </div>
                    </div>
                </div>
                @empty
                <p>EMPTY BLOG</p>
                @endforelse

            </div>
            <br>
            <a class="tp_btn" href="{{url('/blog')}}" style="color:#000000;">&nbsp;&nbsp;Older Posts&nbsp;&nbsp;</a>
        </div>
    </div>
</section>
<!--================End Latest News Area =================-->
@endsection