@component('mail::message')
# Hallo, {{$name}}

Authorizer telah melakukan REJECT terhadap request pembuatan kategori {{$kategori}} / {{$subkategori}}

Terimakasih,
 
{{ config('app.name') }}
@endcomponent
