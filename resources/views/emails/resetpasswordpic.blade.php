@component('mail::message')
#Halo, {{$name}} 

Email dan nama anda telah didaftarkan oleh Corporate Compliance Department,
Sebagai PIC untuk menindaklanjuti pelaporan pelanggaran yang dilaporkan melalui ACC Whistle.
Silakan ganti password user anda dengan klik tombol di bawah ini: 
@component('mail::button', ['url' => $url])
    Ubah Password
@endcomponent

Terima Kasih,

{{ config('app.name') }}

@component('mail::subcopy', ['url' => $url])
    If you’re having trouble clicking the "Reset " button, copy and paste the URL below
    into your web browser: [{{ $url }}]({{ $url}})
@endcomponent
@endcomponent
