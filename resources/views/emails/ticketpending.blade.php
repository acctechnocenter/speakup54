@component('mail::message')
# Hallo, {{$name}}

ACC Whistle  telah menerima tiket pelaporan baru.

Silahkan memberikan memberikan approval  kepada tiket #{{$number}} untuk diproses lebih lanjut.

Untuk melihat detail tiket silahkan click tombol di bawah ini :
 
@component('mail::button', ['url' => $url ])
Let's go!
@endcomponent
 
Terimakasih,
 
{{ config('app.name') }}
@endcomponent


