@component('mail::message')
# Hallo, {{$name}}
@if(!empty($type) AND $type == 'guest')
Terima kasih sudah melaporkan di ACC Whistle.

Tiket anda dengan id #{{$number}} sudah kami tindak lanjuti.
@else
Terima kasih sudah melaporkan di ACC Whistle.

Tiket anda dengan id #{{$number}} sudah kami tindak lanjuti.

Untuk melihat detail tiket silahkan click tombol di bawah ini :
 
@component('mail::button', ['url' => $url ])
Let's go!
@endcomponent
@endif

Terimakasih,
 
{{ config('app.name') }}
@endcomponent


