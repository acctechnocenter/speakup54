
@component('mail::message')
# Reset your password
 
Klik link berikut untuk reset password akun anda.
 
@component('mail::button', ['url' => url('api/reset/' . $user->email) ])
Reset!
@endcomponent
 
Thanks,
 
{{ config('app.name') }}
@endcomponent