@component('mail::message')
# Hallo, {{$name}}

Terima kasih atas pelaporan anda melalui ACC Whistle. Tiket anda dengan id #{{$number}} telah kami terima dan segera akan kami tindak lanjut.
jika ingin mengetahui status pelaporan, silahkan membuat user dan login

dengan click tombol di bawah ini :
 
@component('mail::button', ['url' => $url ])
Let's go!
@endcomponent
 
Terimakasih,
 
{{ config('app.name') }}
@endcomponent


