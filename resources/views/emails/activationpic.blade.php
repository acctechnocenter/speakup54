@component('mail::message')
# Hallo, {{$name}}

Administrator telah melakukan perubahan / penambahan pada PIC baru.

Silahkan memberikan approval kepada PIC {{$picname}} untuk dapat melakukan tindak lanjut terhadap pelaporan.

Untuk melihat detail pic silahkan klik tombol di bawah ini :
 
@component('mail::button', ['url' => $url ])
Let's go!
@endcomponent
 
Terimakasih,
 
{{ config('app.name') }}
@endcomponent


