@component('mail::message')
# Hallo, {{$name}}
@if(!empty($type) AND $type == 'guest')
   Terima kasih atas pelaporan anda melalui ACC Whistle. Tiket anda dengan id #{{$number}} telah kami terima dan segera akan kami tindak lanjut.
@else
  Terima kasih atas pelaporan anda melalui ACC Whistle. Tiket anda dengan id #{{$number}} telah kami terima dan segera akan kami tindak lanjut.
  jika ingin mengetahui status pelaporan, silahkan login dengan click tombol di bawah ini ::
   
  @component('mail::button', ['url' => $url ])
  Let's go!
  @endcomponent
@endif
 
Terimakasih,
 
{{ config('app.name') }}
@endcomponent


