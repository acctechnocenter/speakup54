@component('mail::message')
#Hallo, {{$name}}
Anda mendapatkan ticket dengan id #{{$number}}

Silakan klik button ini untuk melihat detail ticket
 
@component('mail::button', ['url' => $url ])
Let's go!
@endcomponent
 
Terimakasih,
 
{{ config('app.name') }}
@endcomponent