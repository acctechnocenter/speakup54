@component('mail::message')
# Hallo, {{$name}}

Authorizer telah melakukan REJECT terhadap request pembuatan pic {{$picname}}

Terimakasih,
 
{{ config('app.name') }}
@endcomponent
