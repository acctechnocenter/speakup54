
@component('mail::message')
# Your Report Detail
 
Klik link berikut untuk melihat detail report anda.
 
@component('mail::button', ['url' => url('api/ticket/' . $ticket->id_ticket) ])
Check it on AccWhistle Apps!
@endcomponent
 
Thanks,
 
{{ config('app.name') }}
@endcomponent