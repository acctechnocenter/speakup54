@component('mail::message')
# Hallo, {{$name}}

Administrator telah melakukan perubahan / penambahan pada kategori pelaporan.

Silahkan memberikan approval kepada kategori {{$kategori}} / {{$subkategori}}, 
untuk diproses lebih lanjut.

Untuk melihat detail kategori silahkan click tombol di bawah ini :
 
@component('mail::button', ['url' => $url ])
Let's go!
@endcomponent
 
Terimakasih,
 
{{ config('app.name') }}
@endcomponent
