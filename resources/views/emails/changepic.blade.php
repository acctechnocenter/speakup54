@component('mail::message')
#Hello,{{$name}} 
 
Anda telah di assign oleh department compliance sebagai PIC ticket pelaporan dengan kategori {{$parent}}/{{$sub}}.

Silakan klik button untuk melihat list ticket yang anda tangani. 
@component('mail::button', ['url' => url('/users/list-complaint-pic')])
    List Ticket
@endcomponent

Terima Kasih,

{{ config('app.name') }}

@endcomponent