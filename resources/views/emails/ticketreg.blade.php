@component('mail::message')
# Hallo, {{$name}}

Terima kasih sudah melaporkan di ACC Whistle.

Tiket anda dengan id #{{$number}} akan segera kami tindak lanjuti.

Untuk melihat detail tiket silahkan click tombol di bawah ini :
 
@component('mail::button', ['url' => $url ])
Let's go!
@endcomponent
 
Terimakasih,
 
{{ config('app.name') }}
@endcomponent


