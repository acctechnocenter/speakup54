@component('mail::message')
# Hello {{ $name }}

Anda menerima email ini karena kami menerima permintaan pengaturan ulang kata sandi untuk akun Anda.
@component('mail::button', ['url' => $url])
    Ubah Password
@endcomponent

Terima Kasih,

{{ config('app.name') }}

@component('mail::subcopy', ['url' => $url])
    If you’re having trouble clicking the "Reset " button, copy and paste the URL below
    into your web browser: [{{ $url }}]({{ $url}})
@endcomponent
@endcomponent