@extends('layouts.app')

@section('content')
<section class="contact_us_area">
            <div class="container">
			<div id="mapBox" class="mapBox-login m0" style="padding:25px;">
			
				  <div class="contact_details_inner">
                    <div class="row">
						<div class="col-lg-2">
						 </div>
                        <div class="col-lg-8">
                    		<div class="contact_form">
								<div class="main_title text-center">
									<h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Isi email untuk merubah password </h3>
								</div>
                                @if (session('status'))
                                    <div class="alert alert-success text-center">
                                        {{ session('status') }}
                                    </div>
                                @endif

                                <form method="POST" action="{{ route('password.email') }}">
                                    {!! csrf_field() !!}

                                    <div class="form-group row">
                                        <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>

                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" value="submit" class="btn submit_btn2" style="width:300px">Kirim untuk mengganti password</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
						<div class="col-lg-3">
						</div>
                    </div>
                </div>
			</div>
        </div>
    </section>

@endsection
