@extends('layouts.app')

@section('content')
<section class="contact_us_area">
            <div class="container">
			<div id="mapBox" class="mapBox m0" style="padding:25px;">
			
				  <div class="contact_details_inner">
                    <div class="row">
						<div class="col-lg-2">
						 </div>
                        <div class="col-lg-8">
                    		<div class="contact_form">
								<div class="main_title text-center">
									<h3>Daftar akun baru ACC Whistle sekarang</h3>
								</div>
								@if (session('status'))
									<div class="alert alert-danger">
										{{ session('status') }}
									</div>
								@endif
								<form method="POST" action="{{ route('register') }}">
								    {!! csrf_field() !!}
									<div class="form-group row">
										<label for="fullname" class="col-md-3 col-form-label text-md-left">Nama Lengkap*</label>

										<div class="col-md-8">
											<input id="fullname" type="text" class="form-control{{ $errors->has('fullname') ? ' is-invalid' : '' }}" name="fullname" value="{{ old('fullname') }}" required autofocus>

											@if ($errors->has('fullname'))
												<span class="invalid-feedback">
													<strong>{{ $errors->first('fullname') }}</strong>
												</span>
											@endif
										</div>
									</div>

									<div class="form-group row">
										<label for="email" class="col-md-3 col-form-label text-md-left">Email*</label>

										<div class="col-md-8">
											<input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

											@if ($errors->has('email'))
												<span class="invalid-feedback">
													<strong>{{ $errors->first('email') }}</strong>
												</span>
											@endif
										</div>
									</div>

									<div class="form-group row">
										<label for="no_handphone" class="col-md-3 col-form-label text-md-left">No handphone*</label>

										<div class="col-md-8">
											<input maxlength="15" id="no_handphone" type="text" class="form-control{{ $errors->has('no_handphone') ? ' is-invalid' : '' }}" name="no_handphone" value="{{ old('no_handphone') }}" required autofocus>

											@if ($errors->has('no_handphone'))
												<span class="invalid-feedback">
													<strong>{{ $errors->first('no_handphone') }}</strong>
												</span>
											@endif
										</div>
									</div>

									<div class="form-group row">
										<label for="password" class="col-md-3 col-form-label text-md-left">Password*</label>

										<div class="col-md-8">
											<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
											<span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
											@if ($errors->has('password'))
												<span class="invalid-feedback">
													<strong>{{ $errors->first('password') }}</strong>
												</span>
											@endif
										</div>
									</div>

									<div class="form-group row">
										<label for="password" class="col-md-3 col-form-label text-md-left">Password Konfirmasi*</label>

										<div class="col-md-8">
											<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
											<span toggle="#password-confirm" class="fa fa-fw fa-eye field-icon toggle-password"></span>
											@if ($errors->has('password'))
												<span class="invalid-feedback">
													<strong>{{ $errors->first('password') }}</strong>
												</span>
											@endif
										</div>
									</div>
						
									<div class="col-lg-12 text-center">
										<button type="submit" value="submit" class="btn submit_btn2">Register</button>
										<a href="{{ url('/home') }}" class="btn submit_btn2">Cancel</a> 	
									</div>
								</form>
							</div>
                        </div>
							<div class="col-lg-3">
						 </div>
                    </div>
                </div>
			</div>
            </div>
        </section>
@endsection
@push('style')
        <style>
        .field-icon {
            float: right;
            margin-left: -25px;
            margin-top: -25px;
            position: relative;
            z-index: 2;
			margin-right:10px;
        }
        </style>
@endpush
@push('scripts')
<script>
$(".toggle-password").click(function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
    input.attr("type", "text");
    } else {
    input.attr("type", "password");
    }
});
</script>
@endpush