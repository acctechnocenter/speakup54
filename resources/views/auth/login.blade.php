@extends('layouts.app')

@section('content')
    <section class="contact_us_area">
        <div class="container">
        <div id="mapBox-login" class="mapBox-login m0" style="padding:25px;">
        
                <div class="contact_details_inner">
                <div class="row">
                    <div class="col-lg-2">
                        </div>
                    <div class="col-lg-8">
                        <div class="contact_form">
                            <div class="main_title text-center">
                                <h4>Silakan masuk ke dalam akun kamu</h4>
                            </div>

                            @if (session('status'))
                                <div class="alert alert-success text-center">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <form method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}

                                <div class="form-group row">
                                    <label for="email" class="col-sm-4 col-form-label text-md-right">Email</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                        @if ($errors->has('flag_active'))
                                            <span style="font-size:.875rem;color:#dc3545;margin-top:.25rem">
                                                <strong>{{ $errors->first('flag_active') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                        <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <!-- <button type="submit" class="btn btn-primary">
                                            Login
                                        </button> -->
                                        <button type="submit" value="submit" class="btn submit_btn2">Login</button>

                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            Lupa password ?
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                        <div class="col-lg-3">
                        </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection
@push('style')
        <style>
        .field-icon {
            float: right;
            margin-left: -25px;
            margin-top: -25px;
            position: relative;
            z-index: 2;
            margin-right:10px;
        }
        </style>
@endpush
@push('scripts')
<script>
$(".toggle-password").click(function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
    input.attr("type", "text");
    } else {
    input.attr("type", "password");
    }
});
</script>
@endpush