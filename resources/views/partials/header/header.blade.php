<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" href="img/fav-icon.png" type="image/x-icon" />
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>ACC - Whistle Blower</title>

        <!-- Icon css link -->
        <link href="{{ URL::asset('/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('/vendors/elegant-icon/style.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('/vendors/themify-icon/themify-icons.css') }}" rel="stylesheet" type="text/css">
        <!-- Bootstrap -->
        <link href="{{ URL::asset('/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">

        <!-- Rev slider css -->
        <link href="{{ URL::asset('/vendors/revolution/css/settings.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('/vendors/revolution/css/layers.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('/vendors/revolution/css/navigation.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('/vendors/animate-css/animate.css') }}" rel="stylesheet" type="text/css">

        <!-- Extra plugin css -->
        <link href="{{ URL::asset('/vendors/owl-carousel/owl.carousel.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('/vendors/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('/vendors/datatables/responsive.dataTables.min.css') }}" rel="stylesheet" type="text/css">

        <link href="{{ URL::asset('/css/style.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('/css/responsive.css') }}" rel="stylesheet" type="text/css">

        <link href="{{ URL::asset('/vendors/bootstrap-sweetalerts/dist/sweetalert.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('/vendors/css-hamburgers/dist/hamburgers.css') }}" rel="stylesheet" type="text/css">


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        @stack('style')
        <style>
            .pace {
  -webkit-pointer-events: none;
  pointer-events: none;

  -webkit-user-select: none;
  -moz-user-select: none;
  user-select: none;
}

.pace-inactive {
  display: none;
}

.pace .pace-progress {
  background: #29d;
  position: fixed;
  z-index: 2000;
  top: 0;
  right: 100%;
  width: 100%;
  height: 2px;
}

            #return-to-top {
                position: fixed;
                bottom: 45px;
                right: 20px;
                background: rgb(0, 0, 0);
                background: rgb(0, 114, 187);
                width: 50px;
                height: 50px;
                display: block;
                text-decoration: none;
                -webkit-border-radius: 35px;
                -moz-border-radius: 35px;
                border-radius: 35px;
                display: none;
                -webkit-transition: all 0.3s linear;
                -moz-transition: all 0.3s ease;
                -ms-transition: all 0.3s ease;
                -o-transition: all 0.3s ease;
                transition: all 0.3s ease;
                z-index: 1000;
            }
            #return-to-top i {
                color: #fff;
                margin: 0;
                position: relative;
                left: 16px;
                top: 13px;
                font-size: 19px;
                -webkit-transition: all 0.3s ease;
                -moz-transition: all 0.3s ease;
                -ms-transition: all 0.3s ease;
                -o-transition: all 0.3s ease;
                transition: all 0.3s ease;
            }
            #return-to-top:hover {
                background: rgb(0, 137, 209);
            }
            #return-to-top:hover i {
                color: #fff;
                top: 5px;
            }

        </style>
    </head>
    <body>
        <section class="search_area">
                <div class="search_inner">
                <form method="GET" action="{{url('/users/search')}}" role = "search">
                {!! csrf_field() !!}
                    <input type="text" id = "search" name = "search" placeholder="Masukkan nomor tiket Anda">
                    <i class="ti-close"></i>
                </form>
                    
                </div>
            </section>
            <!--================End Search Area =================-->

            <!--================Header Menu Area =================-->
            <header class="main_menu_area">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <a class="navbar-brand" href="{{url('/')}}"><img src="{{URL::asset('/img/logo.png')}}" alt=""></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon" style="background-color: #0072BB;"></span>
                        <span class="navbar-toggler-icon" style="background-color: #0072BB;"></span>
                        <span class="navbar-toggler-icon" style="background-color: #0072BB;"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        @guest
                          
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item active"><a class="nav-link" href="{{route('login')}}">Login</a></li>
                                <li class="nav-item active separator"><a class="nav-link" href="#">|</a></li>
                                <li class="nav-item active"><a class="nav-link" href="{{route('register')}}">Daftar</a></li>
                                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="icon_search_link"><i class="icon_search"></i>&nbsp;&nbsp;&nbsp;&nbsp;Lacak Tiket&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                            </ul>
                            <!-- <ul class="navbar-nav justify-content-end">
                                 <li><a href="#" class="icon_search_link"><i class="icon_search"></i>&nbsp;&nbsp;Search Tiket&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                            </ul> -->
                        @else
                          
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item active"><a class="nav-link" href="{{route('users')}}">Hi, {{ Auth::user()->fullname }}</a></li>
                                <li class="nav-item active separator"><a class="nav-link" href="#">|</a></li>
                                <li class="nav-item active"><a class="nav-link" href="{{route('logout')}}">Logout</a></li>
                                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="icon_search_link"><i class="icon_search"></i>&nbsp;&nbsp;Lacak Tiket&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                            </ul>
                            <!-- <ul class="navbar-nav justify-content-end">
                                <li><a href="#" class="icon_search_link"><i class="icon_search"></i>&nbsp;&nbsp;Search Tiket&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                            </ul> -->
                        @endif
                    </div>
                </nav>

                
            </header>
            <!--================End Header Menu Area =================-->

            <section class="main_slider_area">
                <div class="copy_right_area">
    
                </div>
            </section>

