        <section class="main_slider_area">
			<div class="copy_right_area">
  
            </div>
		</section>

                <!--================Footer Area =================-->
        <footer class="footer_area">
            <div class="footer_widgets_area">
                <div class="container">
					<div class="row justify-content-center" >
						<div class="col-lg-2  text-center">
                        	 kami di media sosial
							<p class="text-center">
							  <a href="https://www.facebook.com/ACCKreditMobil" target="_blank"><img src="{{URL::asset('/img/icon/icon-fb.jpg')}}" alt=""></a>
							  <a href="https://www.twitter.com/ACCKreditMobil" target="_blank"><img src="{{URL::asset('/img/icon/icon-twitter.jpg')}}" alt=""></a>
							  <a href="https://www.instagram.com/acckreditmobil/" target="_blank"><img src="{{URL::asset('/img/icon/icon-instagram.jpg')}}" alt=""></a>
							  <a href="https://www.youtube.com/channel/UCGD0ql_0Zqc4NhrQutIKcFw" target="_blank"><img src="{{URL::asset('/img/icon/icon-youtube.jpg')}}" alt=""></a>
						  </p>
                        </div>
                        <div class="col-lg-3  text-center">
						    ACC terdaftar & diawasi oleh
						    <img src="{{URL::asset('/img/icon/img-ojk.jpg')}}" alt="" style="display:block; margin:auto;">
                        </div>
                        <div class="col-lg-2  text-center">
						    Hubungi kami
							<img src="{{URL::asset('/img/icon/img-contact_0305.jpg')}}" alt="" style="display:block; margin:auto;">
                        </div>
                        <div class="col-lg-2 ">
							<img src="{{URL::asset('/img/icon/logo-acc-footer.png')}}" alt="" style="display:block; margin:auto;">
                        </div>
                        <div class="col-lg-3 text-center">
							<p class="text-center">
                                Download Aplikasi Android    <br>
                                <a href="https://play.google.com/store/apps/details?id=io.ionic.accwhistle" target="_blank"><img src="{{URL::asset('/img/icon/apk_downloader_logo.png')}}" alt=""></a>
						  </p>
						</div>
                    </div>
                    
					
					<div class="row justify-content-center">
						<div class="col-lg-10 text-center">
								<p class="text-center">
							  <a href="https://www.facebook.com/ACCKreditMobil" target="_blank">Home</a> &nbsp;&nbsp; | &nbsp;&nbsp;
							  <a href="{{url('/hubungi_kami')}}">Hubungi Kami</a>  &nbsp;&nbsp; | &nbsp;&nbsp;
							  <a href="{{url('/faq')}}" >FAQ's</a>  &nbsp;&nbsp; | &nbsp;&nbsp;
							  <!--<a href="{{URL::asset('images/dummy.pdf')}}" target="_blank">Petunjuk Penggunaan</a>&nbsp;&nbsp; | --> &nbsp;&nbsp;
                              <a href="{{url('/alur_kerja')}}">Alur Kerja</a> 
						  </p>
						</div>
					</div>
					<div class="row justify-content-center">
						<div class="col-lg-5 col-md-6 text-center">
							<div class="text-center">© 2018 Astra Credit Companies</div>
						</div>
					</div>
                </div>
            </div>
      
        </footer>

        <!-- Return to Top -->
        <a href="javascript:" id="return-to-top"><i class="fa fa-arrow-up"></i></a>
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="{{ asset('js/popper.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <!-- Rev slider js -->
        <script src="{{ asset('vendors/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
        <script src="{{ asset('vendors/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
        <script src="{{ asset('vendors/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
        <script src="{{ asset('vendors/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
        <script src="{{ asset('vendors/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
        <script src="{{ asset('vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
        <script src="{{ asset('vendors/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
        <script src="{{ asset('vendors/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
        <!-- Extra plugin css -->
        <script src="{{ asset('vendors/counterup/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('vendors/counterup/jquery.counterup.min.js') }}"></script>
        <script src="{{ asset('vendors/counterup/apear.js') }}"></script>
        <script src="{{ asset('vendors/counterup/countto.js') }}"></script>
        <script src="{{ asset('vendors/owl-carousel/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('vendors/parallaxer/jquery.parallax-1.1.3.js') }}"></script>
        <script src="{{ asset('vendors/datatables/datatables.min.js') }}"></script>
        <script src="{{ asset('vendors/datatables/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('vendors/datatables/handlebars.js') }}"></script>
        <script src="{{ asset('vendors/datatables/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('vendors/pace/pace.min.js') }}"></script>
        <!--Tweets-->
        <!-- <script src="{{ asset('vendors/tweet/tweetie.min.js') }}"></script>
        <script src="{{ asset('vendors/tweet/script.js') }}"></script> -->

        <script src="{{ asset('js/theme.js') }}"></script>
        @stack('scripts')
        <script>
            // ===== Scroll to Top ==== 
            $(window).scroll(function() {
                if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
                    $('#return-to-top').fadeIn(200);    // Fade in the arrow
                } else {
                    $('#return-to-top').fadeOut(200);   // Else fade out the arrow
                }
            });
            $('#return-to-top').click(function() {      // When arrow is clicked
                $('body,html').animate({
                    scrollTop : 0                       // Scroll to top of body
                }, 500);
            });
            
            $('#anchor1').click(function(){
                $('html, body').animate({
                    scrollTop: 500
                }, 500);
                return false;
            });
            $('#anchor2').click(function(){
                $('html, body').animate({
                    scrollTop: 950
                }, 500);
                return false;
            });
            $('#anchor3').click(function(){
                $('html, body').animate({
                    scrollTop: 1400
                }, 500);
                return false;
            });


        </script>

        <!--Start of Tawk.to Script-->
        <!-- <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5b87a176afc2c34e96e80c4d/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
        </script> -->
        <!--End of Tawk.to Script-->
    </body>
</html>