<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;
use App\Model\ParentCategory;
use App\Model\SubCategory;
use Jenssegers\Agent\Agent;


Route::match(['get', 'post'], '/botman', 'BotManController@handle');
Route::get('/botman/tinker', 'BotManController@tinker');
Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/users/getSubCategoryDesc/{par}/{sub}', 'UsersController@getSubCategoryDesc');
Route::get('/users/getParentCategory/{id}', 'UsersController@getParentCategory');
Route::get('/users/getSubCategory/{id}', 'UsersController@getSubCategory');
Route::get('/users/getPicSubCategory/{id}', 'UsersController@getPicSubCategory');
Route::post('/users/create-complaint/save-guest', 'UsersController@complaintStoreGuest');


Route::group(['middleware' => 'auth'], function () {

    Route::get('/users', 'UsersController@index')->name('users');
    Route::get('/users/data', 'UsersController@getAuthData');
    Route::get('/users/create-complaint', 'UsersController@createComplaint');
    Route::post('/users/create-complaint/save', 'UsersController@complaintStore');
    Route::post('/users/followup-complaint/save', 'UsersController@complaintStoreFollowUp');
    Route::get('/users/search', 'UsersController@search');
    Route::get('/users/list-complaint', 'UsersController@listComplaint');
    Route::get('/users/list-complaint-approval', 'UsersController@listComplaintApproval');
    Route::get('/users/list-complaint-pic', 'UsersController@listComplaintPic');
    Route::get('/download/{filename}', 'UsersController@downloadFile');
    Route::get('/users/complaint/data', 'UsersController@complaintDataTable');
    Route::get('/users/complaint/data/approval', 'UsersController@complaintDataTableforApproval');
    Route::get('/users/complaint/data/pic', 'UsersController@complaintDataTableforPic');
    Route::get('/users/complaint/{id}/{type}', 'UsersController@detailComplaint');
    Route::get('/users/complaint/{id}/{type}/for/list-complaint-pic', 'UsersController@detailComplaint');
    Route::get('/users/complaint/{id}/{type}/for/list-complaint-approval', 'UsersController@detailComplaint');


    Route::group(['middleware' => 'admin'], function () {
        Route::get('/administrator', 'AdminController@index')->name('administrator');
        Route::get('/administrator/blog', 'AdminController@blogIndex');
        Route::get('/administrator/blog/create', 'AdminController@blogCreate');
        Route::post('/administrator/blog/save', 'AdminController@blogStore');
        Route::get('/administrator/blog/data', 'AdminController@blogDataTable');
        Route::get('/administrator/blog/{id}/{type}', 'AdminController@blogEditAndDelete');

        Route::get('/administrator/faqs', 'AdminController@faqsIndex');
        Route::get('/administrator/faqs/create', 'AdminController@faqsCreate');
        Route::post('/administrator/faqs/save', 'AdminController@faqsStore');
        Route::get('/administrator/faqs/data', 'AdminController@faqsDataTable');
        Route::get('/administrator/faqs/{id}/{type}', 'AdminController@faqsEditAndDelete');

        Route::get('/administrator/slider', 'AdminController@sliderIndex');
        Route::get('/administrator/slider/create', 'AdminController@sliderCreate');
        Route::post('/administrator/slider/save', 'AdminController@sliderStore');
        Route::get('/administrator/slider/data', 'AdminController@sliderDataTable');
        Route::get('/administrator/slider/{id}/{type}', 'AdminController@sliderEditAndDelete');

        Route::get('/administrator/quote', 'AdminController@quoteIndex');
        Route::get('/administrator/quote/create', 'AdminController@quoteCreate');
        Route::post('/administrator/quote/save', 'AdminController@quoteStore');
        Route::get('/administrator/quote/data', 'AdminController@quoteDataTable');
        Route::get('/administrator/quote/{id}/{type}', 'AdminController@quoteEditAndDelete');

        Route::get('/administrator/pic', 'AdminController@picIndex');
        Route::get('/administrator/pic/create', 'AdminController@picCreate');
        Route::post('/administrator/pic/save', 'AdminController@picStore');
        Route::get('/administrator/pic/data', 'AdminController@picDataTable');
        Route::get('/administrator/pic/{id}/{type}', 'AdminController@picEditAndDelete');

        Route::get('/administrator/category', 'AdminController@categoryIndex');
        Route::get('/administrator/category/create', 'AdminController@categoryCreate');
        Route::post('/administrator/category/saveparent', 'AdminController@parentCategoryStore');
        Route::post('/administrator/category/savesub', 'AdminController@subCategoryStore');
        Route::get('/administrator/category/data', 'AdminController@categoryDataTable');
        Route::get('/administrator/category/{id}/{type}', 'AdminController@categoryEditAndDelete');
        Route::get('/administrator/sub-category/{id}/{type}', 'AdminController@subCategoryEditAndDelete');

        Route::get('/administrator/user', 'AdminController@userIndex');
        // Route::get('/administrator/user/create', 'AdminController@userCreate');
        Route::post('/administrator/user/save', 'AdminController@userStore');
        Route::get('/administrator/user/data', 'AdminController@userDataTable');
        Route::get('/administrator/user/{id}/{type}', 'AdminController@userEditAndDelete');

        //use ->middleware('admin) to prevent request if Auth not admin.
        Route::get('/administrator/report', 'AdminController@reportIndex');
        Route::get('/administrator/report/data/{year?}', 'AdminController@reportData');
        Route::get('/administrator/report/datatable/{year?}/{month?}', 'AdminController@reportDataTable');
        Route::post('/administrator/report/export/', 'AdminController@reportExportExcel');
    });
});




Route::resource('/blog', 'BlogController');
Route::resource('/faq', 'FaqController');

Route::get('activation/{token}', function ($token) {
    $agent = new Agent();
    $decrypted = Crypt::decryptString($token);
    $user = App\Model\User::where('email', $decrypted)->first();

    if (!empty($user)) {
        $user->flag_active = "1";
        $user->save();
        if ($agent->isMobile()) {
            $url = 'accwhistle://accwhistle.com/active/1';
            return Redirect::away($url);
        } else {
            return redirect('/login')->with('status', 'User anda sudah aktif, silahkan login!');
        }
        // return redirect('/login')->with('status', 'User anda sudah aktif, silahkan login!');
    }
});

Route::get('/create-complaint', function (Request $request) {
    if (Auth::check()) {
            return redirect('/users/create-complaint');
        }
    $parentcategories = ParentCategory::all();
    $categories = SubCategory::all();
    return view('pages.create_ticket_guest', compact('parentcategories', 'categories'));
});

Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/hubungi_kami', function () {
    return view('pages.hubungi_kami');
});
Route::get('/alur_kerja', function () {
    return view('pages.alur_kerja');
});
Route::get('/tentang-acc-whistle', function (Request $request) {
    if (!empty($request->query('Element'))) {
        $Element = $request->query('Element');
        return view('pages.pelajari_lebih_lanjut')->with('Element', $Element);
    } else {
            return view('pages.pelajari_lebih_lanjut');
        }
});

