<?php

// use App\Model\User;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;

// require_once 'Mobile_Detect.php';
// require_once "libs/Mobile_Detect.php";



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api'], function () {
    //authentication
    Route::post('auth/logout', 'AuthController@logout');

    //ticket
    Route::post('ticket/myticket', 'TicketController@getMyTicketList'); //param id_user
    Route::post('ticket/read', 'TicketController@readTicket');

    //OneSignal
    Route::group(['prefix' => 'onesignal'], function () {
        Route::post('setUserID', 'OneSignalController@setUserIdentifier')->name('set_user_identifier');
    });
});
Route::post('ticket/getticket', 'TicketController@getTicket');
Route::get('/download/{filename}', 'UsersController@downloadFile');

// Route::get('ticket/myticket',function(){
//     return 'kimak';
// });

//TICKET SECTION
//ok
//Route::get('ticket/status/{idticket}', 'TicketController@getTicketStatus');
Route::post('ticket/status', 'TicketController@getTicketStatus');
Route::get('ticket/closed', 'TicketController@getClosedTicket');
Route::get('ticket/pending', 'TicketController@getPendingTicket');
Route::get('ticket/received', 'TicketController@getReceivedTicket');
Route::get('ticket/process', 'TicketController@getProcessTicket');
Route::get('ticket/analyze', 'TicketController@getAnalyzeTicket');

//LOGIN & REGISTER SECTION
Route::post('auth/login', 'AuthController@login');
Route::post('auth/register', 'AuthController@register');
Route::post('auth/reset', 'AuthController@reset');
Route::post('auth/reset/password', 'AuthController@resetpassword');
//CATEGORIES SECTION
Route::post('category/add', 'CategoriesController@addCategory');
Route::get('category/getall', 'CategoriesController@getAllCategory');
Route::post('category/delete', 'CategoriesController@deleteCategory');

//SUB CATEGORIES SECTION
Route::post('subcategory/add', 'CategoriesController@addSubCategory');
Route::get('subcategory/getall', 'SubCategoriesController@getAllSubCategory');
Route::post('subcategory/delete', 'CategoriesController@deleteCategory');

//FAQ SECTION
Route::post('faq/create', 'FaqController@createFAQ');
Route::get('faq/getall', 'FaqController@getAllFAQ');
Route::post('faq/delete', 'FaqController@deleteFAQ');

//FORGET PASSWORD NO LONGER USED USE "RESET PASSWORD BELOW"
// Route::post('forgot/password', 'Auth\ForgotPasswordController')->name('forgot.password');

//EMAIL ACTIVATION
Route::get('activation/{token}', function ($token) {
    $agent = new Agent();

    $decrypted = Crypt::decryptString($token);
    $user = App\Model\User::where('email', $decrypted)->first();

    if (!empty($user)) {
        $user->flag_active = "1";
        $user->save();
        if ($agent->isMobile()) {
            $url = 'accwhistle://accwhistle.com/active/1';
            return Redirect::away($url);
        } else {
            // $url = 'accwhistle://accwhistle.com/active/1';
            // return Redirect::away($url);
            // $url = 'https://accwhistle.acc.co.id/login';
            // return Redirect::away($url)->with('status', 'User anda sudah aktif, silahkan login!');
            // return redirect()->away('http://localhost/login')->with('status', 'User anda sudah aktif, silahkan login!');
            return redirect('/login')->with('status', 'User anda sudah aktif, silahkan login!');
        }
    }
});

// Route::get('activation/desktop/{token}', function ($token)
// {
//     $decrypted = Crypt::decryptString($token);
//     $user = App\Model\User::where('email', $decrypted)->first();
    
//     if (!empty($user)){
//         $user->flag_active = "1";
//         $user->save();
//         $url = 'localhost/login';
//         // return redirect('/login')->with('status', 'User anda sudah aktif, silahkan login!');
//         return Redirect::away($url);
//     }
// });

//RESET PASSWORD (NEW IMPLEMENTATION)
Route::get('reset/{token}', function ($token) {
    $agent = new Agent();
    $decrypted = Crypt::decryptString($token);
    $user = App\Model\User::where('email', $decrypted)->first();

    if (!empty($user)) {
        
        if ($agent->isMobile()) {
            $url = 'accwhistle://accwhistle.com/reset/' . $token;
        return Redirect::away($url);
        } else {
            // $url = 'accwhistle://accwhistle.com/active/1';
            // return Redirect::away($url);
            // $url = 'https://accwhistle.acc.co.id/login';
            // return Redirect::away($url)->with('status', 'User anda sudah aktif, silahkan login!');
            // return redirect()->away('http://localhost/login')->with('status', 'User anda sudah aktif, silahkan login!');
            return redirect('/password/reset/'.$token);
        }
    }
});

//TICKET DETAIL FROM EMAIL
Route::get('ticket/{token}', function ($token) {
    $agent = new Agent();
    $decrypted = Crypt::decryptString($token);
    $ticket = App\Model\Ticket::where('id_ticket', $decrypted)->first();

    if (!empty($ticket)) {
        
        if ($agent->isMobile()) {
            $url = 'accwhistle://accwhistle.com/ticket/' . $decrypted;
        return Redirect::away($url);
        } else {
            // $url = 'accwhistle://accwhistle.com/active/1';
            // return Redirect::away($url);
            // $url = 'https://accwhistle.acc.co.id/login';
            // return Redirect::away($url)->with('status', 'User anda sudah aktif, silahkan login!');
            // return redirect()->away('http://localhost/login')->with('status', 'User anda sudah aktif, silahkan login!');
            return redirect('/users/complaint/'.$ticket->id.'/detail');
        }
    }
});

//ADD TICKET
Route::post('ticket/create', 'TicketController@create');
Route::post('ticket/upload', 'TicketController@upload');

//BLOG SECTION
Route::get('blog', 'BlogController@collect');

Route::get('identify', 'TicketController@getTicketIdentifier');
