<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategory extends Model
{
    //
    use SoftDeletes;
    protected $table = 'speakup_sub_categories';

    protected $fillable = ['id_category', 'id_sub_category', 'desc_sub_category', 'sub_category_name', 'id_pic', 'sla', 'flag_active', 'created_by'];

    public function parentcategory()
    {
        return $this->hasOne('App\Model\ParentCategory');
    }
}
