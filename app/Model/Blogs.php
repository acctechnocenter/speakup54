<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Blogs extends Model
{
    //
    protected $table = 'speakup_blogs';

    protected $fillable = [
        'slug_blog', 'image_url', 'title', 'post_body', 'flag_publish',
    ];

    public function getCreatedAtBlogAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['created_at'])
        ->format(' M d, Y ');
    }

}
