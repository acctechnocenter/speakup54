<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faq extends Model
{
    //
    use SoftDeletes;
    protected $table = 'speakup_faqs';

    protected $fillable = [
        'faqs_question', 'answers_question',
    ];

}
