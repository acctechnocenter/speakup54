<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    //
    protected $table = 'speakup_tickets';
    protected $fillable = ['id_ticket','id_pic','email_for_guest', 'no_handphone_for_guest','id_user','id_category','id_sub_category','ticket_description','ticket_attachment','ticket_status','ticket_sla','response_pic','created_by'];
}
