<?php

namespace App\Model;

use App\Notifications\ResetPassword as ResetPasswordNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use HasApiTokens;

    protected $table = 'speakup_users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /* untuk bisa kasih value di group, bagian fillable dikasih field group */
    protected $fillable = [
        'fullname', 'email', 'no_handphone', 'password', 'flag_active', 'group',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function tickets(){
        return $this->hasMany('App\Model\Ticket','id_user');
    }

}
