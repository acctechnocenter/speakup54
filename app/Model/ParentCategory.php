<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ParentCategory extends Model
{
    //
    use SoftDeletes;
    protected $table = 'speakup_parent_categories';
    protected $fillable = ['category_name','desc_category','flag_active','created_by'];
}
