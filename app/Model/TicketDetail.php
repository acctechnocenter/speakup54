<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class TicketDetail extends Model
{
    //
    protected $table = 'speakup_tickets_details';
    protected $fillable = ['id','id_ticket','user','response'];
}
