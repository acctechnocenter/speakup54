<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pic extends Model
{
    
    use SoftDeletes;
    protected $table = 'speakup_pics';

    protected $fillable = [
        'name_pic', 'no_handphone','email_pic', 'department_pic', 'created_by', 'group', 'id_user', 'flag_active'
    ];
}
