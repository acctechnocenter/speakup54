<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    //
    protected $table = 'speakup_slider';

    protected $fillable = [
        'image_url', 'title', 'additional_text', 'button_text', 'button_url', 'flag_publish',
    ];

}
