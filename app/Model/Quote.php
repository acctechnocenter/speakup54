<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quote extends Model
{
    //
    use SoftDeletes;
    protected $table = 'speakup_quotes';

    protected $fillable = [
        'image_url', 'quote', 'person_name', 'job', 'flag_publish',
    ];
}
