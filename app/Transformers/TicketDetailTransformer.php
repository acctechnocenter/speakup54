<?php

namespace App\Transformers;

use App\Model\TicketDetail;

use League\Fractal\TransformerAbstract;

class TicketDetailTransformer extends TransformerAbstract
{
    public function transform(TicketDetail $ticket)
    {
        return [
      'ID Ticket' => $ticket->id_ticket,
      'User' => $ticket->user,
      'Status' => $ticket->response,
      'Registered' => $ticket->created_at->diffForHumans(),
      'Updated' => $ticket->updated_at->diffForHumans(),
    ];
    }
}
