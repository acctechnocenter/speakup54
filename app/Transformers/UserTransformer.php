<?php

namespace App\Transformers;

use App\Model\User;

use League\Fractal\TransformerAbstract;
use Carbon\Carbon;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        return [
        // 'id' => $user->id,
        'name' => $user->fullname,
        // 'Phone Number' => $user->no_handphone,
        'Email' => $user->email,
        // 'Registered' => $user->created_at->diffForHumans(),
        // 'Flag' => $user->flag_active,
      ];
    }
}
