<?php

namespace App\Transformers;

use App\Model\Faq;

use League\Fractal\TransformerAbstract;

class FAQTransformer extends TransformerAbstract
{
    public function transform(Faq $faq)
    {
        return [
      'ID FAQ' => $faq->id,
      'Question' => $faq->faqs_question,
      'Answer' => $faq->answers_question,
      'Registered' => $faq->created_at->diffForHumans(),
      'Updated' => $faq->updated_at->diffForHumans(),
    ];
    }
}
