<?php

namespace App\Transformers;

use App\Model\ParentCategory;

use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{
    public function transform(ParentCategory $category)
    {
        return [
      'ID_Category' => $category->id,
      'Category_Name' => $category->category_name,
      'Category_Description' => $category->desc_category,
      'Registered' => $category->created_at->diffForHumans(),
      'Updated' => $category->updated_at->diffForHumans(),
    ];
    }
}
