<?php

namespace App\Transformers;

use App\Model\SubCategory;

use League\Fractal\TransformerAbstract;

class SubCategoryTransformer extends TransformerAbstract
{
    public function transform(SubCategory $category)
    {
        return [
      'ID_Category' => $category->id_category,
      'ID_SubCategory' => $category->id_sub_category,
      'Category_Name' => $category->sub_category_name,
      'Category_Description' => $category->desc_sub_category,
      'Flag' => $category->flag_active,
      'Registered' => $category->created_at->diffForHumans(),
      'Updated' => $category->updated_at->diffForHumans(),
    ];
    }
}
