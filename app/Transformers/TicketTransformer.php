<?php

namespace App\Transformers;

use App\Model\ParentCategory;
use App\Model\SubCategory;
use App\Model\Ticket;
use League\Fractal\TransformerAbstract;

class TicketTransformer extends TransformerAbstract
{
    public function transform(Ticket $ticket)
    {
        $parentcat = ParentCategory::where('id', $ticket->id_category)->get()->first();

        $subcat = SubCategory::where([
            ['id_category', '=', $ticket->id_category],
            ['id_sub_category', '=', $ticket->id_sub_category],
        ])->get()->first();
        
        return [
            'ID' => $ticket->id_ticket,
            'Created by' => $ticket->created_by,
            'Ticket Status' => $ticket->ticket_status,
            'Category' => !empty($parentcat->category_name) ? $parentcat->category_name : '',
            'Sub Category' => !empty($parentcat->sub_category_name) ? $parentcat->sub_category_name : '',
            'Created at' => $ticket->created_at->diffForHumans(),
            'Updated at' => $ticket->updated_at->diffForHumans(),
        ];
    }
}
