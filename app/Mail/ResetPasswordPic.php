<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPasswordPic extends Mailable
{
    use Queueable, SerializesModels;

    protected $token;
    protected $user;
    protected $department;
    protected $name;
    protected $type;
    protected $parentCategory;
    protected $subCategory;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $pic, $parentCategory, $post, $typeEmail)
    {
        $this->user = $user;
        $this->token = app('auth.password.broker')->createToken($user);
        $this->type = !empty($typeEmail) ? $typeEmail : '';
        $this->department = !empty($pic->department_pic) ? $pic->department_pic : '';
        $this->name = !empty($user->fullname) ? $user->fullname : '';
        $this->parentCategory = !empty($parentCategory) ? $parentCategory : '';
        $this->subCategory = !empty($post->sub_category_name) ? $post->sub_category_name : '';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->type == 'changePasswordPic')
        {   
            return $this->subject('Department Compliance')->markdown('emails.resetpasswordpic', [
                'url' => url('password/reset/'.$this->token),
                'department' => $this->department,
                'name' => $this->name,
            ]);
        }
        if($this->type == 'assignCategoryPic'){
            return $this->subject('Assign PIC compliance')->markdown('emails.changepic', [
                'department' => $this->department,
                'name' => $this->name,
                'parent' => $this->parentCategory,
                'sub' => $this->subCategory,
            ]);
        } 
    }
}
