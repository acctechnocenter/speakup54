<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TicketforUser extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $post;
    protected $ticket;
    protected $type;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$post, $type)
    {
        //
        $this->user = $user->fullname;
        $this->ticket = $post->id;
        $this->post = $post->id_ticket;
        $this->type = !empty($type) ? $type : '';
        $this->laststatus = $post->ticket_status;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->type == 'changestatus')
        {
            return $this->subject('Tiket pelaporan anda')->markdown('emails.ticketforuser', [
                'url' => url('/users/complaint/'.$this->ticket.'/detail'),
                'name' => $this->user,
                'number' => $this->post,
                'status' => $this->laststatus,
                'type' => '',
            ]);
        }
        if($this->type == 'ticketreg')
        {
            return $this->subject('Tiket pelaporan anda')->markdown('emails.ticketreg', [
                'url' => url('/users/complaint/'.$this->ticket.'/detail'),
                'name' => $this->user,
                'number' => $this->post,
                'type' => '',
            ]);
        }
        if($this->type == 'ticketclos')
        {
            return $this->subject('Tiket pelaporan anda')->markdown('emails.ticketclose', [
                'url' => url('/users/complaint/'.$this->ticket.'/detail'),
                'name' => $this->user,
                'number' => $this->post,
                'type' => '',
            ]);
        }
        if($this->type == 'ticketpending')
        {
            return $this->subject('Tiket pelaporan anda')->markdown('emails.ticketpending', [
                'url' => url('/users/complaint/'.$this->ticket.'/detail'),
                'name' => $this->user,
                'number' => $this->post,
                'type' => '',
            ]);
        }
    }
}
