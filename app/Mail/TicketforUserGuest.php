<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TicketforUserGuest extends Mailable
{
    use Queueable, SerializesModels;

    protected $email;
    protected $post;
    protected $ticket;
    protected $type;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $post, $type)
    {
        //
        $this->user = $email;
        $this->ticket = $post->id;
        $this->post = $post->id_ticket;
        $this->type = !empty($type) ? $type : '';
        $this->laststatus = $post->ticket_status;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->type == 'changestatus')
        {
            return $this->subject('Tiket pelaporan anda')->markdown('emails.ticketforuser', [
                'url' => url('/register'),
                'name' => $this->user,
                'number' => $this->post,
                'status' => $this->laststatus,
                'type' => 'guest',
            ]);
        }
        if($this->type == 'ticketreg')
        {
            return $this->subject('Tiket pelaporan anda')->markdown('emails.ticketforguest', [
                'url' => url('/register'),
                'name' => $this->user,
                'number' => $this->post,
                'type' => 'guest',
            ]);
        }
        if($this->type == 'ticketclos')
        {
            return $this->subject('Tiket pelaporan anda')->markdown('emails.ticketclose', [
                'url' => url('/users/complaint/'.$this->ticket.'/detail'),
                'name' => $this->user,
                'number' => $this->post,
                'type' => 'guest',
            ]);
        }
    }
}
