<?php

namespace App\Mail;

use App\Model\Ticket;
use App\Model\Pic;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TicketforPic extends Mailable
{
    use Queueable, SerializesModels;

    public $ticket;
    public $user; 
    public $pic;
    protected $number;
    /**
     *
     * @return void
     */
    public function __construct(Ticket $ticket, Pic $pic)
    {
        $this->ticket = $ticket->id;
        $this->number = $ticket->id_ticket;
        $this->pic = $pic->name_pic;
        // $this->user = $user;
        // $this->pic = $pic;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->view('view.name'); 
        return $this->subject('Tiket For PIC')->markdown('emails.ticketforpic', [
            'url' => url('/users/complaint/'.$this->ticket.'/detail'),
            'name' => $this->pic,
            
            'number' => $this->number,
        ]);
    }
}
