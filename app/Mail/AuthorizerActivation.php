<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AuthorizerActivation extends Mailable
{
    use Queueable, SerializesModels;

    protected $pic;
    protected $name;
    protected $picname;
    protected $subkategoriid;
    protected $kategori;
    protected $subkategori;
    protected $type;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $pic, $category, $subcategory, $type)
    {
        //
        $this->pic = !empty($pic->id) ? $pic->id : '';
        $this->name = !empty($user->fullname) ? $user->fullname : '';
        $this->picname = !empty($pic->name_pic) ? $pic->name_pic : '';
        $this->subkategoriid = !empty($subcategory->id) ? $subcategory->id : '';
        $this->kategori = !empty($category->category_name) ? $category->category_name : '';
        $this->subkategori = !empty($subcategory->sub_category_name) ? $subcategory->sub_category_name : '';
        $this->type = !empty($type) ? $type : '';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->type == 'activepic')
        {
            return $this->markdown('emails.activationpic', [
                'url' => url('administrator/pic/'.$this->pic.'/edit'),
                'name' => $this->name,
                'picname' => $this->picname,
            ]);
        }
        if($this->type == 'picreject')
        {
            return $this->markdown('emails.picrejectforadmin', [
                'name' => $this->name,
                'picname' => $this->picname,
            ]);
        }
        if($this->type == 'activekategori')
        {
            return $this->markdown('emails.activationkategori', [
                'url' => url('/administrator/sub-category/'.$this->subkategoriid.'/edit'),
                'kategori' => $this->kategori,
                'subkategori' => $this->subkategori,
                'name' => $this->name,
            ]);
        }
        if($this->type == 'kategorireject')    
        {
            return $this->markdown('emails.kategorirejectforadmin', [
                'name' => $this->name,
                'subkategori' => $this->subkategori,
                'kategori' => $this->kategori,

            ]);
        }
        if($this->type == 'kategorinewpic')
        {
            return $this->markdown('emails.activationkategorinewpic', [
                'url' => url('/administrator/sub-category/'.$this->subkategoriid.'/edit'),
                'kategori' => $this->kategori,
                'subkategori' => $this->subkategori,
                'picname' => $this->picname,
                'name' => $this->name,
            ]);
        }
    }
}
