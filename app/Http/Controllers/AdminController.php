<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\BlogRequest;
use App\Http\Requests\FaqRequest;
use App\Http\Requests\SliderRequest;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\ParentCategoryRequest;
use App\Http\Requests\PicRequest;
use App\Http\Requests\QuoteRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule; 
use App\Model\Blogs;
use App\Model\Faq;
use App\Model\Slider;
use App\Model\Quote;
use App\Model\Pic;
use App\Model\User;
use App\Model\SubCategory;
use App\Model\ParentCategory;
use App\Model\Ticket;
use App\Model\TicketDetail;
use DB;
use DataTables;
use Mail;
use App\Mail\UserActivation;
use App\Mail\EmailVerification;
use App\Mail\ResetPasswordPic;
use App\Mail\AuthorizerActivation;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Auth\Passwords\TokenRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Excel;

class AdminController extends Controller
{
    use SendsPasswordResetEmails;

    public function index()
    {
        return view('pages.admin.admin');
    }

    public function blogIndex()
    {
        $view = 'list-blog';
        return view('pages.admin.admin', compact('view'));
    }

    public function blogCreate()
    {
        $view = 'create-blog';
        return view('pages.admin.admin', compact('view'));
    }

    public function blogDataTable()
    {
        $model = DB::table('speakup_blogs')
                 ->select(['id', 'title', 'flag_publish', 'created_at'])
                 ->whereNull('deleted_at')
                 ->orderBy('created_at', 'DESC');
        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return '<a href="'.url('/administrator/blog').'/'.$model->id.'/edit" class="btn btn-primary btn-sm" style="margin-right:1px"><i class="fa fa-pencil"></i> </a><a href="'.url('/administrator/blog').'/'.$model->id.'/delete" data-id="'.$model->id.'" class="btn btn-danger btn-sm delete"><i class="fa fa-trash-o"></i></a>';
            })
            ->rawColumns(['link', 'action'])
            ->toJson();
    }

    public function blogStore(BlogRequest $request)
    {
        if($request->hasfile('image_url'))
        {
            $name = !empty($request->file('image_url')->getClientOriginalName()) ? $request->file('image_url')->getClientOriginalName() : 'default-acc.jpg';
            $request->file('image_url')->move(public_path().'/images/', strtolower($name));  
            $data = strtolower($name);
        }

        if(!empty($request->id)){
            $blogs = Blogs::find($request->id);
            $blogs->slug_blog   = !empty($request->title) ? str_slug($request->title, "-") : $request->title;
            $blogs->image_url   = !empty($data) ? $data : $blogs->image_url;
            $blogs->title       = $request->title;
            $blogs->post_body   = $request->post_body;
            $blogs->flag_publish = $request->flag_publish;
            $blogs->save();
        }else{
            $post = Blogs::create([
                'image_url'   => !empty($data) ? $data : '',
                'slug_blog'   => !empty($request->title) ? str_slug($request->title, "-") : $request->title,
                'title'       => $request->title,
                'post_body'   => $request->post_body,
                'flag_publish' => $request->flag_publish
            ]);
        }
        return redirect('/administrator/blog');
    }

    public function blogEditAndDelete($id, $type)
    {
        if($type == 'edit'){
            $view = 'edit-blog';
            $blogs = Blogs::find($id);
            return view('pages.admin.admin', compact('blogs', 'view'));
        }
        if($type == 'delete'){
            $blogs = Blogs::find($id);
            $blogs->delete();
            return redirect('/administrator/blog')->with('status', 'Blogs deleted successfully');
        }
    }

    /*===================================END BLOG======================================*/

    /*===================================  FAQ'S  =====================================*/
    public function faqsIndex()
    {
        $view = 'list-faqs';
        return view('pages.admin.admin', compact('view'));
    }

    public function faqsCreate()
    {
        $view = 'create-faqs';
        return view('pages.admin.admin', compact('view'));
    }

    public function faqsDataTable()
    {
        $model = DB::table('speakup_faqs')
                 ->select(['id', 'faqs_question', 'answers_question', 'flag_publish'])
                 ->whereNull('deleted_at');
        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return '<a href="'.url('/administrator/faqs').'/'.$model->id.'/edit" class="btn btn-primary btn-sm" style="margin-right:1px"><i class="fa fa-pencil"></i> </a><a href="'.url('/administrator/faqs').'/'.$model->id.'/delete" data-id="'.$model->id.'" class="btn btn-danger btn-sm delete"><i class="fa fa-trash-o"></i></a>';
            })
            ->rawColumns(['link', 'action'])
            ->toJson();
    }

    public function faqsStore(FaqRequest $request)
    {
        if(!empty($request->id)){
            $faqs = Faq::find($request->id);
            $faqs->faqs_question      = $request->faqs_question;
            $faqs->answers_question   = $request->answers_question;
            $faqs->flag_publish       = $request->flag_publish;
            $faqs->save();
        }else{
            $post = Faq::create([
                'faqs_question'     => $request->faqs_question,
                'answers_question'  => $request->answers_question,
                'flag_publish'      => $request->flag_publish
            ]);
        }
        return redirect('/administrator/faqs');
    }

    public function faqsEditAndDelete($id, $type)
    {
        if($type == 'edit'){
            $view = 'edit-faqs';
            $faqs = Faq::find($id);
            return view('pages.admin.admin', compact('faqs', 'view'));
        }
        if($type == 'delete'){
            $blogs = Faq::find($id);
            $blogs->delete();
            return redirect('/administrator/faqs')->with('status', 'Faqs deleted successfully');
        }
    }
    /*================================== END FAQ'S ====================================*/

    /*================================== REPORT ====================================*/
    public function reportIndex()
    {
        $view = 'report';
        $years = Ticket::distinct('created_at')->groupBy(DB::raw('YEAR(created_at)'))->pluck('created_at');
        return view('pages.admin.admin', compact('view'))->with('years',$years);
    }
    public function reportData($year = null)
    {
        if(isset($year)){
            $report = DB::table('speakup_tickets')
                    ->join('speakup_parent_categories','speakup_tickets.id_category','=','speakup_parent_categories.id')
                    ->select(DB::raw('COUNT(speakup_parent_categories.category_name) AS total, MONTH(speakup_tickets.created_at) AS bulan'),'speakup_parent_categories.category_name','speakup_tickets.ticket_status')
                    ->whereYear('speakup_tickets.created_at',$year)
                    ->groupBy('bulan','speakup_parent_categories.category_name')
                    ->get()
                    ->toJson(JSON_PRETTY_PRINT);
        }else{
            $report = DB::table('speakup_tickets')
                    ->join('speakup_parent_categories','speakup_tickets.id_category','=','speakup_parent_categories.id')
                    ->select(DB::raw('COUNT(speakup_parent_categories.category_name) AS total, MONTH(speakup_tickets.created_at) AS bulan'),'speakup_parent_categories.category_name','speakup_tickets.ticket_status')
                    ->whereYear('speakup_tickets.created_at',date('Y'))
                    ->groupBy('bulan','speakup_parent_categories.category_name')
                    ->get()
                    ->toJson(JSON_PRETTY_PRINT);
        }
        return $report;
    }

    public function reportDataTable($year = null, $month = null)
    {
        $model = DB::table('speakup_tickets')
                   ->leftJoin('speakup_parent_categories', 'speakup_tickets.id_category','=','speakup_parent_categories.id')
                   ->leftJoin('speakup_sub_categories', function($join)
                   {
                        $join->on('speakup_tickets.id_category', '=', 'speakup_sub_categories.id_category');
                        $join->on('speakup_tickets.id_sub_category', '=', 'speakup_sub_categories.id_sub_category');
                   })
                   ->leftJoin('speakup_pics','speakup_tickets.id_pic','=','speakup_pics.id')
                   ->select(
                        'speakup_tickets.id',
                        'speakup_tickets.id_ticket',
                        'speakup_parent_categories.category_name',
                        'speakup_sub_categories.sub_category_name',
                        'speakup_pics.name_pic',
                        'speakup_tickets.ticket_sla',
                        'speakup_tickets.ticket_status',
                        'speakup_tickets.created_at',
                        'speakup_tickets.updated_at'
                    )
                   ->orderBy('speakup_tickets.id', 'id');

        if(isset($year)){
             $model->whereYear('speakup_tickets.created_at',$year);
        }
        else
        {
            $year = date('Y');
            $model->whereYear('speakup_tickets.created_at',$year);
        }
        if(isset($month)){
            $model->whereMonth('speakup_tickets.created_at','=',$month);
        }
        $model->orderBy('speakup_tickets.id','id');
        $model = $model->get();
        return DataTables::of($model)
                         ->addColumn('date_ticket', function ($model) {
                            return \Carbon\Carbon::parse($model->created_at)
                                ->format(' d M Y H:i');
                         })
                         ->addColumn('last_date_response',function($model){
                             $last_date_response = TicketDetail::where('id_ticket',$model->id_ticket)->orderBy('created_at','desc')->first();
                             if($last_date_response)
                             {
                                return  Carbon::parse($last_date_response['created_at'])->format(' d M Y H:i');
                             }
                             else
                             {
                                 return null;
                             }
                         })
                         ->addColumn('action', function ($model) {
                             if($model->ticket_status == 'REGS') {
                                return '<a href="'.url('/users/complaint').'/'.$model->id.'/detail" class="btn btn-primary btn-sm" style="margin-right:1px"><i class="fa fa-eye"></i> </a> <a href="javascript:void(0)" data-id="'.$model->id.'" class="btn btn-danger btn-sm delete" style="margin-right:1px"><i class="fa fa-trash-o"></i></a>';
                             }else{
                                return '<a href="'.url('/users/complaint').'/'.$model->id.'/detail" class="btn btn-primary btn-sm" style="margin-right:1px"><i class="fa fa-eye"></i> </a><a href="javascript:void(0)" data-id="'.$model->id.'" class="btn btn-danger btn-sm non-delete" style="margin-right:1px"><i class="fa fa-trash-o"></i></a>';
                             }
                         
                         })
                         ->addColumn('sla_terbaru',function ($model){
                            $sla_terbaru = $model->ticket_sla;
                            if($model->ticket_status == 'OPEN')
                            {
                                $updated = Carbon::parse($model->updated_at);
                                $current = Carbon::now();
                                $expires = $updated->addDays($model->ticket_sla);
                                $sla_terbaru = $current->diffInDays($expires);
                            }
                            return $sla_terbaru;
                         })
                         ->rawColumns(['link', 'action'])
                         ->toJson();
    }

    public function reportExportExcel(Request $request)
    {
        $year = $request->select_period;
        $model = DB::table('speakup_tickets')
        ->leftJoin('speakup_parent_categories', 'speakup_tickets.id_category','=','speakup_parent_categories.id')
        ->leftJoin('speakup_sub_categories', function($join)
            {
                $join->on('speakup_tickets.id_category', '=', 'speakup_sub_categories.id_category');
                $join->on('speakup_tickets.id_sub_category', '=', 'speakup_sub_categories.id_sub_category');
            })
        ->join('speakup_pics','speakup_tickets.id_pic','=','speakup_pics.id')
        ->leftjoin('speakup_users','speakup_tickets.id_user','=','speakup_users.id')
        ->leftJoin('speakup_tickets_details','speakup_tickets.id_ticket','=','speakup_tickets_details.id_ticket')
        ->select(
                'speakup_tickets.id',
                'speakup_tickets.id_ticket',
                'speakup_users.fullname',
                'speakup_parent_categories.category_name',
                'speakup_sub_categories.sub_category_name',
                'speakup_pics.name_pic',
                'speakup_tickets.email_for_guest',
                'speakup_tickets.ticket_sla',
                'speakup_tickets.ticket_description',
                'speakup_tickets.ticket_attachment',
                'speakup_tickets.ticket_status',
                'speakup_tickets_details.user',
                'speakup_tickets_details.response',
                'speakup_tickets.created_at',
                'speakup_tickets.updated_at'
            )
        ->whereYear('speakup_tickets.created_at',$year)
        ->orderBy('speakup_tickets.updated_at', 'desc');
        $model = $model->get();
        $data = $model->toArray();
        foreach($data as $d){
            if($d->response == null){
                $d->response = "";
            }
            else
            {
                $d->response = strip_tags($d->response);
            }
            $d->ticket_description = strip_tags($d->ticket_description);
            if($d->ticket_status == 'OPEN')
            {
                $updated = Carbon::parse($d->updated_at);
                $current = Carbon::now();
                $expires = $updated->addDays($d->ticket_sla);
                $sla_terbaru = $current->diffInDays($expires);
                $d->sla_terbaru = $sla_terbaru;
            }
            elseif($d->ticket_status == 'CLOS')
            {
                $openDate = TicketDetail::where('id_ticket', $d->id_ticket)->where('response', 'Change Status from REGS to OPEN')->latest()->first();
                $closeDate = TicketDetail::where('id_ticket', $d->id_ticket)->where('response', 'Change Status from OPEN to CLOS')->latest()->first();
                if($openDate!=null && $closeDate!=null)
                {
                    $openDate = $openDate->created_at;
                    $closeDate = $closeDate->created_at;
                    $expires = $openDate->addDays($d->ticket_sla);
                    $d->sla_terbaru = $closeDate->diffInDays($expires);
                }
                else
                {
                    $d->sla_terbaru = $d->ticket_sla;
                }
            }
            else
            {
                $d->sla_terbaru = $d->ticket_sla;
            }
        }
        $export = [];
        foreach($data as $d){
            $row = (object)[];
            $row->STATUS_TICKET = $d->ticket_status;
            $row->TICKET_ID = $d->id_ticket;
            $row->REQUEST_DATE = $d->created_at;
            if(empty($d->email_for_guest))
            {
                $row->CREATED_BY = $d->fullname;
            }
            else
            {
                $row->CREATED_BY = $d->email_for_guest;
            }
            $row->CATEGORY = $d->category_name;
            $row->SUB_CATEGORY = $d->sub_category_name;
            $row->PIC = $d->name_pic;
            $row->SLA = $d->ticket_sla;
            $row->LAST_SLA = $d->sla_terbaru;
            if(Auth::user()->group == 'authorizer'){
              $row->TICKET_DESCRIPTION = $d->ticket_description;
              $row->TICKET_ATTACHMENT = $d->ticket_attachment;
            }
            // $row->RESPONSES = [];
            $row->RESPONSES = "";
            if($d->response !== "")
            {
                foreach($data as $x){
                    if($x->id_ticket == $d->id_ticket)
                    {
                        $respone = $x->user . ':' . $x->response;
                        // array_push($row->RESPONSES,$respone);
                        $row->RESPONSES =  $row->RESPONSES . "   " . $respone;
                    }
                }
            }
            array_push($export,$row);
        }
        $export = array_unique($export, SORT_REGULAR);
        $data= json_decode( json_encode($export), true);
        return Excel::create('accwhistle_report_'.$year, function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download('xls');
    }
    /*================================== END REPORT ====================================*/

    /*===================================  SLIDER  =====================================*/
    public function sliderIndex()
    {
        $view = 'list-slider';
        return view('pages.admin.admin', compact('view'));
    }

    public function sliderCreate()
    {
        $view = 'create-slider';
        return view('pages.admin.admin', compact('view'));
    }

    public function sliderDataTable()
    {
        $model = DB::table('speakup_slider')
                ->select(['id', 'title', 'flag_publish']);
        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return '<a href="'.url('/administrator/slider').'/'.$model->id.'/edit" class="btn btn-primary btn-sm" style="margin-right:1px"><i class="fa fa-pencil"></i> </a><a href="'.url('/administrator/slider').'/'.$model->id.'/delete" data-id="'.$model->id.'" class="btn btn-danger btn-sm delete"><i class="fa fa-trash-o"></i></a>';
            })
            ->rawColumns(['link', 'action'])
            ->toJson();
    }

    public function sliderStore(SliderRequest $request)
    {   
        if($request->hasfile('image_url'))
        {
            $name = !empty($request->file('image_url')->getClientOriginalName()) ? $request->file('image_url')->getClientOriginalName() : 'default-acc.jpg';
            $request->file('image_url')->move(public_path().'/images/', strtolower($name));  
            $data = strtolower($name);
        }

        if(!empty($request->id)){
            $slider = Slider::find($request->id);
            $slider->image_url         =  !empty($data) ? $data : $slider->image_url;
            $slider->title             = $request->title;
            $slider->additional_text   = $request->additional_text;
            $slider->button_text       = $request->button_text;
            $slider->button_url        = $request->button_url;
            $slider->flag_publish      = $request->flag_publish;
            $slider->save();
        }else{
            // dd($request);
            // $this->validate($request,[
            //     'image_url.*' => 'required',
            // ]); 
            $post = Slider::create([
                'image_url'         => !empty($data) ? $data : NULL,
                'title'             => $request->title,
                'additional_text'   => $request->additional_text,
                'button_text'       => $request->button_text,
                'button_url'        => $request->button_url,
                'flag_publish'      => $request->flag_publish,
            ]);
        }
        return redirect('/administrator/slider');
    }

    public function sliderEditAndDelete($id, $type)
    {
        if($type == 'edit'){
            $view = 'edit-slider';
            $slider = Slider::find($id);
            return view('pages.admin.admin', compact('slider', 'view'));
        }
        if($type == 'delete'){
            $blogs = Slider::find($id);
            $blogs->delete();
            return redirect('/administrator/slider')->with('status', 'slider deleted successfully');
        }
    }

    /*================================== END SLIDER ====================================*/

    /*================================== CATEGORY ====================================*/
    public function categoryIndex()
    {
        $view = 'list-category';
        return view('pages.admin.admin', compact('view'));
    }

    public function categoryCreate()
    {
        $view = 'create-category';
        $categories = ParentCategory::all();
        $pics = Pic::where('flag_active', 'APPROVE')->get();
        return view('pages.admin.admin', compact('view'))->with('categories', $categories)->with('pics',$pics);
    }

    public function categoryDataTable(){
        $data = DB::table('speakup_parent_categories')
            ->select([
                'speakup_parent_categories.id',
                'speakup_parent_categories.category_name',
            ])
            ->whereNull('deleted_at')
            ->get();
        return Datatables::of($data)
            ->addColumn('sub_category', function($data) {
                return url('administrator/category/' . $data->id .'/details-data');
            })
            ->addColumn('action', function($data){
                return '<a href="'.url('/administrator/category').'/'.$data->id.'/edit" class="btn btn-primary" style="margin-right:1px"><i class="fa fa-pencil"></i></a><a href="'.url('/administrator/category').'/'.$data->id.'/delete" data-id="'.$data->id.'" class="btn btn-danger delete"><i class="fa fa-trash-o"></i></a>';
            })
            ->rawColumns(['link','action'])
            ->toJson();
    }

    public function parentCategoryStore(ParentCategoryRequest $request ){
        if(!empty($request->id)){
            $parentCategory = ParentCategory::find($request->id);
            $parentCategory->category_name  = $request->category_name;
            $parentCategory->desc_category  = $request->desc_category;
            $parentCategory->created_by     = Auth::id();
            $parentCategory->save();
            return redirect('/administrator/category')->with('work', 'Parent Category berhasil diubah!');
        }else{
            $post = ParentCategory::create([
                'category_name'     => $request->category_name,
                'desc_category'     => $request->desc_category,
                'created_by'        => Auth::id()
            ]);
        return redirect('/administrator/category')->with('work', 'Parent Category berhasil dibuat!');
        }
    }

    public function subCategoryStore(CategoryRequest $request ){
        if(!empty($request->id)){
            $category = SubCategory::find($request->id);
            $oldcategory = SubCategory::find($request->id);

            $statuscount = DB::table('speakup_tickets')
            ->select(DB::raw('count(*) as status_count'))
            ->where('id_category',$category->id_category)
            ->where('id_sub_category',$category->id_sub_category)
            ->where('ticket_status', '<>', 'CLOS')
            ->first();
            $getidticket = DB::table('speakup_tickets')
            ->select('id_ticket')->where('id_category',$category->id_category)
            ->where('id_sub_category',$category->id_sub_category)
            ->get();
            if($statuscount->status_count == 0)
            {
                $category->id_category = $request->id_category;
                $category->desc_sub_category = $request->desc_sub_category;
                $category->sub_category_name = $request->sub_category_name;
                $category->id_pic = $request->pic;
                $category->sla = $request->sla;
                if($request->has('flag_active'))
                {
                    if($request->flag_active == 'yes')
                    {
                        $category->flag_active = 'APPROVE';
                        $pic = Pic::where('id', $request->pic)->first();
                        $user = User::where('email', $pic->email_pic)->first();
                        $parentCategory = ParentCategory::where('id',$request->id_category)->first()->category_name;
                        Mail::to($user->email)->send(new ResetPasswordPic($user, $pic, $parentCategory, $category, 'assignCategoryPic'));
                        //log email
                        Mail::to('mutiaracaesagusta@gmail.com')->cc('christianti.angelin@ti.ukdw.ac.id')->send(new ResetPasswordPic($user, $pic, $parentCategory, $category, 'assignCategoryPic'));
                    }
                    if($request->flag_active == 'no')
                    {
                        $category->flag_active = 'REJECT';
                        $admin = User::where('id', $category->created_by)->first();
                        $parentCategory = ParentCategory::where('id',$request->id_category)->first();
                        Mail::to($admin->email)->send(new AuthorizerActivation($admin, array(), $parentCategory, $category, 'kategorireject'));
                        //log email
                        Mail::to('mutiaracaesagusta@gmail.com')->cc('christianti.angelin@ti.ukdw.ac.id')->send(new AuthorizerActivation($admin, array(), $parentCategory, $category, 'kategorireject'));
                    }
                }

                if($oldcategory->id_pic != $request->pic)
                {
                    // if(!empty($request->pic)){
                    //     $pic = Pic::where('id', $request->pic)->first();
                    //     if($pic){
                    //         //update tiket yang statusnya REGS
                    //         // $getTicketRegs = DB::table('speakup_tickets')
                    //         // ->select('id')->where('id_category',$category->id_category)
                    //         // ->where('id_sub_category',$category->id_sub_category)
                    //         // ->where('ticket_status', '=', 'REGS')
                    //         // ->get();
                    //         // if($getTicketRegs){
                    //         //     foreach ($getTicketRegs as $key => $value) {
                    //         //         $regs = Ticket::find($value->id);
                    //         //         $regs->id_pic = $request->pic;
                    //         //         $regs->update();
                    //         //     }
                    //         // }
                    //         //send email to PIC
                    //         $user = User::where('email', $pic->email_pic)->first();
                    //         $parentCategory = ParentCategory::where('id',$request->id_category)->first()->category_name;
                    //         Mail::to($user->email)->send(new ResetPasswordPic($user, $pic, $parentCategory, $category, 'assignCategoryPic'));
                    //     }
                    // }
                    $pic = Pic::where('id', $request->pic)->first();
                    $category->flag_active = 'PENDING';
                    $autho = User::where('group', 'authorizer')->first();
                    $parentCategory = ParentCategory::where('id',$request->id_category)->first();
                    Mail::to($autho->email)->send(new AuthorizerActivation($autho, $pic, $parentCategory,  $category, 'kategorinewpic'));
                    //log email
                    Mail::to('mutiaracaesagusta@gmail.com')->cc('christianti.angelin@ti.ukdw.ac.id')->send(new AuthorizerActivation($autho, $pic, $parentCategory,  $category, 'kategorinewpic'));
                }
                
                $category->save();
                return redirect('/administrator/category')->with('work', 'Detail Kategori '.$category->sub_category_name.' berhasil dirubah')->with(compact('oldcategory', 'category'));
            }else{
                return redirect('/administrator/category')->with('status', 'Tiket di bawah ini masih dalam proses. silahkan rubah status CLOSED !')->with(compact('getidticket'));
            }
        }else{
            $saveSubCategory = SubCategory::create([
                'id_category' => $request->id_category,
                'id_sub_category' => SubCategory::withTrashed()->where('id_category',$request->id_category)->count() + 1,
                'desc_category' => ParentCategory::where('id',$request->id_category)->first()->desc_category,
                'desc_sub_category' => $request->desc_sub_category,
                'sub_category_name' => $request->sub_category_name,
                'id_pic' => $request->pic,
                'sla' => $request->sla,
                'flag_active' => 'PENDING',
                'created_by' => Auth::id()
            ]);
           
            if($saveSubCategory AND !empty($saveSubCategory->id_pic)){
                $pic = Pic::where('id', $saveSubCategory->id_pic)->first();
                if($pic){
                    // $user = User::where('email', $pic->email_pic)->first();
                    $autho = User::where('group', 'authorizer')->first();
                    $parentCategory = ParentCategory::where('id',$request->id_category)->first();
                    Mail::to($autho->email)->send(new AuthorizerActivation($autho, $pic, $parentCategory,  $saveSubCategory, 'activekategori'));
                    //log email
                    Mail::to('mutiaracaesagusta@gmail.com')->cc('christianti.angelin@ti.ukdw.ac.id')->send(new AuthorizerActivation($autho, $pic, $parentCategory,  $saveSubCategory, 'activekategori'));
                    // Mail::to($user->email)->send(new ResetPasswordPic($user, $pic, $parentCategory, $saveSubCategory, 'assignCategoryPic'));
                }
            }
            return redirect('/administrator/category')->with('work', 'Kategori berhasil dibuat');
        }
    }

    public function categoryEditAndDelete($id, $type)
    {
        if($type == 'edit'){
            $view = 'edit-parent-category';
            $category = ParentCategory::find($id);
            return view('pages.admin.admin', compact('category', 'view'));
        }
        if($type == 'delete'){
            $parentcategory = ParentCategory::find($id);
            
            $statuscount = DB::table('speakup_tickets')
            ->select(DB::raw('count(*) as status_count'))
            ->where('id_category',$parentcategory->id)
            ->where('ticket_status', '<>', 'CLOS')
            ->first();
            $getidticket = DB::table('speakup_tickets')
            ->select('id_ticket')->where('id_category',$parentcategory->id)
            ->get();
            if($statuscount->status_count == 0)
            {
                $category = SubCategory::all();
                foreach($category as $ct)
                {
                    if($parentcategory->id == $ct->id_category)
                    {
                        $ct->flag_active = 'no';
                        $ct->save();
                        $ct->delete();
                    }
                }
                $parentcategory->delete();
                
                return redirect('/administrator/category')->with('work', 'Kategori berhasil dihapus');
            }
            else
            {
                return redirect('/administrator/category')->with('status', 'Masih ada beberapa Tiket yang belum CLOS!')->with(compact('getidticket'));
            }
        }

        if($type == 'details-data'){
            $data = DB::table('speakup_sub_categories')
                ->select([
                    'speakup_sub_categories.id',
                    'speakup_sub_categories.sub_category_name',
                    'speakup_sub_categories.flag_active',
                ])->where('id_category', $id)
                ->whereNull('deleted_at')
                ->get();

            return Datatables::of($data)
                ->addColumn('action', function($data){
                    return '<a href="'.url('/administrator/sub-category').'/'.$data->id.'/edit" class="btn btn-primary" style="margin-right:1px"><i class="fa fa-pencil"></i></a><a href="'.url('/administrator/sub-category').'/'.$data->id.'/delete" data-id="'.$data->id.'" class="btn btn-danger delete-sub"><i class="fa fa-trash-o"></i></a>';
                })
                ->addColumn('status', function ($data){
                    $status = $data->flag_active;
                    return $status;
                })
                ->setRowClass(function($data){
                    $status = $data->flag_active;
                    if($status == 'PENDING' OR $status == 'REJECT')
                    {
                        return 'table-warning';
                    }
                })
                ->rawColumns(['link','action'])
                ->toJson();
        }
    }

    public function subCategoryEditAndDelete($id, $type)
    {
        if($type == 'edit'){
            $view = 'edit-sub-category';
            $subcategory = SubCategory::find($id);
            $parentcategory = ParentCategory::all();
            $pics = Pic::where('flag_active', 'APPROVE')->get();
            return view('pages.admin.admin', compact('subcategory','parentcategory', 'pics', 'view'));
        }
        if($type == 'delete'){
            $category = SubCategory::find($id);
            $statuscount = DB::table('speakup_tickets')
            ->select(DB::raw('count(*) as status_count'))
            ->where('id_category',$category->id_category)
            ->where('id_sub_category',$category->id_sub_category)
            ->where('ticket_status', '<>', 'CLOS')
            ->first();
            $getidticket = DB::table('speakup_tickets')
            ->select('id_ticket')->where('id_category',$category->id_category)
            ->where('id_sub_category',$category->id_sub_category)
            ->get();
            if($statuscount->status_count == 0)
            {
                $category->save();
                $category->delete();
                return redirect('/administrator/category')->with('work', 'Kategori berhasil dihapus');
            }
            else
            {
                return redirect('/administrator/category')->with('status', 'Masih ada beberapa Tiket yang belum CLOS!')->with(compact('getidticket'));
            }
        }
    }
    /*================================== END CATEGORY ====================================*/

    /*===================================  PIC  =====================================*/
    public function picIndex()
    {
        $view = 'list-pic';
        return view('pages.admin.admin', compact('view'));
    }

    public function picCreate()
    {
        $view = 'create-pic';
        return view('pages.admin.admin', compact('view'));
    }

    public function picDataTable()
    {
        $model = DB::table('speakup_pics')
            ->select(['id', 'name_pic', 'no_handphone', 'email_pic', 'department_pic', 'flag_active'])->whereNull('deleted_at');
        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return '<a href="'.url('/administrator/pic').'/'.$model->id.'/edit" class="btn btn-primary btn-sm" style="margin-right:1px"><i class="fa fa-pencil"></i> </a><a href="'.url('/administrator/pic').'/'.$model->id.'/delete" data-id="'.$model->id.'" class="btn btn-danger btn-sm delete" ><i class="fa fa-trash-o"></i></a>';
            })
            ->addColumn('status', function ($model){
                $status = $model->flag_active;
                return $status;
            })
            ->setRowClass(function($model){
                $status = $model->flag_active;
                if($status == 'PENDING' OR $status == 'REJECT')
                {
                    return 'table-warning';
                }
            })
            ->rawColumns(['link', 'action'])
            ->toJson();
    }

    public function picStore(PicRequest $request)
    {
        $pic = Pic::find($request->id);
        $emailPic = !empty($pic->email_pic) ? $pic->email_pic : '';
        // $cekPic = Pic::where('email_pic', $request->email_pic)->whereNotNull('deleted_at')->get();
        // $cekPic = DB::table('speakup_pics')
        // ->select(DB::raw('count(*) as pic_count'))
        // ->where('email_pic', $request->email_pic)
        // ->where('flag_active', 'yes')
        // ->first();
        // dd($cekPic);
        // ---------- EDIT PIC -----------
        if(!empty($request->id)){
            $statuscount = DB::table('speakup_tickets')
            ->select(DB::raw('count(*) as status_count'))
            ->where('id_pic', $pic->id)
            ->where('ticket_status', '<>' , 'CLOS')
            ->first();
            $getidticket = DB::table('speakup_tickets')
            ->select('id_ticket')->where('id_pic',$pic->id)
            ->get();
            // cek jika PIC mempunyai tiket..
            if($statuscount->status_count == 0)
            {
                //jika tidak sama
                if($pic->email_pic != $request->email_pic)
                {
                    //ambil data user pic yang di edit
                    $useredit = User::where('email', $emailPic)->first();
                    $useredit->group = 'member';
                    $useredit->save();
                    //cek email sudah ada tidak di table user ?
                    $user = User::where('email', $request->email_pic)->first();
                    if(empty($user)){
                        // jika tidak ada di table user insert ke table user dan pic
                        $pic->delete();
                        $this->saveToPic($request);
                        $newuser = User::where('email', $request->email_pic)->first();
                        Mail::to($newuser->email)->send(new ResetPasswordPic($newuser, $picSave, array(), array(), 'changePasswordPic'));
                        //log email
                        Mail::to('mutiaracaesagusta@gmail.com')->cc('christianti.angelin@ti.ukdw.ac.id')->send(new ResetPasswordPic($newuser, $picSave, array(), array(), 'changePasswordPic'));
                        return redirect('/administrator/pic')->with('work', 'Pic berhasil dibuat');
                    }else{
                        //jika sdh ada di table user update saja ke table pic
                        $cekPic = Pic::where('email_pic', $request->email_pic)->first();
                        //cek biar tidak duplicate di table pic
                        if(empty($cekPic)){
                            // $pic->name_pic          = $request->name_pic;
                            // $pic->no_handphone      = $request->no_handphone;
                            // $pic->email_pic         = $request->email_pic;
                            // $pic->id_user           = !empty($user->id) ? $user->id : '';
                            // $pic->department_pic    = $request->department_pic;
                            // if($pic->save()){
                            //     $user->group = 'pic';
                            //     $user->save();
                            //     //update group pic to member
                            //     $updateGroupUser = User::where('email',  $emailPic)->first();
                            //     $updateGroupUser->group = 'member';
                            //     $updateGroupUser->update();
                            // }
                            $pic->delete();
                            $this->saveToPic($request);
                            // $user->group = 'pic';
                            // $user->save();
                            return redirect('/administrator/pic')->with('work', 'Pic Berhasil diubah dan sudah di buatkan user');
                            $newuser = User::where('email', $request->email_pic)->first();
                            Mail::to($newuser->email)->send(new ResetPasswordPic($newuser, $picSave, array(), array(), 'changePasswordPic'));
                            Mail::to('mutiaracaesagusta@gmail.com')->cc('christianti.angelin@ti.ukdw.ac.id')->send(new ResetPasswordPic($newuser, $picSave, array(), array(), 'changePasswordPic'));
                        }else
                        {
                            return redirect('/administrator/pic')->with('status', 'Email Pic '.$request->email_pic.' sudah terdaftar sebagai PIC');
                        }
                    }
                }else{ 
                    //jika sama
                    // update saja ke table PIC & user => update full name & no hp di tabel user dari request PIC
                    $user = User::where('email',$request->email_pic)->first();
                    if($request->has('flag_active'))
                    {
                        if($request->flag_active == 'yes')
                        {
                            $pic->name_pic          = $request->name_pic;
                            $pic->no_handphone      = $request->no_handphone;
                            $pic->email_pic         = $request->email_pic;
                            $pic->id_user           = !empty($user->id) ? $user->id : '';
                            $pic->department_pic    = $request->department_pic;
                            $pic->flag_active       = 'APPROVE';
                            $pic->save();
                            
                            $user->fullname         = $request->name_pic;
                            $user->no_handphone     = $request->no_handphone;
                            $user->flag_active      = '1';
                            $user->group            = 'pic';
                            $user->save();

                            Mail::to($user->email)->send(new ResetPasswordPic($user, $pic, array(), array(), 'changePasswordPic'));
                            Mail::to('mutiaracaesagusta@gmail.com')->cc('christianti.angelin@ti.ukdw.ac.id')->send(new ResetPasswordPic($user, $pic, array(), array(), 'changePasswordPic'));
                        }
                        elseif($request->flag_active == 'no')
                        {
                            $pic->name_pic          = $request->name_pic;
                            $pic->no_handphone      = $request->no_handphone;
                            $pic->email_pic         = $request->email_pic;
                            $pic->id_user           = !empty($user->id) ? $user->id : '';
                            $pic->department_pic    = $request->department_pic;
                            $pic->flag_active       = 'REJECT';
                            $pic->save();

                            $admin = User::where('id', $pic->created_by)->first();
                            
                            Mail::to($admin->email)->send(new AuthorizerActivation($admin, $pic, array(), array(), 'picreject'));
                            Mail::to('mutiaracaesagusta@gmail.com')->cc('christianti.angelin@ti.ukdw.ac.id')->send(new AuthorizerActivation($admin, $pic, array(), array(), 'picreject'));
                        }
                    }
                    else
                    {
                        $user = User::where('email',$request->email_pic)->first();
                        $pic->name_pic          = $request->name_pic;
                        $pic->no_handphone      = $request->no_handphone;
                        $pic->email_pic         = $request->email_pic;
                        $pic->id_user           = !empty($user->id) ? $user->id : '';
                        $pic->department_pic    = $request->department_pic;
                        $pic->save();
                        
                        //update table use
                        
                        $user->fullname         = $request->name_pic;
                        $user->no_handphone     = $request->no_handphone;
                        $user->save();   
                    }             
                    return redirect('/administrator/pic')->with('work', 'Pic Berhasil diubah');
                }
          
            }else{
                return redirect('/administrator/pic')->with('status', 'Masih ada beberapa Tiket yang belum CLOS!')->with(compact('getidticket'));
            }
            
        }
        // ------ CREATE dari NOL -------
        else{
            $cek = Pic::where('email_pic', $request->email_pic)->first();
            if(empty($cek))
            {
                $this->saveToPic($request);
                $newpic = Pic::where('email_pic', $request->email_pic)->first();
                $newpic->flag_active = 'PENDING';
                $newpic->save();

                $user = User::where('email', $request->email_pic)->first();
                $user->flag_active = '0';
                $user->save();

                $autho = User::where('group', 'authorizer')->first();
                Mail::to($autho->email)->send(new AuthorizerActivation($autho, $newpic, array(), array(), 'activepic'));
                Mail::to('mutiaracaesagusta@gmail.com')->cc('christianti.angelin@ti.ukdw.ac.id')->send(new AuthorizerActivation($autho, $newpic, array(), array(), 'activepic'));
                return redirect('/administrator/pic')->with('work', 'Pic berhasil dibuat');
            }
            else
            {
                return redirect('/administrator/pic')->with('status', 'Email Pic '.$request->email_pic.' sudah terdaftar sebagai PIC');
            }
        }
    }

    public function saveToPic($request) {
        //cek semua email pic termasuk yang sudah dihapus
        $pic = Pic::where('email_pic', $request->email_pic)->withTrashed()->first();
        //cek subkategori dengan id pic yang diganti emailnya 
        $subkategori = SubCategory::where('id_pic', $request->id)->get();
        //saat di user tidak ada, otomatis di pic tidak ada. jadi create di pic dan di user
        if(empty($pic)){
            $pic = Pic::create([
                'name_pic'          => $request->name_pic,
                'no_handphone'      => $request->no_handphone,
                'email_pic'         => $request->email_pic,
                'department_pic'    => $request->department_pic,
                'created_by'        => Auth::id(),
                // 'flag_active'       => 'no',
            ]);
            if($pic){
                $this->saveToUser($pic, $request);
            }
        }else{
            //saat di user ada, otomatis di pic ada. jadi data pic dengan email yang di request, di update terbaru dan diaktifkan lagi
            $pic->name_pic = $request->name_pic;
            $pic->no_handphone = $request->no_handphone;
            $pic->email_pic = $request->email_pic;
            $pic->department_pic = $request->department_pic;
            $pic->created_by = Auth::id();
            $pic->deleted_at = NULL;
            $pic->update();
            $this->saveToUser($pic, $request);
        }

        if(!empty($subkategori))
        {
            foreach($subkategori as $sub)
            {
                $sub->id_pic = $pic->id;
                $sub->save();
            }
        }
    }

    public function saveToUser($picSave, $request){
        if($picSave){
            $user = User::where('email', $picSave->email_pic)->withTrashed()->first();
            if(empty($user)){
                $userSave = User::create([
                    'fullname'          => $request->name_pic,
                    'no_handphone'      => $request->no_handphone,
                    'email'             => $request->email_pic,
                    'password'          => Hash::make('Password1!'),
                    'group'             => 'pic',
                    'flag_active'       => '1',
                ]);

                if($userSave){
                    $picSave->id_user = $userSave->id;
                    // $picSave->flag_active = 'no';
                    $picSave->save();
                    // Mail::to($userSave->email)->send(new ResetPasswordPic($userSave, $picSave, array(), array(), 'changePasswordPic'));
                }
            }else{
                $user->fullname = $request->name_pic;
                $user->no_handphone = $request->no_handphone;
                $user->email = $request->email_pic;
                $user->password = Hash::make('Password1!');
                $user->group = 'pic';
                $user->flag_active = 1;
                $user->deleted_at = NULL;
                $user->update();
            }

            if(!empty($user) AND ($user->group == 'pic' OR $user->group == 'member')){
                $user->group = 'pic';
                $user->save();

                $picSave->id_user = $user->id;
                $picSave->save();
                Mail::to($user->email)->send(new ResetPasswordPic($user, $picSave, array(), array(), 'changePasswordPic'));
                Mail::to('mutiaracaesagusta@gmail.com')->cc('christianti.angelin@ti.ukdw.ac.id')->send(new ResetPasswordPic($user, $picSave, array(), array(), 'changePasswordPic'));
            }

            if(!empty($user) AND $user->group == 'administrator'){
                $picSave->id_user = $user->id;
                $picSave->save();
            }
        }
    }

    public function picEditAndDelete($id, $type, $userEmail=null)
    {
        if($type == 'edit'){
            $view = 'edit-pic';
            $pic = Pic::find($id);
            return view('pages.admin.admin', compact('pic', 'view'));
        }
        if($type == 'delete'){
            $pic = Pic::find($id);
            if(!empty($pic)){
                $statuscount = DB::table('speakup_tickets')
                ->select(DB::raw('count(*) as status_count'))
                ->where('id_pic', $pic->id)
                ->where('ticket_status', '<>' , 'CLOS')
                ->first();
                $getidticket = DB::table('speakup_tickets')
                ->select('id_ticket')->where('id_pic',$pic->id)
                ->get();
                if($statuscount->status_count == 0)
                {
                    $user = User::where('email', $pic->email_pic)->first();
                    if(!empty($user)){
                        //$user->group = 'member';
                        //$user->save();
                        $user->delete();
                    }
                    //$pic->flag_active = 'no';
                    //$pic->save();
                    $pic->delete();
                    
                    //$user->delete();
                    return redirect('/administrator/pic')->with('work', 'Pic berhasil dihapus');    
                } else {
                    return redirect('/administrator/pic')->with('status','Masih ada beberapa Tiket yang belum CLOS!')->with(compact('getidticket')); 
                }
            }else{
                $user = User::where('email', $userEmail->email)->first();
                if(!empty($user)){
                    $user->delete();
                }
                return redirect('/administrator/pic')->with('work', 'Pic berhasil dihapus');  
            }
        }
    }
    /*================================== END PIC ====================================*/

    /*===================================  USER  =====================================*/
    public function userIndex()
    {
        $view = 'list-user';
        return view('pages.admin.admin', compact('view'));
    }

    public function userDataTable()
    {
        $model = DB::table('speakup_users')
                 ->select(['id', 'fullname', 'no_handphone', 'email','group','flag_active'])
                 ->whereNull('deleted_at');
        return DataTables::of($model)
                 ->addColumn('action', function ($model) {
                     return '<a href="'.url('/administrator/user').'/'.$model->id.'/edit" class="btn btn-primary btn-sm" style="margin-right:1px"><i class="fa fa-pencil"></i> </a><a href="'.url('/administrator/user').'/'.$model->id.'/delete" data-id="'.$model->id.'" class="btn btn-danger btn-sm delete"><i class="fa fa-trash-o"></i></a>';
                 })
                 ->rawColumns(['link', 'action'])
                 ->toJson();
        ///administrator/user/'.$model->id.'/delete
    }
    //hanya untuk saat yang diedit awalnya pic
    public function savePicforUser($request)
    {
        $old = Pic::where('id_user', $request->id)->first();
        if(!empty($old))
        {
            $subkategori = SubCategory::where('id_pic', $old->id)->get();
        }
        $pic = Pic::where('email_pic', $request->email)->withTrashed()->first();
        //cek subkategori dengan id pic yang diganti emailnya 
        //saat di user tidak ada, otomatis di pic tidak ada. jadi create di pic dan di user
        if(empty($pic)){
            $pic = Pic::create([
                'name_pic'          => $request->fullname,
                'no_handphone'      => $request->no_handphone,
                'email_pic'         => $request->email,
                'id_user'           => $request->id,
                'created_by'        => Auth::id()
            ]);
        }else{
            //saat di user ada, otomatis di pic ada. jadi data pic dengan email yang di request, di update terbaru dan diaktifkan lagi
            $pic->name_pic = $request->fullname;
            $pic->no_handphone = $request->no_handphone;
            $pic->email_pic = $request->email;
            $pic->department_pic = $pic->department_pic;
            $pic->created_by = Auth::id();
            $pic->deleted_at = NULL;
            $pic->update();
        }

        if(!empty($subkategori))
        {
            foreach($subkategori as $sub)
            {
                $sub->id_pic = $pic->id;
                $sub->save();
            }
        }
    }

    public function userStore(Request $request){
        $this->validate($request,[
            'fullname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'no_handphone' => 'required'
        ]);
        $user = User::find($request->id);
        if(!empty($request->id))
        {
            //update heres
            //jika user dengan group pic di edit
            if($user->group == 'pic')
            {
                $pic = Pic::where('id_user',$user->id)->first();
                $statuscount = DB::table('speakup_tickets')
                ->select(DB::raw('count(*) as status_count'))
                ->where('id_pic', $pic->id)
                ->where('ticket_status', '<>' , 'CLOS')
                ->first();
                $getidticket = DB::table('speakup_tickets')
                ->select('id_ticket')->where('id_pic',$pic->id)
                ->get();
                //jika diedit menjadi selain member
                if($request->group != 'member')
                {
                    //cek status ticket
                    if($statuscount->status_count == 0)
                    {
                        //cek sama tidaknya email yang dimasukkan 
                        if($pic->email_pic != $request->email)
                        {
                            $cekPic = Pic::where('email_pic', $request->email_pic)->first();
                            //cek biar tidak duplicate di table pic aktif
                            if(empty($cekPic))
                            {
                                $pic->delete();
                                $this->savePicforUser($request);
                            }
                            else
                            {
                                return redirect('/administrator/user')->with('status', 'Email Pic '.$request->email.' sudah terdaftar sebagai PIC');
                            }
                        }
                        else
                        {
                            $pic->name_pic          = $request->fullname;
                            $pic->no_handphone      = $request->no_handphone;
                            $pic->save();
                        }
                        $user->fullname = $request->fullname;
                        $user->no_handphone = $request->no_handphone;
                        $user->email = $request->email;
                        $user->group = $request->group;
                        $user->flag_active = $request->flag_active;
                        $user->save();
                    }
                    else
                    {
                        return redirect('/administrator/user')->with('status', 'Masih ada beberapa Tiket yang belum CLOS!')->with(compact('getidticket'));
                    }
                }
                //jika diedit menjadi member
                else
                {
                    $pic->delete();
                    $user->fullname = $request->fullname;
                    $user->no_handphone = $request->no_handphone;
                    $user->email = $request->email;
                    $user->group = $request->group;
                    $user->flag_active = $request->flag_active;
                    $user->save();
                }
            }
            //edit selain pic
            else
            {
                if($request->group == 'pic')
                {
                    $pic = Pic::where('id_user',$user->id)->withTrashed()->first();
                    if(!empty($pic))
                    {
                        $this->savePicforUser($request);
                    }
                    else
                    {
                        $this->savePicforUser($request);
                    }
                }
                $user->fullname = $request->fullname;
                $user->no_handphone = $request->no_handphone;
                $user->email = $request->email;
                $user->group = $request->group;
                $user->flag_active = $request->flag_active;
                $user->save();
            }
        }
        else
        {
            //store heres
        }
        $view = 'list-user';
        return redirect('/administrator/user');
    }

    public function userEditAndDelete($id, $type){
        if($type == 'edit')
        {
            //update view here
            $view = 'edit-user';
            $user = User::find($id);
            return view('pages.admin.admin', compact('user', 'view'));
        }
        if($type == 'delete')
        {
            $user = User::find($id);
            if(!empty($user)){
                if($user->group == 'pic')
                {
                    $pic = Pic::where('id_user', $user->id)->first();
                    $picId = !empty($pic->id) ? $pic->id : 0;
                    return $this->picEditAndDelete($picId, 'delete', $user);
                    
                }
                else
                {
                    $user->flag_active = '0'; 
                    $user->save();
                    $user->delete();
                    return redirect('/administrator/user')->with('status', 'User deleted successfully');
                }
            }
 
           //here is a place to delete user
            
            
        }
    }

    /*================================== END USER  ====================================*/

    /*===================================  QUOTE  =====================================*/
    public function quoteIndex()
    {
        $view = 'list-quote';
        return view('pages.admin.admin', compact('view'));
    }

    public function quoteCreate()
    {
        $view = 'create-quote';
        return view('pages.admin.admin', compact('view'));
    }

    public function quoteDataTable()
    {
        $model = DB::table('speakup_quotes')
                ->select(['id', 'person_name', 'quote', 'flag_publish'])->whereNull('deleted_at');
        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return '<a href="'.url('/administrator/quote').'/'.$model->id.'/edit" class="btn btn-primary btn-sm" style="margin-right:1px"><i class="fa fa-pencil"></i> </a><a href="'.url('/administrator/quote').'/'.$model->id.'/delete" data-id="'.$model->id.'" class="btn btn-danger btn-sm delete"><i class="fa fa-trash-o"></i></a>';
            })
            ->rawColumns(['link', 'action'])
            ->toJson();
    }

    public function quoteStore(QuoteRequest $request)
    {
        if($request->hasfile('image_url'))
        {
            $name = !empty($request->file('image_url')->getClientOriginalName()) ? $request->file('image_url')->getClientOriginalName() : 'default-acc.jpg';
            $request->file('image_url')->move(public_path().'/images/', strtolower($name));  
            $data = strtolower($name);
        }
        if(!empty($request->id)){
            $quote = Quote::find($request->id);
            $quote->image_url         = !empty($data) ? $data : $quote->image_url;
            $quote->quote             = $request->quote;
            $quote->person_name       = $request->person_name;
            $quote->job               = $request->job;
            $quote->flag_publish      = $request->flag_publish;
            $quote->save(); 
        }else{
            $post = Quote::create([
                'image_url'         => !empty($data) ? $data : '',
                'quote'             => $request->quote,
                'person_name'       => $request->person_name,
                'job'               => $request->job,
                'flag_publish'      => $request->flag_publish,
            ]);
        }
        return redirect('/administrator/quote');
    }

    public function quoteEditAndDelete($id, $type)
    {
        if($type == 'edit'){
            $view = 'edit-quote';
            $quote = Quote::find($id);
            return view('pages.admin.admin', compact('quote', 'view'));
        }
        if($type == 'delete'){
            $quote = Quote::find($id);
            $quote->flag_publish = 'no';
            $quote->delete();
            return redirect('/administrator/quote')->with('status', 'Quote deleted successfully');
        }
    }

    /*================================== END QUOTE ====================================*/

}
