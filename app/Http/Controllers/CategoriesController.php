<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\ParentCategory;

use App\Transformers\CategoryTransformer;

class CategoriesController extends Controller
{
    public function addCategory(Request $request, ParentCategory $category)
    {
        $this->validate($request, [
        'category_name' => 'required|unique:speakup_parent_categories',
        'desc_category' => 'required',
      ]);

        $category = $category->create([
        'category_name' => $request->category_name,
        'desc_category' => $request->desc_category,
      ]);

        $response = $category()
          ->item($category)
          ->transformWith(new CategoryTransformer)
          ->toArray();

        return response()->json($response, 201);
    }

    public function getAllCategory()
    {
        //$data = ParentCategory::withTrashed()->get();
        // $data = ParentCategory::onlyTrashed()->get();
        $data = ParentCategory::all();

        return fractal()
          ->collection($data)
          ->transformWith(new CategoryTransformer)
          ->toArray();
    }

    public function deleteCategory(Request $request)
    {
        $data = ParentCategory::where('category_name', $request->category_name)->firstOrFail();

        $data->delete();

        return fractal()
        ->item($data)
        ->transformWith(new CategoryTransformer)
        ->addMeta([
          'Deleted at' => date('Y-m-d H:i:s'),
        ])
        ->toArray();
    }
}
