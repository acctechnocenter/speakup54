<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Faq;
use App\Transformers\FAQTransformer;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faq = Faq::where('flag_publish', 'yes')->get();
        return view('pages.faq.faq', compact('faq'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createFAQ(Request $request, Faq $faq)
    {
      $this->validate($request, [
      'faqs_question' => 'required|unique:speakup_faqs',
      'answers_question' => 'required',
    ]);

      $faq = $faq->create([
      'faqs_question' => $request->faqs_question,
      'answers_question' => $request->answers_question,
    ]);

      $response = fractal()
        ->item($faq)
        ->transformWith(new FAQTransformer)
        ->toArray();

      return response()->json($response, 201);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getAllFAQ()
    {
      //$data = Faq::withTrashed()->get();
      $data = Faq::all();

      return fractal()
        ->collection($data)
        ->transformWith(new FAQTransformer)
        ->toArray();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteFAQ(Request $request)
    {
      $data = Faq::where('category_name', $request->id)->firstOrFail();

      $data->delete();

      return fractal()
      ->item($data)
      ->transformWith(new FAQTransformer)
      ->addMeta([
        'Deleted at' => date('Y-m-d H:i:s'),
      ])
      ->toArray();
    }
}
