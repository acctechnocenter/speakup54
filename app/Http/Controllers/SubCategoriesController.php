<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\SubCategory;

use App\Transformers\SubCategoryTransformer;

class SubCategoriesController extends Controller
{
    public function addSubCategory(Request $request, SubCategory $category)
    {
        $this->validate($request, [
        'sub_category_name' => 'required|unique:speakup_sub_categories',
        'desc_sub_category' => 'required',
      ]);

        $category = $category->create([
        'sub_category_name' => $request->sub_category_name,
        'desc_sub_category' => $request->desc_sub_category,
      ]);

        $response = $category()
          ->item($category)
          ->transformWith(new SubCategoryTransformer)
          ->toArray();

        return response()->json($response, 201);
    }

    public function getAllSubCategory()
    {
        // $data = SubCategory::withTrashed()->get();
        // $data = SubCategory::onlyTrashed()->get();
        $data = SubCategory::all();

        return fractal()
          ->collection($data)
          ->transformWith(new SubCategoryTransformer)
          ->toArray();
    }

    public function deleteSubCategory(Request $request)
    {
        $data = SubCategory::where('sub_category_name', $request->sub_category_name)->firstOrFail();

        $data->delete();

        return fractal()
        ->item($data)
        ->transformWith(new SubCategoryTransformer)
        ->addMeta([
          'Deleted at' => date('Y-m-d H:i:s'),
        ])
        ->toArray();
    }
}
