<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Support\Facades\Crypt;

use App\Mail\DetailTicket;
use App\Model\ParentCategory;
use App\Model\Pic;
use App\Model\SubCategory;
use App\Model\Ticket;
use App\Model\TicketDetail;
use App\Model\User;
use App\Transformers\TicketDetailTransformer;
use App\Transformers\TicketTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use function GuzzleHttp\json_encode;

class TicketController extends Controller
{
    // public function getTicketStatus($idticket)
    // {
    //     $data = TicketDetail::where('id_ticket', $idticket)->firstOrFail();
    //
    //     return fractal()
    //     ->item($data)
    //     ->transformWith(new TicketDetailTransformer)
    //     ->toArray();
    // }
    public function getMyTicketList(Request $request)
    {
        //$user = User::where('id',$request->id_user)->first();
        $user = $request->user();

        if (!empty($user)) {
            $tickets = $user->tickets()->get();
            for ($i = 0; $i < $tickets->count(); $i++) {
                $data[$i]["id"] = $tickets[$i]["id_ticket"];
                $data[$i]["kategori"] = ParentCategory::where('id', $tickets[$i]["id_category"])->get()->first()->category_name;
                $data[$i]["sub_kategori"] = SubCategory::where('id_category', $tickets[$i]["id_category"])->where('id_sub_category', $tickets[$i]["id_sub_category"])->get()->first()->sub_category_name;
                $data[$i]["status"] = $tickets[$i]["ticket_status"];
                $data[$i]["sla"] = $tickets[$i]["ticket_sla"];
                $data[$i]["flag_read"] = $tickets[$i]["flag_read"];
                $data[$i]["created_at"] = $tickets[$i]["created_at"]->format('Y-m-d H:i:s');
                $data[$i]["updated_at"] = $tickets[$i]["updated_at"]->format('Y-m-d H:i:s');
            }
            return response()->json($data);
        } else {
            return response()->json([
                'message' => 'user not found',
                'status' => 'failed',
            ], 204);
        }
    }
    public function readTicket(Request $request)
    {
        $user = $request->user();
        $ticket = Ticket::where('id_ticket', $request->id_ticket)->where('id_user', $user->id)->first();
        if (!empty($ticket)) {
            $ticket->flag_read = "Y";
            $ticket->save();
            return response()->json([
                'message' => 'berhasil membaca',
            ], 200);
        } else {
            return response()->json([
                'message' => 'tidak berhasil membaca',
            ], 404);
        }
    }
    public function getTicket(Request $request)
    {
        $id_ticket = $request->id_ticket;
        $ticket = new Ticket;
        $ticket = $ticket->where('id_ticket', $id_ticket)->get()->first();
        if (!empty($ticket)) {
            $data = new \stdClass();
            $data->id_ticket = $ticket->id_ticket;
            // $pic = Pic::where('id', $ticket->id_pic)->get()->first();
            // $data->pic = !empty($pic->name_pic) ? $pic->name_pic : '';
            $data->kategori = ParentCategory::where('id', $ticket->id_category)->get()->first()->category_name;
            $data->sub_kategori = SubCategory::where('id_category', $ticket->id_category)->where('id_sub_category', $ticket->id_sub_category)->get()->first()->sub_category_name;
            $data->description = preg_replace("/&nbsp;/", '', strip_tags($ticket->ticket_description));
            $data->ticket_status = $ticket->ticket_status;
            $data->sla = $ticket->ticket_sla;
            $data->created_by = $ticket->created_by;
            $data->files = !empty($ticket->ticket_attachment) ? json_decode($ticket->ticket_attachment) : '';

            $data->created_at = Carbon::parse($ticket->created_at)->format('d M Y H:i');
            $res_array = array();
            $detail_ticket = TicketDetail::where('id_ticket', $ticket->id_ticket)->get();

            foreach ($detail_ticket as $detail) {
                $res = new \stdClass();
                $res->user = $detail['user'];
                $res->response = $detail['response'];

                $res->created_at = Carbon::parse($detail['created_at'])->format('d M Y, H:i');
                array_push($res_array, $res);
            }
            $data->ticket_response = $res_array;
            return response()->json($data, 200);
        } else {
            $data = new \stdClass();
            $data->pesan = "Tiket tidak ditemukan";
            return response()->json($data, 404);
        }
    }

    public function upload(Request $request)
    {
        header("Access-Control-Allow-Origin: *");
        header("Content-type:multipart/form-data");
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-  Disposition, Content-Description');
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");

        // $target_path = "images";

        // $target_path = $target_path . basename($_FILES['file']['name']);

        // if (isset($_POST['func'])) {
        //     if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
        //         echo "Upload and move success";
        //     } else {
        //         echo $target_path;
        //         echo "There was an error uploading the file, please try again!";}
        //     ;} else {echo "no func";}

        // $id_ticket = 'PLP' . date('Ymd') . str_pad(Ticket::whereDate('created_at', '=', Carbon::today()->toDateString())->count() + 1, 3, '0', STR_PAD_LEFT);
        $file = $request->file('ionicfile');
        $nameFile = $file->getClientOriginalName();
        if ($file) {
            // $extension = $file->getClientOriginalExtension();
            // $name = $nameFile . "." . $extension;
            $file->move(public_path() . '/images/', strtolower($nameFile));
            // $data[] = strtolower($name);
            // $counter++;

            return $nameFile;
        } else {
            return 'Failed to upload file';
        }

        // $fileName = null;

        // $file = $request->file('ionicfile');

        // if ($file != null) {
        //     //$ext = $file->getClientOriginalExtension();
        //     $fileName = $request->file('ionicfile')->getClientOriginalName();
        //     $file->move('images', $fileName);
        // } else {
        //     return 'Failed to upload file';
        // }

        // return $fileName;
    }

    public function getTicketIdentifier(){
        // $id = Ticket::whereDate('created_at','=',Carbon::today()->toDateString())->count();
        
        // return $id;
        $lastnumber = 001;
        $date = date('Ymd');
        $tick = Ticket::where('id_ticket', 'LIKE', '%' . $date . '%')->orderBy('id_ticket', 'DESC')->get()->first();
        if ($tick != null) {
            $lastnumber = substr($tick->id_ticket, 11, 14);
            $lastnumber = $lastnumber + 1;
        }

        $formattedNumber = sprintf('%03d', $lastnumber);

        return json_encode($formattedNumber);
        
    }

    public function create(Request $request, Ticket $ticket, TicketDetail $ticketDetail)
    {
        $lastnumber = 001;
        $data = null;

        $user = User::where('email', $request->email)->get()->first();

        if ($user != null) {
            $userid = $user->id;
        }

        $date = date('Ymd');

        $tick = Ticket::where('id_ticket', 'LIKE', '%' . $date . '%')->orderBy('id_ticket', 'DESC')->get()->first();
        $data_pic = SubCategory::select('id_pic')->where('id_category', $request->id_category)->where('id_sub_category', $request->id_sub_category)->get()->first();
        if ($tick != null) {
            $lastnumber = substr($tick->id_ticket, 11, 14);
            $lastnumber = $lastnumber + 1;
        }

        $formattedNumber = sprintf('%03d', $lastnumber);

        $this->validate($request, [
            'email' => 'required|email',
            'id_category' => 'required',
            'id_sub_category' => 'required',
            'ticket_description' => 'required',
        ]);

        $idticket = strtoupper("PLP") . date('Ymd') . $formattedNumber;
        
        $dataFileName = !empty($request->file_name) ? explode(",", $request->file_name) : '';
        
        if ($user != null) {
            $data = $ticket->create([
                'id_ticket' => $idticket,
                'id_user' => $userid,
                'id_pic' => $data_pic->id_pic,
                'id_category' => $request->id_category,
                'id_sub_category' => $request->id_sub_category,
                'ticket_description' => $request->ticket_description,
                'ticket_attachment' => !empty($dataFileName) ? json_encode($dataFileName) : '',
                'ticket_status' => "REGS",
                'created_by' => $request->email,
            ]);
        } else {
            $data = $ticket->create([
                'id_ticket' => $idticket,
                'email_for_guest' => $request->email,
                'no_handphone_for_guest' => $request->no_handphone,
                'id_pic' => $data_pic->id_pic,
                'id_category' => $request->id_category,
                'id_sub_category' => $request->id_sub_category,
                'ticket_description' => $request->ticket_description,
                'ticket_attachment' => !empty($dataFileName) ? json_encode($dataFileName) : '',
                'ticket_status' => "REGS",
                'created_by' => $request->email,
            ]);
        }

        $dataDetail = $ticketDetail->create([
            'id_ticket' => $idticket,
            'user' => !empty($user->fullname) ? $user->fullname : " ",
            'response' => "Pelaporan sudah diterima",
        ]);

        $response = fractal()
            ->item($data)
            ->transformWith(new TicketTransformer)
            // ->addMeta([
            //   'token' => $user->api_token,
            // ])
            ->toArray();

        $data->id_ticket = Crypt::encryptString($data->id_ticket);
        Mail::to($request->email)->send(new DetailTicket($data));

        return response()->json($response, 201);
    }

    public function getTicketStatus(Request $request)
    {
        $data = TicketDetail::where('id_ticket', $request->id_ticket)->firstOrFail();

        return fractal()
            ->item($data)
            ->transformWith(new TicketDetailTransformer)
            ->toArray();
    }

    public function getClosedTicket()
    {
        $newData = [];
        $data = TicketDetail::all();
        foreach ($data as $value) {
            if (ends_with($value->response, ['clos', 'CLOS', 'Clos'])) {
                $newData[] = $value;
            }
        }

        return fractal()
            ->collection($newData)
            ->transformWith(new TicketDetailTransformer)
            ->toArray();
    }

    public function getPendingTicket()
    {
        $newData = [];
        $data = TicketDetail::all();
        foreach ($data as $value) {
            if (ends_with($value->response, ['pend', 'PEND', 'Pend'])) {
                $newData[] = $value;
            }
        }

        return fractal()
            ->collection($newData)
            ->transformWith(new TicketDetailTransformer)
            ->toArray();
    }

    public function getReceivedTicket()
    {
        $newData = [];
        $data = TicketDetail::all();
        foreach ($data as $value) {
            if (ends_with($value->response, ['diterima', 'DITERIMA', 'Diterima'])) {
                $newData[] = $value;
            }
        }

        return fractal()
            ->collection($newData)
            ->transformWith(new TicketDetailTransformer)
            ->toArray();
    }

    public function getProcessTicket()
    {
        $newData = [];
        $data = TicketDetail::all();
        foreach ($data as $value) {
            if (ends_with($value->response, ['ditindaklanjuti', 'DITINDAKLANJUTI', 'Ditindaklanjuti'])) {
                $newData[] = $value;
            }
        }

        return fractal()
            ->collection($newData)
            ->transformWith(new TicketDetailTransformer)
            ->toArray();
    }

    public function getAnalyzeTicket()
    {
        $newData = [];
        $data = TicketDetail::all();
        foreach ($data as $value) {
            if (ends_with($value->response, ['dianalisa', 'DIANALISA', 'Dianalisa'])) {
                $newData[] = $value;
            }
        }

        return fractal()
            ->collection($newData)
            ->transformWith(new TicketDetailTransformer)
            ->toArray();
    }
}
