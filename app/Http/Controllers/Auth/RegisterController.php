<?php

namespace App\Http\Controllers\Auth;

use Mail;
use DB;
use App\Model\User;
use Illuminate\Http\Request;
use App\Mail\UserActivation;
use App\Mail\EmailVerification;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule; 
use Illuminate\Support\Facades\Crypt;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;

use Illuminate\Foundation\Auth\RegistersUsers;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        // $id = $this->route('post');
        return Validator::make($data, [
            'fullname' => 'required|string|min:6|max:255|regex:/^[\pL\s]+$/u',
            'email' => 'required|string|email|max:255|unique:speakup_users,email,NULL,id,deleted_at,NULL',
            // 'email'     => 'required|string|email|max:255',
            // 'email'         => [
            //             'required',
            //             'string',
            //             'email',
            //             'max:255',
            //             Rule::unique('speakup_users')->where(function($query){
            //                 $query->whereNull('deleted_at');
            //             })
            //         ],
            'no_handphone' => 'required|regex:/(08)[0-9]{9}/',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // $this->validate($data,[
        //     'email'         => [
        //         'required',
        //         'string',
        //         'email',
        //         'max:255',
        //         Rule::unique('speakup_users')->where(function($query){
        //             $query->whereNull('deleted_at');
        //         })
        //     ],
        // ]);
        // $user = User::where('email', $data[''])
		 return User::create([
            'fullname' => $data['fullname'],
            'email' => $data['email'],
            'no_handphone' => $data['no_handphone'],
            'password' => bcrypt($data['password']),
            'group' => 'member',
        ]);
        // return User::get();
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        // $user = User::where('email', $request->email)->get();
        // $user = DB::table('speakup_users')
        // ->select(DB::raw('count(*) as count'))
        // ->where('email', $request->email)
        // ->where('flag_active', 'yes')
        // ->first();
        // dd($user);
        // if(empty($user->email))
        // {
            
        // }
        // else
        // {
        //     return redirect()->back()->with('status', 'Email Sudah Ada');
        // }

        $this->validator($request->all())->validate();

            event(new Registered($user = $this->create($request->all())));
    
            //disabed auto login after registration
            //$this->guard()->login($user);
    
            return $this->registered($request, $user) ?: redirect($this->redirectPath());
        
    }


    protected function registered($request, $user)
    {
        $user->email = Crypt::encryptString($user->email);
        Mail::to($request->email)->send(new UserActivation($user));
        // dd(Mail::to($user)->send(new UserActivation($user)));
        return redirect('/login')->with('status', 'Please check your email, for activation email!');
    }
}
