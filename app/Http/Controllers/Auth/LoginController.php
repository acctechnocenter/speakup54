<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Validation\Rule; 
use App\Model\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/users';


    protected function validateLogin(\Illuminate\Http\Request $request)
    {
        $flag = User::where('email',$request->email)->first();
        $customMessages = [
            'email.exists' => 'Email anda tidak terdaftar.',
            'flag_active.in' => 'Email belum diaktivasi, segera cek email anda.'
        ];
        $flag ? $request['flag_active'] = $flag->flag_active:'';
        $this->validate($request, [
            $this->username() => 'required|email|exists:speakup_users,email',
            'password' => 'required|string',
            'flag_active' => 'in:1'
        ],$customMessages);
    }

    protected function sendFailedLoginResponse(\Illuminate\Http\Request $request)
    {
        $errors = ['password' => trans('auth.failed')];

        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }

    // protected $maxLoginAttempts = 3; // Amount of bad attempts user can make
    // protected $lockoutTime = 300; // Time for which user is going to be blocked in seconds
 

    protected function hasTooManyLoginAttempts(\Illuminate\Http\Request $request)
    {
        return $this->limiter()->tooManyAttempts(
            $this->throttleKey($request), 2, 1
        );
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(\Illuminate\Http\Request $request)
    {
        return ['email' => $request->email, 'password' => $request->password];
    }
}
