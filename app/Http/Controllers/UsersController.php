<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TicketRequest;
use App\Http\Requests\TicketRequestGuest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Model\ParentCategory;
use App\Model\SubCategory;
use App\Model\User;
use App\Model\Pic;
use App\Model\Ticket;
use App\Model\TicketDetail;
use App\Mail\TicketforPic;
use App\Mail\TicketforUser;
use App\Mail\TicketforUserGuest;
use DateTime;
use DB;
use DataTables;
use Mail;
use Carbon\Carbon;

class UsersController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if((Auth::user()->group == 'administrator')){
            return redirect('/users/list-complaint');
        }

        if((Auth::user()->group == 'pic')){
            return redirect('/users/list-complaint-pic');
        }

        if((Auth::user()->group == 'authorizer'))
        {
            return redirect('/users/list-complaint-approval');
        }

        return view('pages.list_ticket');
    }

    public function createComplaint() {
        $parentcategories = ParentCategory::all();
        $categories = SubCategory::all();
        $pic = Pic::all();
        if(Auth::check())
        {
            return view('pages.create_ticket', compact('parentcategories', 'categories', 'pic'));
        }
        else
        {
            return view('pages.create_ticket_guest', compact('parentcategories','categories','pic'));
        }
    }

    public function listComplaint() {
        if(Auth::user()->group == 'administrator' || Auth::user()->group == 'authorizer'){
            return redirect('/administrator/report');
        }
        return view('pages.list_ticket');
    }
    
    public function listComplaintApproval() {
        return view('pages.list_ticket_approval');
    }

    public function listComplaintPic() {
        return view('pages.list_ticket_pic');
    }

    public function complaintDataTable(){
        // untuk tampil data yang dibuat oleh user
        $model = DB::table('speakup_tickets')
        ->leftJoin('speakup_parent_categories', 'speakup_tickets.id_category','=','speakup_parent_categories.id')
        ->leftJoin('speakup_sub_categories', function($join)
        {
            $join->on('speakup_tickets.id_category', '=', 'speakup_sub_categories.id_category');
            $join->on('speakup_tickets.id_sub_category', '=', 'speakup_sub_categories.id_sub_category');
        })
        ->select(
            'speakup_tickets.id', 
            'speakup_tickets.id_ticket',
            'speakup_parent_categories.category_name',
            'speakup_sub_categories.sub_category_name',
            'speakup_tickets.ticket_sla',
            'speakup_tickets.ticket_status',
            'speakup_tickets.created_at',
            'speakup_tickets.updated_at'
            )
            ->orderBy('speakup_tickets.created_at', 'desc');

            if(Auth::user()->group == 'member' || Auth::user()->group == 'pic'){
                $model->where('speakup_tickets.id_user', Auth::user()->id);
            }
            $model = $model->get();
            
        return DataTables::of($model)
                ->addColumn('date_ticket', function ($model) {
                    return \Carbon\Carbon::parse($model->created_at)
                        ->format(' d M Y H:i');
                })
                ->addColumn('action', function ($model) {
                    return '<a href="'.url('/users/complaint').'/'.$model->id.'/detail" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-edit"></i> Detail </a>';
                })
                ->addColumn('status', function ($model){
                    $status = $model->ticket_status;
                    if($model->ticket_status == 'OPEN')
                    {
                        $status = 'APPROVE';
                    }
                    return $status;
                })
                ->addColumn('sla_terbaru',function ($model){
                    $sla_terbaru = $model->ticket_sla;
                    if($model->ticket_status == 'OPEN')
                    {
                        // $ticketDetail = TicketDetail::where('id_ticket', $model->id_ticket);
                        // if($ticketDetail->where('response','==','Change Status form REGS to OPEN')->first() != null)
                        // {
                        //     $ticketDetail = $ticketDetail->get();
                        //     foreach($ticketDetail as $td)
                        //     {
                        //         if($td->response->first() == 'Change Status form REGS to OPEN')
                        //         {
                        //             $openDate = $td->created_at;
                        //         }
                        //     }
                        //     $current = Carbon::now();
                        //     $expires = $openDate->addDays($model->ticket_sla);
                        //     $sla_terbaru = $current->diffInDays($expires);
                        // }
                        $updated = Carbon::parse($model->updated_at);
                        $current = Carbon::now();
                        $expires = $updated->addDays($model->ticket_sla);
                        $sla_terbaru = $current->diffInDays($expires,false);
                        // if($sla_terbaru <= 0)
                        // {
                        //     $sla_terbaru = 'Terlambat Follow Up';
                        // }
                    }
                    elseif($model->ticket_status == 'CLOS')
                    {
                        $openDate = TicketDetail::where('id_ticket', $model->id_ticket)->where('response', 'Change Status from REGS to OPEN')->latest()->first();
                        $closeDate = TicketDetail::where('id_ticket', $model->id_ticket)->where('response', 'Change Status from OPEN to CLOS')->latest()->first();
                        if($openDate!=null && $closeDate!=null)
                        {
                            $openDate = $openDate->created_at;
                            $closeDate = $closeDate->created_at;
                            $expires = $openDate->addDays($model->ticket_sla);
                            $sla_terbaru = $closeDate->diffInDays($expires,false);
                        }
                        else
                        {
                            $sla_terbaru = $model->ticket_sla;
                        }
                    }
                    return $sla_terbaru;
                })
                // mau untuk merubah warna, tapi gagal huhu :(
                // ->editColumn('sla_terbaru','{{$sla_terbaru}}')
                // ->setRowClass(function($model){
                //     $sla_terbaru = $model->ticket_sla;
                //     if($model->ticket_status == 'OPEN')
                //     {
                //         $updated = Carbon::parse($model->updated_at);
                //         $current = Carbon::now();
                //         $expires = $updated->addDays($model->ticket_sla);
                //         $sla_terbaru = $current->diffInDays($expires);
                //         return $sla_terbaru > 0 ? 'alert-success' : 'alert-warning';
                //     }
                    
                // }) 
                // ->setRowAttr([
                //     'color' => 'red',
                // ])
                ->setRowClass(function($model){
                    $updated = Carbon::parse($model->updated_at);
                    $current = Carbon::now();
                    $expires = $updated->addDays($model->ticket_sla);
                    $sla_terbaru = $current->diffInDays($expires,false);
                    if($sla_terbaru <= '0' && $model->ticket_status == 'OPEN')
                    {
                        // $sla_terbaru = 'Terlambat Follow Up';
                        return 'table-warning';
                    }
                })
                ->rawColumns(['link', 'action'])
                ->toJson();

                
    }

    public function complaintDataTableforPic(){
        //datatable untuk tampil data ticket dengan pic yang login
        $model = DB::table('speakup_tickets')
        ->leftJoin('speakup_parent_categories', 'speakup_tickets.id_category','=','speakup_parent_categories.id')
        ->leftJoin('speakup_sub_categories', function($join)
        {
            $join->on('speakup_tickets.id_category', '=', 'speakup_sub_categories.id_category');
            $join->on('speakup_tickets.id_sub_category', '=', 'speakup_sub_categories.id_sub_category');
        })
        ->leftJoin('speakup_pics','speakup_tickets.id_pic','=','speakup_pics.id')
        ->select(
            'speakup_tickets.id', 
            'speakup_tickets.id_ticket',
            'speakup_parent_categories.category_name',
            'speakup_sub_categories.sub_category_name',
            'speakup_tickets.ticket_sla',
            'speakup_tickets.ticket_status',
            'speakup_tickets.created_at',
            'speakup_tickets.updated_at'
            )
            ->where('speakup_tickets.ticket_status', '!=' , 'CLOS')
            ->orderBy('speakup_tickets.created_at', 'desc');
            if(Auth::user()->group == 'pic')
            {
                $pic = Pic::where('id_user', Auth::user()->id)->first(); 
                if(!empty($pic->id)){ 
                    $open = "OPEN"; 
                    $model->where('speakup_tickets.id_pic', $pic->id); 
                    $model->where('speakup_tickets.ticket_status', $open); 
                }
            }

        $model = $model->get();
           
        return DataTables::of($model)
                ->addColumn('date_ticket', function ($model) {
                    return \Carbon\Carbon::parse($model->created_at)
                        ->format(' d M Y H:i');
                })
                ->addColumn('action', function ($model) {
                    return '<a href="'.url('/users/complaint').'/'.$model->id.'/detail/for/list-complaint-pic" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-edit"></i> Detail </a>';
                })
                ->addColumn('status', function ($model){
                    $status = $model->ticket_status;
                    if($model->ticket_status == 'OPEN')
                    {
                        $status = 'APPROVE';
                    }
                    return $status;
                })
                ->addColumn('sla_terbaru',function ($model){
                    $sla_terbaru = $model->ticket_sla;
                    if($model->ticket_status == 'OPEN')
                    {
                        $updated = Carbon::parse($model->updated_at);
                        $current = Carbon::now();
                        $expires = $updated->addDays($model->ticket_sla);
                        $sla_terbaru = $current->diffInDays($expires,false);
                    }
                    return $sla_terbaru;
                })
                ->setRowClass(function($model){
                    $updated = Carbon::parse($model->updated_at);
                    $current = Carbon::now();
                    $expires = $updated->addDays($model->ticket_sla);
                    $sla_terbaru = $current->diffInDays($expires,false);
                    if($sla_terbaru <= '0' && $model->ticket_status == 'OPEN')
                    {
                        // $sla_terbaru = 'Terlambat Follow Up';
                        return 'table-warning';
                    }
                })
                ->rawColumns(['link', 'action'])
                ->toJson();
    }
    
    public function complaintDataTableforApproval(){
         //datatable untuk tampil data ticket dengan pic yang login
         $model = DB::table('speakup_tickets')
         ->leftJoin('speakup_parent_categories', 'speakup_tickets.id_category','=','speakup_parent_categories.id')
         ->leftJoin('speakup_sub_categories', function($join)
         {
             $join->on('speakup_tickets.id_category', '=', 'speakup_sub_categories.id_category');
             $join->on('speakup_tickets.id_sub_category', '=', 'speakup_sub_categories.id_sub_category');
         })
         ->leftJoin('speakup_pics','speakup_tickets.id_pic','=','speakup_pics.id')
         ->select(
             'speakup_tickets.id', 
             'speakup_tickets.id_ticket',
             'speakup_parent_categories.category_name',
             'speakup_sub_categories.sub_category_name',
             'speakup_tickets.ticket_sla',
             'speakup_tickets.ticket_status',
             'speakup_tickets.created_at',
             'speakup_tickets.updated_at'
             )
             ->where('speakup_tickets.ticket_status', 'PEND')
             ->orderBy('speakup_tickets.created_at', 'desc');            
 
            $model = $model->get();

         return DataTables::of($model)
                 ->addColumn('date_ticket', function ($model) {
                     return \Carbon\Carbon::parse($model->created_at)
                         ->format(' d M Y H:i');
                 })
                 ->addColumn('action', function ($model) {
                    return '<a href="'.url('/users/complaint').'/'.$model->id.'/detail/for/list-complaint-approval" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-edit"></i> Detail </a>';
                 })
                 ->addColumn('sla_terbaru',function ($model){
                     $sla_terbaru = $model->ticket_sla;
                     return $sla_terbaru;
                 })
                 ->rawColumns(['link', 'action'])
                 ->toJson();
    }
    

    public function getSubCategory($id = ''){
        if(!empty($id))
        {
            $items = [];
            $items = SubCategory::where('id_category', $id)
                    ->orderBy('sub_category_name')
                    ->where('flag_active', 'APPROVE')
                    ->pluck('sub_category_name', 'id_sub_category');
            if(!empty($items))
                return $items->toJson();
            else
                return json_encode($items);
        }
    }
    public function getParentCategory($id = ''){
        if(!empty($id))
        {
            $desc = ParentCategory::find($id)->desc_category;
            return json_encode($desc);
            // return $desc->toJson();
        }
    }
    public function getSubCategoryDesc($parent_id = '',$sub_id = ''){
        if(!empty($parent_id) && !empty($sub_id))
        {
            $desc = SubCategory::where('id_category',$parent_id)->where('id_sub_category',$sub_id)->first()->desc_sub_category;
            return json_encode($desc);
            // return $desc->toJson();
        }
    }

    public function getPicSubCategory($id = ''){
        if(!empty($id))
        {
            $items = [];
            $items = Pic::where('id', $id)
                    ->pluck('name_pic', 'id');
            if(!empty($items))
                return $items->toJson();
            else
                return json_encode($items);
        }
    }

    public function complaintStore(TicketRequest $request)
    {
        $id_ticket = 'PLP'. date('Ymd') . str_pad(Ticket::whereDate('created_at','=',Carbon::today()->toDateString())->count() + 1,3,'0',STR_PAD_LEFT);
        $counter = 0;
        if($request->hasfile('ticket_attachment'))
        {
            foreach($request->file('ticket_attachment') as $file){
                $size = filesize($file);
                if($size > 10485760)
                {
                    return redirect(url('/users/create-complaint?type=fail'));
                }
                $extension = $file->getClientOriginalExtension();
                $name = $id_ticket."_".$counter.".".$extension;
                $file->move(public_path().'/images/', strtolower($name));
                $data[] = strtolower($name);
                $counter++;
            }
        }
            $user = User::find(Auth::id());
            $getPic = SubCategory::where('id_category', $request->id_category)->where('id_sub_category',$request->id_sub_category)->first();
            $post = Ticket::create([
                'id_ticket' => $id_ticket,
                'id_user' => Auth::check() ? Auth::id() : 0,
                'id_pic' => !empty($getPic->id_pic) ? $getPic->id_pic : '',
                'id_category' => !empty($request->id_category) ? $request->id_category : '',
                'id_sub_category' => !empty($request->id_sub_category) ?  $request->id_sub_category : '',
                'ticket_description' => !empty($request->ticket_description) ?  $request->ticket_description : '',
                'ticket_status' => 'REGS',
                'ticket_attachment' => !empty($data) ? json_encode($data) : '',
                'ticket_sla' => !empty($getPic->sla) ? $getPic->sla : '',
                'created_by' => !empty(Auth::user()->email) ? Auth::user()->email : '',
            ]);
            Mail::to(Auth::user()->email)->send(new TicketforUser($user, $post, 'ticketreg'));
        if(Auth::check())
        {
            return redirect(url('/users/create-complaint?type=success'));
        } else {
            return redirect('/');
        }
    }

    public function complaintStoreGuest(TicketRequestGuest $request)
    {
        $id_ticket = 'PLP'. date('Ymd') . str_pad(Ticket::whereDate('created_at','=',Carbon::today()->toDateString())->count() + 1,3,'0',STR_PAD_LEFT);
        $counter = 0;
        if($request->hasFile('ticket_attachment'))
        {
            foreach($request->file('ticket_attachment') as $file)
            {
                $size = filesize($file);
                if($size > 10485760)
                {
                    return redirect(url('/create-complaint?type=fail'));
                }
                $extension = $file->getClientOriginalExtension();
                $name = $id_ticket."_".$counter.".".$extension;
                $file->move(public_path().'/images/', strtolower($name));
                $data[] = strtolower($name);
                $counter++;
            }
        }
            $email = !empty($request->email_guest) ? $request->email_guest : '';
            // check user from email
            if(!empty($email)){
              $user = User::where('email', $email)->first();
            }
            $getPic = SubCategory::where('id_category', $request->id_category)->where('id_sub_category',$request->id_sub_category)->first();
            $post = Ticket::create([
                'id_ticket' => $id_ticket,
                'email_for_guest' => $email,
                'no_handphone_for_guest' => !empty($request->no_handphone_for_guest) ? $request->no_handphone_for_guest : '',
                'id_user' => !empty($user) ? $user->id : 0,
                'id_pic' => !empty($getPic->id_pic) ? $getPic->id_pic : '',
                'id_category' => !empty($request->id_category) ? $request->id_category : '',
                'id_sub_category' => !empty($request->id_sub_category) ?  $request->id_sub_category : '',
                'ticket_description' => !empty($request->ticket_description) ?  $request->ticket_description : '',
                'ticket_status' => 'REGS',
                'ticket_attachment' => !empty($data) ? json_encode($data) : '',
                'ticket_sla' => !empty($getPic->sla) ? $getPic->sla : '',
                'created_by' => !empty(Auth::user()->email) ? Auth::user()->email : 'guest',
            ]);
            
            Mail::to($email)->send(new TicketforUserGuest($email, $post, 'ticketreg'));
        
            return redirect(url('/create-complaint?type=success'));
    }


    public function search()
    {
        $key = Input::get('search');

        $ticket = Ticket::where('id_ticket',$key)->first();
        if(!empty($ticket))
        {
            if(Auth::user()->group == 'administrator')
            {
                return redirect(url('/users/complaint').'/'.$ticket->id.'/detail')->with('work','Tiket anda telah ditemukan');
            }
            elseif(Auth::user()->group == 'pic')
            {
                $pic = Pic::find($ticket->id_pic);
                if(!empty($pic) AND $pic->id_user == Auth::user()->id AND $ticket->ticket_status == 'OPEN')
                {
                    return redirect(url('/users/complaint').'/'.$ticket->id.'/detail')->with('work','Tiket anda telah ditemukan');
                }
                else
                {
                    $picuser = Pic::where('id_user',$ticket->id_user)->first();
                    if(!empty($picuser) AND $picuser->id_user == Auth::user()->id)
                    {
                        return redirect(url('/users/complaint').'/'.$ticket->id.'/detail')->with('work','Tiket anda telah ditemukan');
                    }
                    else
                    {
                        return redirect(url('/users/list-complaint'))->with('status', 'Tiket tidak ditemukan');
                    }
                    
                }
            }
            elseif(Auth::user()->group == 'member')
            {
                if($ticket->id_user == Auth::user()->id)
                {
                    return redirect(url('/users/complaint').'/'.$ticket->id.'/detail')->with('work','Tiket anda telah ditemukan');
                }
                else
                {
                    return redirect(url('/users/list-complaint'))->with('status', 'Tiket tidak ditemukan');
                }
            }
        }
        else
        {
            return redirect(url('/users/list-complaint'))->with('status', 'Tiket tidak ditemukan');
        }
    }

    public function detailComplaint($id, $type) 
    {
        if($type == 'detail')
        {
            $ticket = Ticket::find($id);
            $parentcategories = ParentCategory::all();
            $categories = SubCategory::all();
            $idPic =  !empty($ticket->id_pic) ? $ticket->id_pic : '';
            $idUser =  !empty($ticket->id_user) ? $ticket->id_user : '';
            $tiketId =  !empty($ticket->id_ticket) ? $ticket->id_ticket : '';
            $id_category =  !empty($ticket->id_category) ? $ticket->id_category : '';
            $id_sub_category =  !empty($ticket->id_sub_category) ? $ticket->id_sub_category : '';
            $pic = Pic::find($idPic);
            $user = User::find($idUser);
            $parentcategoriesname = ParentCategory::where('id', $id_category)->first();
            $subcategoriesname = SubCategory::where('id_category', $id_category)->where('id_sub_category', $id_sub_category)->first();
            $detailTicket = TicketDetail::where('id_ticket',$tiketId)->orderBy('id','desc')->paginate(10);

            return view('pages.detail_ticket', compact('ticket', 'parentcategories', 'categories', 'pic', 'user', 'detailTicket', 'parentcategoriesname', 'subcategoriesname'));   
        }
        if($type == 'delete')
        {
            $ticket = Ticket::find($id);
            if($ticket->ticket_status == 'REGS'){
              $ticket->delete();
              return redirect('/administrator/report')->with('status', 'slider deleted successfully');
            }
        }

    }

    public function downloadFile($filename)
    {
        return response()->download(("images/{$filename}"));
    }


    public function complaintStoreFollowUp(Request $request)
    {
        // dd($request);
        $ticket = Ticket::where('id_ticket', $request->id_ticket)->first();
        $old = Ticket::where('id_ticket', $request->id_ticket)->first();
        if(!empty($ticket)){
            
            $user = User::where('id',$ticket->id_user)->get()->first();
            if(!empty($user)){
                $ticket->flag_read = "N";
                $ticket->save();
                $onesignal = new OneSignalController;
                $onesignal->sendToUser($user);
            }
            if(Auth::user()->group == 'administrator') {
                $ticket->id_category = !empty($request->id_category) ? $request->id_category : $ticket->id_category;
                $ticket->id_sub_category = !empty($request->id_sub_category) ? $request->id_sub_category : $ticket->id_sub_category;
                $ticket->ticket_status = !empty($request->ticket_status) ? $request->ticket_status : $ticket->ticket_status;
                $ticket->id_pic = !empty($request->id_pic) ? $request->id_pic : $ticket->id_pic;
                $ticket->save();
                if($ticket->ticket_status == 'PEND')
                {
                    //Send mail to Authorizer
                    $authorizer = User::where('group', 'Authorizer')->first();
                    $user = User::where('id', $ticket->id_user)->first();
                    Mail::to($authorizer->email)->send(new TicketforUser($authorizer, $ticket, 'ticketpending'));
                    if(!empty($user))
                    {
                        Mail::to($user->email)->send(new TicketforUser($user, $ticket,'changestatus'));
                    }
                    else
                    {
                        Mail::to($ticket->email_for_guest)->send(new TicketforUserGuest($ticket->email_for_guest, $ticket, 'changestatus'));
                    }
                    
                    // Mail::to($pic->email_pic)->send(new TicketforPic($ticket, $pic));
                    //Mail::to($user->email)->send(new TicketforPic($ticket, $user));
                }

                if($ticket->ticket_status == 'CLOS')
                {
                    // send email ke pelapor
                    $user = User::where('id', $ticket->id_user)->first();
                    if(!empty($user))
                    {
                        Mail::to($user->email)->send(new TicketforUser($user, $ticket, 'ticketclos'));
                    }
                    else
                    {
                        Mail::to($ticket->email_for_guest)->send(new TicketforUserGuest($ticket->email_for_guest, $ticket, 'ticketclos'));
                    }
                }
            }
            elseif(Auth::user()->group == 'authorizer'){
                if($ticket->ticket_status == 'OPEN')
                {
                    //Send mail to PIC
                    $pic = Pic::where('id', $ticket->id_pic)->first();
                    $user = User::where('id', $ticket->id_user)->first();
                    Mail::to($pic->email_pic)->send(new TicketforPic($ticket, $pic));
                    if(!empty($user))
                    {
                        Mail::to($user->email)->send(new TicketforUser($user, $ticket, 'changestatus'));
                    }
                    else
                    {
                        Mail::to($ticket->email)->send(new TicketforUserGuest($ticket->email_for_guest, $ticket, 'changestatus'));
                    }
                }
                elseif($ticket->ticket_status == 'REJ')
                {
                    //Send mail to user and admin
                    $user = User::where('id', $ticket->id_user)->first();
                    // $admin = User::where('group', 'administrator')->get();
                    if(!empty($user))
                    {
                        Mail::to($user->email)->send(new TicketforUser($user, $ticket, 'changestatus'));
                    }
                    else
                    {
                        Mail::to($ticket->email)->send(new TicketforUserGuest($ticket->email_for_guest, $ticket, 'changestatus'));
                    }
                }
            }
            
            if(!empty($request->user) AND !empty($request->feedback)){
                if($request->feedback != 'Others')
                {
                    $save = TicketDetail::create([
                        'id_ticket' => !empty($request->id_ticket) ? $request->id_ticket : '',
                        'user' => !empty($request->user) ? $request->user : '',
                        'response' => !empty($request->feedback) ? $request->feedback : '',
                    ]);
                }
                else
                {
                    $save = TicketDetail::create([
                        'id_ticket' => !empty($request->id_ticket) ? $request->id_ticket : '',
                        'user' => !empty($request->user) ? $request->user : '',
                        'response' => !empty($request->response) ? $request->response : '',
                    ]);
                }
            }
            if($old->ticket_status != $request->ticket_status)
            {
                if(!empty($request->ticket_status))
                {
                    $save = TicketDetail::create([
                        'id_ticket' => !empty($request->id_ticket) ? $request->id_ticket : '',
                        'user' => !empty($request->user) ? $request->user : '',
                        'response' => 'Change Status from '.$old->ticket_status.' to '. $request->ticket_status
                    ]);
                    $ticket->ticket_status = $request->ticket_status;
                    $ticket->save();            
                }
            }
            if(Auth::check())
            {
                $success = 'success';
                if($request->url_segment == 'list-complaint-pic'){
                    // return redirect('/users/list-complaint-pic')->with('work', 'Tiket berhasil difollow-up');
                    return redirect()->back()->with('type','success');
                }
                elseif($request->url_segment == 'list-complaint-approval'){
                    // return redirect('/users/list-complaint-approval')->with('work', 'Tiket berhasil difollow-up');
                    return redirect()->back()->with('type','success');
                }
                else{
                    // return redirect('/users/list-complaint')->with('work', 'Tiket berhasil difollow-up');
                    return redirect()->back()->with('type','success');
                }
            } else {
                return redirect('/');
            }
        }   
    }

}
