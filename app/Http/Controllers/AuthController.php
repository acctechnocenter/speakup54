<?php

namespace App\Http\Controllers;

use App\Mail\UserActivationMobile;
use App\Mail\UserPasswordReset;
use App\Model\User;
use App\Transformers\Json;
use App\Transformers\UserTransformer;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Crypt;
use Mail;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class AuthController extends Controller
{
    use AuthenticatesUsers;


    public function register(Request $request, User $user)
    {
        $validate = $this->validate($request, [
            'fullname' => 'required|string|min:6|max:255|regex:/^[\pL\s]+$/u',
            'no_handphone' => 'required|regex:/(08)[0-9]{9}/',
            'email' => 'required|string|email|max:255|unique:speakup_users,email,NULL,id,deleted_at,NULL',
            'password' => 'required|min:6',
        ]);
        if (!empty($validate)) {
            return response()->json(['error' => $validate], 401);
        } else {
            $user = new User;
            $user->fullname = $request->fullname;
            $user->no_handphone = $request->no_handphone;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->group = "member";
            $user->flag_active = "0";
            $user->save();
            $user->email = Crypt::encryptString($user->email);
            Mail::to($request->email)->send(new UserActivationMobile($user));

            return response()->json('success', 200);
        }
    }

    public function logout(Request $request, Response $response)
    {
        $user = $request->user();
        
        $user->tokens()->where('revoked', 0)->get()->each(function ($token, $value) {
            $token->revoked = 1;
            $token->save();
        });
        return response()->json(['message' => 'Logged out successfully'], 200);
    }

    public function login(Request $request, User $user)
    {
        if ($this->hasTooManyLoginAttempts($request)) 
        {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        if (!Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $this->incrementLoginAttempts($request);
            return response()->json(['error' => 'Your credential is wrong'], 401);
        } else {
            $user = Auth::user();

            if ($user->flag_active == 0) {
                return response()->json(['error' => 'Please activate your email first.'], 401);
            }
            if (!empty($user->tokens()->where('revoked', 0)->get()->first())) {
                return response()->json(['error' => 'You are already logged in. Please sign out of your other session first.'], 401);
            } else {

                $token = $user->createToken("accwhistle-mobile");
                return fractal()
                    ->item($user)
                    ->transformWith(new UserTransformer)
                    ->addMeta([
                        'token' => [
                            'access_token' => $token->accessToken,
                            'expires_at' => Carbon::parse($token->token->expires_at)->toDateTimeString(),
                        ],
                    ])
                    // ->addMeta([
                    //   'token' => $user->remember_token,
                    // ])
                    ->toArray();
            }
        }
    }

    public function reset(Request $request, User $user)
    {
        $user = User::where('email', $request->email)->first();

        if (!empty($user)) {
            $response = fractal()
                ->item($user)
                ->transformWith(new UserTransformer)
                ->addMeta([
                    'Reset status' => 'Email already send',
                ])
                ->toArray();

            $user->email = Crypt::encryptString($user->email);
            Mail::to($request->email)->send(new UserPasswordReset($user));

            return response()->json($response, 201);
        } else {
            return response()->json([
                'message' => 'user not found',
                'status' => 'failed',
            ], 401);
        }
    }

    public function resetpassword(Request $request, User $user)
    {
        $decryptedEmail = Crypt::decryptString($request->token);
        $user = User::where('email', $decryptedEmail)->first();

        // $user = User::where('email', $request->email)->first();

        if (!empty($user)) {
            $user->password = bcrypt($request->password);
            $user->save();
            $response = fractal()
                ->item($user)
                ->transformWith(new UserTransformer)
                ->addMeta([
                    'Status' => 'Password successful reset',
                ])
                ->toArray();

            return response()->json($response, 201);
        } else {
            return response()->json([
                'message' => 'user not found',
                'status' => 'failed',
            ], 401);
        }
    }

    protected function hasTooManyLoginAttempts(Request $request)
    {
        return $this->limiter()->tooManyAttempts(
            $this->throttleKey($request), 3, 1
        );
    }
}
