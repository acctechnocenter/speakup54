<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Blogs;
use App\Model\Slider;
use App\Model\Quote;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*public function __construct()
    {
        $this->middleware('auth');
    }*/

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog = Blogs::where('flag_publish', 'yes')->orderBy('created_at', 'DESC')->skip(0)->take(3)->get();
        $slider = Slider::where('flag_publish', 'yes')->skip(0)->take(5)->get();
        $quote = Quote::where('flag_publish', 'yes')->get();
        return view('pages.home', compact('blog', 'slider', 'quote'));
    }
}
