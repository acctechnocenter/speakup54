<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Blogs;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $view = 'blog-list';
        // $blog = Blogs::where('flag_publish', 'yes')->orderBy('created_at', 'DESC')->skip(0)->take(3)->get();
        $blog = Blogs::where('flag_publish', 'yes')->orderBy('created_at', 'DESC')->paginate(6);
        return view('pages.blog.blog', compact('view', 'blog'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $view = 'blog-content';
        $content = Blogs::where('slug_blog', $id)->first();
        $listBlog = Blogs::where('flag_publish', 'yes')->orderBy('created_at', 'DESC')->skip(0)->take(4)->get();
        return view('pages.blog.blog', compact('view', 'content', 'listBlog'));
    }

    public function collect(){
        $blogList = Blogs::paginate(5);
        return $blogList;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
