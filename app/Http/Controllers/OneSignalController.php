<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\User;
use Illuminate\Http\Response;
use OneSignal;

class OneSignalController extends Controller
{
    public function setUserIdentifier(Request $request, Response $response){
        // $user = User::where('id',$request->user_id)->first();
        $user = $request->user();
        if(!empty($user)){
            $user->id_device = $request->identifier;
            $user->save();
            return response()->json([
                'message' => 'success subscribe onesignal'
            ]);
        }else{
            return response()->json([
                'message' => 'user not found'
            ],204);
        }
    }
    public function sendToUser(User $user){
        try{
            if(!empty($user->id_device)){
                OneSignal::sendNotificationToUser(
                    "Tiket anda diperbarui",
                    $user->id_device
                );
            }
            return ;
        }catch(ClientException $e){
            return ;
        }
    }
    public function sendUserNotification($id,$message){
        OneSignal::sendNotificationToUser(
            $message,
            $id
        );
    }
}
