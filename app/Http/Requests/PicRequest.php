<?php

namespace App\Http\Requests;

use App\Model\Pic;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule; 

class PicRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_pic'          => 'required|string|regex:/^[\pL\s]+$/u',
            'no_handphone'      => 'required|regex:/(08)[0-9]{9}/',
            'email_pic'         => 'required|string|email',
            'department_pic'    => 'required|string',
        ];
    }

    public function messages()
    {
        return[
            'no_handphone.regex' => 'Nomor telepon yang anda masukkan tidak valid'
        ];
    }
}
