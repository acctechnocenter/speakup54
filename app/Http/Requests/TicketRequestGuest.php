<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TicketRequestGuest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [ 
            'ticket_description'    => 'required|string',
            'id_category'           => 'required',
            'id_sub_category'       => 'required',
            'email_guest'           => 'required|email',
            'ticket_attachment.*'     => 'mimes:jpg,gif,doc,pdf,docx,xls,xlsx,ppt,pptx,jpeg,png,mpga,mp3,mpeg,mp4,3gp'
        ];
    }
}
