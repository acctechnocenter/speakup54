<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'ticket_description'    => 'required|string',
            'id_category'           => 'required',
            'id_sub_category'       => 'required',
            'email'                 => 'required|email',
            'ticket_attachment.*'     => 'mimes:jpg,gif,doc,pdf,docx,xls,xlsx,ppt,pptx,jpeg,png,mpga,mp3,mpeg,mp4,3gp'
        ];
        // if($this->input('fileType')=='audio')
        // {
        //     $rules['ticket_attachment'] = 'mimes:mp3,mp4';
        // }
        return $rules;

    }
}
