<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'desc_sub_category' => 'required|string',
            'sub_category_name' => 'required|string',
            'sla' => 'required|integer',
            'id_category' => 'required|integer',
            'pic' => 'required|integer'
        ];
    }
}
