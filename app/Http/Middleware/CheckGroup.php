<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Model\User;
class CheckGroup
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check())
        {
            $user = User::find(Auth::id());
            if($user->group == "administrator" OR $user->group == "authorizer")
            {
                return $next($request);
            }
            else
            {
                Auth::logout();
                return redirect('/login');
            }
        }
        // return $next($request);
    }
}
