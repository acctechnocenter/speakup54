<?php

namespace App\Providers;

use Log;
use Monolog\Logger;
use Monolog\Handler\SlackWebhookHandler;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // // Send errors to slack channel
        // $monolog = Log::getMonolog();
        // if (!\App::environment('local')) {
        //     $slackHandler = new SlackWebhookHandler(env('SLACK_WEBHOOK'), '#error', 'App Alerts', false, 'warning', true, true, Logger::ERROR);
        // } else {
        //     $slackHandler = new SlackWebhookHandler(env('SLACK_WEBHOOK'), '#error', 'App Alerts', false, 'warning', true, true, Logger::ERROR);
        // }
        // $monolog->pushHandler($slackHandler);
    }
    

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
