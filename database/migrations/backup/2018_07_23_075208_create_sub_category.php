<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('speakup_sub_categories', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('id_category')->unsigned();
            $table->integer('id_sub_category')->unsigned();
            $table->string('desc_sub_category');
            $table->string('sub_category_name');
            $table->integer('id_pic');
            $table->integer('sla');
            $table->enum('flag_active',['APPROVE','PENDING', 'REJECT']);
            $table->string('created_by', 255);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('speakup_sub_categories');
    }
}
