<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpeakupSliderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('speakup_slider', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image_url');
            $table->text('title');
            $table->text('additional_text');
            $table->string('button_text');
            $table->string('button_url');
            $table->enum('flag_publish',['yes','no']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('speakup_slider');
    }
}
