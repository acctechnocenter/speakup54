<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpeakupUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('speakup_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname');
            $table->string('no_handphone');
            $table->string('email');
            $table->string('password');
            $table->enum('group',['authorizer', 'administrator', 'member', 'pic']);
            $table->enum('flag_active',['0','1']);
            $table->string('created_by');
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('speakup_users');
    }
}
