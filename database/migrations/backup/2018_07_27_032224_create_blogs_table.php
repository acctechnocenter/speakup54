<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('speakup_blogs', function (Blueprint $table) {
            $table->increments('id');
            // $table->string('slug_blog');
            $table->string('image_url');
            $table->string('title');
            $table->string('post_body');
            $table->enum('flag_publish',['yes','no']);
            $table->string('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('speakup_blogs');
    }
}
