<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('speakup_pics', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');
            $table->string('name_pic');
            $table->string('no_handphone');
            $table->string('email_pic');
            $table->string('department_pic');
            $table->enum('flag_active',['APPROVE','PENDING','REJECT']);
            $table->string('created_by');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('speakup_pics');
    }
}
