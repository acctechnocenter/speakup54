<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('speakup_quotes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image_url');
            $table->text('quote');
            $table->string('person_name');
            $table->string('job');
            $table->enum('flag_publish', ['yes', 'no']);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('speakup_quotes');
    }
}
