<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('speakup_tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_ticket');
            $table->integer('id_user');
            $table->integer('id_pic');
            $table->integer('id_category');
            $table->integer('id_sub_category');
            $table->text('ticket_description');
            $table->string('ticket_attachment');
            $table->enum('ticket_status',['REGS','PEND','OPEN', 'REJ', 'CLOS']);
            $table->integer('ticket_sla');
            $table->string('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('speakup_tickets');
    }
}
